## Background Information
- CLASSIFICATION: hill(rolling hills), scrub(bushes and small trees)
- COUNTRY : France
- ALTITUDE : 367 [m]
- POSITION : [47˚ 42' 51.4'' N 5˚ 33'12.6'' E] (https://geohack.toolforge.org/geohack.php?pagename=belmont&params=47_42_51.4_N_5_33_12.6_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This resource dataset consists of 10-minute statistics of wind speed and wind direction measurements from a small met mast at Belmont, Troyees, France. The period includes 0.5 years of measurements from 2006.

## Map
![A map of Denmark](./Resource/belmont/msmap.gif)
**Figure 1:** A map of Eastern France


## Photos
![](./Resource/belmont/map_01.jpg) **Photo 01:** Aeral foto of location in eastern part of France.

## Mast (relative positions with reference to the reference POSITION) 
1. 50 [m] (0,0,0)

## Project description
Wind resource measurements in France and Greece.
## Measurement system
50 mast with a logger. 

1. [List of mast signals](./Resource/belmont/mast_signals.csv)


## ACKNOWLEDMENTS	:	Arnulf Knittel
- INSTITUTION       :	Hrafnkel SARL
- ADDRESS 	        :   13 rue de savigny, F52500 Pressigny, France
- TEL/FAX 	        :   +33 325 0987 73
- CONTACTS 	        :   Arnulf Knittel
- PERIOD 	        :   2006 
- Naming_authority  	: 'DTU Data'
- DOI               	: 'https://doi.org/10.11583/DTU.14236703'


## PUBLICATIONS	    : 
1. 

## Public data
1. Resource data; belmont.nc  (NetCDF) 


