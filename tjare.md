## Background Information
- SITE : Tjareborg Enge, Esbjerg (tjare)
- CLASSIFICATION: flat(flat landscape), pastoral(open fields and meadows)	
- COUNTRY 	  	: Denmark	
- ALTITUDE 	  	: 4 [m]	
- POSITION : [55 26' 53.64'' N 8˚ 35'37.69'' E](https://geohack.toolforge.org/geohack.php?pagename=tjare&params=55_26_53.64_N_8_35_37.69_E_)
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
The dataset includes resource data, turbine SCADA data together with  time series of mast and structural wind turbine measurement the 60m/2MW wind turbine located in Tjæreborg Enge, Denmark. The statistical values are stored as 10-minute statistitics and consists of 33800 hours, recorded during the period 20-01-1988 until 18-01-1993. The time series of mast and structural load measurements represent more than 75 hours.


## Map
![A map of Denmark](./Resource/tjare/map_1.gif)
**Figure 1:** A map of Denmark

![Location map for Denmark including the Tjaereborg site.](./Resource/tjare/map01.gif)
**Figure 2:** Location map for Denmark including the Tjaereborg site.

![Location map for Denmark including the Tjaereborg site.](./Resource/tjare/MAP02.GIF)
**Figure 3:** Location map for wind turbine and masts at Tjaereborg site.
![Location map for Denmark including the Tjaereborg site.](./Resource/tjare/layout.gif)
**Figure 4:** Site layout for Tjaereborg site.
## Photos
![](./Resource/tjare/photo_01.jpg) **Photo 01:** PT100 and cup anemometer located at h=3 m.
![](./Resource/tjare/photo_02.jpg) **Photo 02:** Cup anemometer (ED-Service) at h=3 m.

![](./Resource/tjare/photo_03.jpg) **Photo 03:** View along the mast - upwards.

![](./Resource/tjare/photo_04.jpg) **Photo 04:** Arrangement of cup and vane, h=10 m.

![](./Resource/tjare/photo_05.jpg) **Photo 05:** Another view along the mast - upwards.

![](./Resource/tjare/photo_06.jpg) **Photo 06:** Sensors at h=3 & h=10 m above ground level.

![](./Resource/tjare/photo_07.jpg) **Photo 07:** 60 m / 2 MW, pitch controlled wind turbine.

![](./Resource/tjare/photo_08.jpg) **Photo 08:** 60 m / 2 MW, pitch controlled wind turbine.

![](./Resource/tjare/photo_09.jpg) **Photo 09:** 60 m / 2 MW wind turbine with large blade tip deflections.

## Graphs
![](./Resource/tjare/GRAPH01.GIF) **Graph 01:** Wind speed distribution, h = 60m, period= 1988 - 92.
![](./Resource/tjare/GRAPH02.GIF) **Graph 02:** Observed monthly 10-min. mean and max values
![](./Resource/tjare/GRAPH03.GIF) **Graph 03:** Wind rose , h=60m, period = 1988 - 1992
netpower curve for Tjareborg 2MW wind turbine.
![](./Resource/tjare/GRAPH04.GIF) **Graph 04:** Mean wind speed, h=10, 30, 45, 60, 75 & 90 m, period=1988 - 1992

## Drawings
![](./Resource/tjare/draw01.GIF) **Drawing 01:** Layout of 90 mast.
![](./Resource/tjare/draw02.GIF) **Drawing 02:** Sketch of cupanemometer mounting arrangement on boom.
![](./Resource/tjare/draw03.gif) **Drawing 03:** Layout of boom.
![](./Resource/tjare/draw04.gif) **Drawing 04:** Definition of mounting details.

## Reports
1. [Loads during normal operation mode (3MB)](./Resource/tjare/Normal_operation.pdf)
2. [Final report (20MB)](./Resource/tjare/Final_report.pdf)
3. [Tjaereborg WIND TURBINE (Esbjerg): Geometric and operational data.](./Resource/tjare/VK-184-901130.pdf)
4. [Tjaereborg WIND TURBINE (Esbjerg) Structural dymanics.](./Resource/tjare/VK-186-910411.pdf)
5. [Final Design Report, Vol 1 1986](./Resource/tjare/Final_report_1.pdf)
6. [Final Design Report, Vol 2 1986](./Resource/tjare/Final_report_2.pdf)


# Mast
1. 90 [m] (110,47,3) (mast 1.)
2. 90 [m] (-110,-47,3) (mast 2.)
3. 60 [m] (0,0,0) (wind turbine)

## Windturbines
1. ELSAM Projekt 60m/2MW 2000 [kW] (0,0,0) (reference position)

## Project description
The primary goal was to design and build a 2MW wind tubine at Tj‘reborg, Esbjerg. This project was accomplished with an intensive measuring programme where both mechanical loadsand the wind climate on the site was measured - and analyzed. The wind data were recorded as part of this measurement programme. Acknowledgements: Peter Christiansen Tech-wise and Peggy Friis, Elsam.

## Measurement system
The measurement system consists of 2 meteorological mast, mast no.1 was placed in front and mast no.2 behind the wind turbine - referring to the dominant wind sector. The measurements system was based on two HP-dataloggers and the data was transferred to a central computer (HP operating HP-Basic with home developed data handling routines and programs). The system operation was primarily focusing on recording structural loads included a number of meteorological channels. Data recording frequency 25 Hz and duration= 184, 600 or 3600 seconds. Only data with a duration of 600 seconds or more are used.

## Resource data (statistics)
1. [List of mast signals](./Resource/tjare/mast_signals.csv)
2. [List of turbine signals](./Resource/tjare/wt_signals.csv)
## Time series (Raw)
3. [List of mast signals](./Resource/tjare/ts_mast_signals.csv)
4. [List of turbine signals](./Resource/tjare/ts_wt_signals.csv)
5. [List of additional signals](./Resource/tjare/add_signals.csv) Note: The statistics for these signals are only included in the header of the packed ASCII file.


## Nominal values
![](./Resource/tjare/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./Resource/tjare/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./Resource/tjare/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./Resource/tjare/w_dist.gif)
**Figure n4:** Nominal wind speed distribuion.

![A map of Denmark](./Resource/tjare/ti_dist.gif)
**Figure n5:** Nominal turbulence distribuion.

![A map of Denmark](./Resource/tjare/dir_dist.gif)
**Figure n6:** Nominal wind direction distribuion. 


# ACKNOWLEDMENTS	: Elsamprojekt A/S, DK-7000 Fredericia	
- INSTITUTION       : Dept. of Energy Engineering, DTU
- ADDRESS 	        : ET,B404, DTU, 2800 Lyngby, Denmark
- TEL/FAX 	        : +45 4525 4318 / +45 4588 2421
- CONTACTS 	        : Kurt S. Hansen
- LINKS 	        : http://www.afm.dtu.dk/staff/ksh/
- COLLABS 	        : Elsamprojekt A/S, DK-7000 Fredericia
- FUND AGENTS       : EU, Joule-I
- PERIOD 	        : 1988-01-01 - 1993-01-15
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.16701961'

## PUBLICATIONS	    : 
1. [An evaluation of measured and predicted loads](./Resource/tjare/Paper_1.pdf)

## Public data
1. Resource data  (NetCDF) 
2. Run statistics (NetCDF) 
3. Raw time series = tjare.zip (ascii) each with a duration between 600 - 3600 sec, sampled with 25 Hz.
