## Background Information
- CLASSIFICATION: flat(flat landscape), coastal(water and land)
- COUNTRY : UK
- ALTITUDE : 5 [m]
- POSITION : [59˚ 6' 58.22'' N 3˚ 8'50.03'' W](https://geohack.toolforge.org/geohack.php?pagename=orkney&params=59_6_58.22_N_3_8_50.03_W_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This resource dataset consists of 10-minute wind speed and wind direction measurements from the met mast on Orkney Island, Scotland. The period includes more than 1 year of measurements, which starts in 2000.

## Map
![A map of Denmark](./Resource/orkney/msmap.gif)
**Figure 1:** A map of Denmark

## Photos
![](./Resource/orkney/burgerhill.jpg) **Photo 01:** Picture of the met mast.

## Drawings
![](./Resource/orkney/layout.gif) **Drawing 01:** Layout of 64 mast similar to the Tjareborg setup (tjare_2).

## Mast (relative positions with reference to the reference POSITION) 
MASTS
1. 64 [m] (0,0,0)

## Project description
High wind test site for NEG Micon wind turbines.

## Measurement system
NA

1. [List of mast signals](./Resource/orkney/mast_signals.csv)

2. [List of additional signals](./Resource/orkney/add_signals.csv) Note: The statistics for these signals are only included in the header of the packed ASCII file.

## Nominal values

![](./Resource/orkney/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./Resource/orkney/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./Resource/orkney/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./Resource/orkney/w_dist.gif)
**Figure n4:** Nominal wind speed distribuion.

![A map of Denmark](./Resource/orkney/ti_dist.gif)
**Figure n5:** Nominal turbulence distribuion.

![A map of Denmark](./Resource/orkney/dir_dist.gif)
**Figure n6:** Nominal wind direction distribuion. 

## ACKNOWLEDMENTS		
- INSTITUTION       :	NEG-Micon
- ADDRESS 	        :   Alsvej 21, DK-8900 Randers, Denmark
- TEL/FAX 	        :   +45 87105169 / +45 86105001
- FUND AGENTS       :   Internal
- PERIOD 	        :   1999-01-01 - 2001-12-17
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14307710'

## PUBLICATIONS	    : 
1. NA

## Public data
1. Resource data  (NetCDF) 
2. Raw time series = orkney.zip (ascii) each with a duration of 600-3600 sec and sampled with 1 Hz.


