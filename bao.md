## Background Information
- CLASSIFICATION: flat(flat landscape), pastoral(open fields and meadows)
- COUNTRY : US
- ALTITUDE : 1584 [m]
- POSITION : [40˚ 3' 0'' N105˚ 0'14.4'' E](https://geohack.toolforge.org/geohack.php?pagename=borlum&params=40_3_0_N_105_0_14.4_W_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This resource dataset consists of 1-minute statistics of wind speed and wind direction measurements from a tall met mast at BOA, CO, US. The period includes 3 year of measurements, which starts in 2002.

## Map
![A map of Denmark](./Resource/bao/msmap.gif)
**Figure 1:** Location of BOA, east of Boulder
![A map of Denmark](./Resource/bao/baomap.gif)
**Figure 2:** Road map including Erie, CO

## Photos
![](./Resource/bao/bao1.gif) **Photo 01:** Arial view of road including Erie in left corner.

![](./Resource/bao/webcam.jpg) **Photo 02:** view from mast in direction N

![](./Resource/bao/bao_view.jpg) **Photo 03:** View along mast from the ground.

![](./Resource/bao/bao4.gif) **Photo 4:** BAO is at center of circular pattern.

## Drawings
![](./Resource/bao/layout.gif) **Drawing 01:** Layout of the 300 mast.

## Mast (relative positions with reference to the reference POSITION) 
1. 300 [m] (0,0,0)

## Project description
The Boulder Atmospheric Observatory (BAO) tower has been owned and operated by the National Oceanic and Atmospheric Administration's (NOAA) Environmental Technology Laboratory (ETL) for more than 25 years. Constructed in 1976-1977 at a cost of $1.3M, the BAO sits on State of Colorado land just west of I-25, and just east of the town of Erie, along the I-25 corridor. At a height of 300 m (with structural strength capable of supporting an additional 200 m extension), the BAO is a very unique observational platform situated on the gently rolling plains of eastern Colorado. The BAO was originally constructed to support atmospheric boundary layer probes (e.g., temperature, humidity, wind, and turbulence sensors). Data from these sensors was used in a number of fundamental boundary layer studies and to assess the performance of a wide variety of ground- based remote sensing systems developed by ETL (e.g., radar, lidar, sodar). The dozens of publications that resulted from these studies have established the BAO tower as a premier boundary layer observation facility. Over the years use of the BAO has expanded. NOAA's Climate Monitoring and Diagnostics Laboratory (CMDL) began to make measurements from the top the BAO tower in 1985 in an effort to extend NOAA's radiation observational network and to take advantage of the unique opportunity presented by the tower. That opportunity was, and is, to be able to view a relatively large surface area and make extended quantitative observations of solar and infrared radiation that is either reflected or emitted from that portion of the earth's surface. The size of the area sampled from the top of the BAO approaches that sampled by satellite or simulated in numerical weather and climate models. The land urface area surrounding the BAO is representative of an area whose complexities make it a challenge to interpret in satellite data or to properly model numerically, but which is similar to a significant portion of the earth's land surface. This defining test for the satellite observations and General Circulations Models (GCM) makes the data particularly valuable. The data collected on the BAO tower have been, and are being, used in multiple satellite global radiation budget studies and by various GCM groups, including NOAA's Geophysical Fluid Dynamics Laboratory and European Center for Medium-Range Weather Forecasting. Additionally the BAO tower site is a participant in the World Climate Research Program's Baseline Surface Radiation Network (BSRN) that requires that the data meet certain high standards. The BSRN collects the data and provides it through its central high-visibility archive in Zurich, Switzerland. Data are also available directly from the CMDL radiation group in Boulder. The radiation data collected at the BAO tower are continuous, sampling once per second, 24 hours a day for the last 16 years with only minimal loss so that data are available for nearly any time frame of interest to the diverse user community. Also, such a long-term data set has become valuable in its own right as an important record of the local and regional climate as driven by and reflected in the energy budget obtained from such a uniquely representative vantage point. The BAO sits on 100 acres of State of Colorado land located ~1.25 miles west of I-25 and .25 miles N of Weld County Rd 8 (Road Map). This land is primarily agricultural and consists of natural grass and wheat fields.

## Measurement system
Completed in 1977, the BAO is a unique research facility for studying the planetary boundary layer and for testing and calibrating atmospheric sensors. The centerpiece of the facility is a 300-m tower instrumented at five levels with slow-response temperature and wind sensors, a variety of remote sensing systems, and a real-time processing and display capability that greatly reduces analysis time for scientists. The BAO has been the host of several large national and international experiments and numerous smaller ones. Journal of Applied Meteorology, 22(5):863-880 (1982) Details of the data acquisition and processing, and as well as references to studies conducted in the past are described in the Site Information link. Instrumentation 5 levels (10, 50,100,200,300 meters) T, RH, Wind Speed, Wind Direction Surface Pressure and precipitation These files are comma delineated from Campbell Scientific dataloggers. There are 24 files/day. Below are the data formats. 100, 200, 300 are the same format. surface/10m 01 Sensor ht 02 Year 03 Julian day 04 HHMM (UTC) end time of averaging period 05 10m temp (C) 06 10m RH (%) 07 10m wind speed (m/s) 08 10m wind direction (True) 09 datalogger battery voltage 10 surface pressure (mb) 11 rain (mm) 100, 200, 300m 01 Sensor ht 02 Year 03 Julian day 04 HHMM (UTC) end time of averaging period 05 temp (C) NW boom 06 RH (%) NW boom 07 wind speed (m/s) 08 wind direction (True) 09 datalogger battery voltage 10 wind speed (m/s) SE boom 11 wind direction (True) SE boom Wind directions are the direction from which the wind is blowing (meteorological). Note: There are 2 wind sensors at 100, 200, and 300m to take into account winds blowing through the tower. NW boom is oriented 334 deg true and SE boom is 154 degrees true. A good rule of thumb is to not use winds 124-184 for the NW boom and 304-004 for the SE boom. Further information Further information can be obtained from Daniel Wolfe. E-mail: Daniel.Wolfe@noaa.gov For more details contact Dan Wolfe 303-497-6204 Acknowledgement: NOAA Environmental Technology Laboratory 325 Broadway R/ETL Boulder, Colorado 80305-3328

1. [List of mast signals](./Resource/bao/mast_signals.csv)


## ACKNOWLEDMENTS	

- INSTITUTION       :	BOULDER ATMOSPHERIC OBSERVATORY Erie, CO
- ADDRESS 	        :   325 Broadway R/ETL, Boulder, Colorado 80305-3328
- TEL/FAX 	        :   +45 35322500 / +45  35322501
- CONTACTS 	        :   Daniel Wolfe
- LINKS 	        :   http://www.esrl.noaa.gov/psd/technology/
- PERIOD 	        :   1978 - 2003
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14198831'

## PUBLICATIONS	    : 
1. 

## Public data
1. Resource data  (NetCDF) 

