## Background Information
- CLASSIFICATION: mountain(sharp contours - separation expected), ice(snow and ice cover)
- COUNTRY : Sweden
- ALTITUDE : 341 [m]
- POSITION : [68˚ 24' 0'' N 18˚ 49'49.67'' E](https://geohack.toolforge.org/geohack.php?pagename=abisko&params=68_24_0_N_18_49_49.67_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)


## Short summary
This dataset consists of raw data and 10/60 - minute statistics of wind speed and wind direction measurements from a heigth of 7m on a frozen lake, abisko, Swenden. The period includes more than 225 hours of measurements from 1989 & 1994.

## Map
![A map of Denmark](./ts_wind/abisko/msmap.gif)
**Map 1:** Location of the abisko lake in the northern part of Sweden.


## Photos
![](./ts_wind/abisko/photo_1.gif) **Photo 01:** The lake - in summer time

![](./ts_wind/abisko/photo_2.gif) **Photo 01:** Hills around lake


![](./ts_wind/abisko/abisko-a.jpg) **Photo 03:** Abisko view
![](./ts_wind/abisko/abisko-b.jpg) **Photo 04:** Abisko view
![](./ts_wind/abisko/abisko-c.jpg) **Photo 05:** Abisko view


## Drawings
NA

## Reports
NA

## Mast (relative positions with reference to the reference POSITION) 
1 24 [m] (0,0,0)

## WIND TURBINES
NA

## Project description
Measurements from a frozen lake in a mountainous area - in Lapland.

## Measurement system
Only a tiny portion of data are available 7 x 60 minutes. Winddirection = 275° or 95°, depending on the wind direction (wind never through the mast). Sensor_direction = towards the wind 20 Hzdata at 3 levels on a 24 m tower are recorded during two months but only 1 level has been made available in the database. The instrumentation consists only of 3-component hotwires.

1. [List of mast signals (time series of wind speed)](./ts_wind/abisko/ts_mast_signals.csv)

## Nominal values

![](./ts_wind/abisko/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/abisko/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/abisko/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wind/abisko/w_dist.gif)
**Figure n4:** Nominal wind speed distribution.

![A map of Denmark](./ts_wind/abisko/ti_dist.gif)
**Figure n5:** Nominal turbulence distribution.

![A map of Denmark](./ts_wind/abisko/dir_dist.gif)
**Figure n6:** Nominal wind direction distribution. 

## ACKNOWLEDMENTS	:	 
- INSTITUTION       :	Dept. of Meteorology, Uppsala University
- ADDRESS 	        :   Uppsala University, Box 516 S-751 20 Uppsala, Sweden
- TEL/FAX 	        :   +46-18 542792 / +46-18 544 706
- CONTACTS 	        :   Mikael Magnusson / Ann-Sofi Smedman
- LINKS 	        :   http://www.met.uu.se/
- PERIOD 	        :   1989-03-27 - 1994-04-15
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.16895842'


## PUBLICATIONS	    : 
NA

## Public data
1. Run statistics; abisko.nc (NetCDF) 
2. Raw time series = abisko.zip (ascii), duration between 600 & 3600 secs and a sampling frequency = 20 Hz.


