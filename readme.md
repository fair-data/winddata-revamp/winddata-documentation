# Winddata.com on DTU Data
<i>Database on Wind Characteristics </i>
Winddata.com; was previously hosted on Risø DTU, but now the main part has been transferred to DTU Research Platform: DTU Data.

The database contains five different categories of wind data where each category is organized as a collection. The organisation of data is illustrated on Figure 1.

![](./intro/DTU_Data-structure.png)
**Figure 1:** Structure of WINDDATA.COM
The dataset are stored together with a description of the measuring site (masts and location) together a short identification of the nearby [disturbing] wind turbines.  

# Introduction:
A list of [sites](./intro/Site_index.csv); which are included the first four collections in Figure 1; documents shortly the measurement setup and instrumentation. This list can be used as a tool to identify applicable measurement data before downloading. 
## 1. Time series of turbulence measurements

This collection consists of sites with time series of turbulence measurements or turbulence array measurements. The measurements primarily include wind speed and wind direction signals, secondarily temperatures, barometric pressure and relative humidity.

The measurements are primarily intended for wind [turbine] design, validation models and education.
- [Turbulence measurement collection](https://doi.org/10.11583/DTU.c.5405292)

## 2. Wind resource data 

This collection consists of sites with wind resource data, which can be used for wind turbine siting, sampled with a frequency of more than 0.5 Hz from mast located instruments. The data primarily includes 10-minute statistics of wind speed and wind direction signals.

The measurements are primarily intended for wind [turbine] design, energy validation and education.
- [Resource data collection](https://doi.org/10.11583/DTU.c.5405286)
## 3. Wind Turbine load measurements

This collection consists of sites with wind turbine load measurements. The data primarily includes time series of structural loads from wind turbines together with wind measurements from a nearby mast, sampled with a frequency of more than 0.5 Hz.

The measurements are primarily intended for wind [turbine] design, validation of structural loads and education.

- [Wind Turbine load measurements collection](https://doi.org/10.11583/DTU.c.5405406)

<b>Note: NA yet</b>

## 4. Wind farm measurements.

This collection includes a small number of dataset, each representing a wind farm. Each dataset includes SCADA data for each individuaæl wind turbine in the wind farm and nearby masts. The data is stored as 10-minute statistical values. 
The measurements are primarily intended for wind [farm] analysis, wake analysis and education.

- [Wind farm measurements collection](https://doi.org/10.11583/DTU.c.5405418)

<b>Note: NA yet</b>

## 5. SCADA data & Project data (meta)
SCADA Data: This document list a number of internal database with restricted access hosted by DTU Wind Energy. The databases are accessible with reference to NDA contract between the data provider and DTU Wind Energy.
- [SCADA data - Overview](/NDAs/SCADA_DATA.md) 

project Data: This document list a number of internal project database hosted by DTU Wind Energy. The database accessible depend on the project managers permission.
- [Project data - Overview](/NDAs/Project_DATA.md)

<b>IMPORTANT</b> Access to these databases are restricted to employees at DTU Wind Energy.

- [SCADA and Project data collection](https://doi.org/10.11583/DTU.c.5405295) 

<b>Note: NA yet</b>
