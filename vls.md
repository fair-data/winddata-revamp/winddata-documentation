## Background Information
- CLASSIFICATION: hill(rolling hills), coastal(water and land)
- COUNTRY : Norway
- ALTITUDE : 38 [m]
- POSITION : [63˚ 51' 38.9'' N 9˚ 45'6'' E](https://geohack.toolforge.org/geohack.php?pagename=vls&params=63_51_38.9_N_9_45_6_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This dataset consists of raw time series and run statistics of wind speed and wind direction measurements from a 30m mast at Vallersund, NE of Frøya, Norway. The period includes 6000 hours of measurements from the period 1991-1994.

## Map
![A map of Denmark](./ts_wind/vls/msmap.gif)
**Map 1:** Western part of Mid-Norway.
![A map of Denmark](./ts_wind/vls/layout.gif)
**Map 2:** VLS site layout.
•

## Mast (relative positions with reference to the reference POSITION) 
1. 30 [m] (0,0,0)

## Project description
The project started in 1990, and the purpose was to study the wind structure, and to compare wind measurements to energy production from a neighbouring WECS.

## Measurement system
 The measurement station consists of 30 m mast with wind speed sensors at 30, 18, 10, and 5 m, and direction sensor at 12 m. Temperature has been measured at 30 and 10 m heights. The logging frequency has been 0.85 Hz.

1. [List of mast signals (time series of wind speed)](./ts_wind/vls/ts_mast_signals.csv)

## Nominal values

![](./ts_wind/vls/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/vls/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/vls/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wind/vls/w_dist.gif)
**Figure n4:** Nominal wind speed distribution.

![A map of Denmark](./ts_wind/vls/ti_dist.gif)
**Figure n5:** Nominal turbulence distribution.

![A map of Denmark](./ts_wind/vls/dir_dist.gif)
**Figure n6:** Nominal wind direction distribution. 

## ACKNOWLEDMENTS	:	Jørgen Løvseth, NTNU 
- INSTITUTION       :	Norwegian Univ. of Sci. and Techn., Dept. of Physics
- ADDRESS 	        :   NTNU, Dept. of Physics, Lade, N-7034 Trondheim, Norway
- TEL/FAX 	        :   +47 7359 1856 / +47 7359 1852
- CONTACTS 	        :   Jørgen Løvseth, NTNU
- LINKS 	        :   http://www.phys.ntnu.no/lade/forskning/miljo/miljofys.htm#Vind / 
- PERIOD 	        :   1992 - 1995
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14380823'
 

## PUBLICATIONS	    : 
NA

## Public data
1. Run statistics; vls.nc (NetCDF) 
2. Raw time series = vls.zip (zip) each with a duration of 3600 sec and sampled with 0.85 Hz. 

