## Background Information
- CLASSIFICATION: flat(flat landscape), coastal(water and land)
- COUNTRY : Netherlands
- ALTITUDE : 12 [m]
- POSITION : [52˚ 47' 8'' N 4˚ 40'20'' E](https://geohack.toolforge.org/geohack.php?pagename=ecn_ems&params=52_47_8_N_4_40_20_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This dataset consists of hourly wind speed, wind direction and temperature measurements from a 50m mast at ECN, Petten, The Netherlands. The period includes more than 650 hours of measurements, which starts in 2002. Furthermore, some 1 Hz hourly time series have been included. 

## Map
![A map of Denmark](./ts_wind/ecn_ems/msmap.gif)
**Figure 1:** Detailed image of the test site

![A map of Denmark](./ts_wind/ecn_ems/plattegrond2.gif)
**Figure 2:** Detailed image of the test site




## Photos
NA

## Drawings
![](./ts_wind/ecn_ems/draw1.gif) **Drawing 01:** Detailed image of the test site.

![](./ts_wind/ecn_ems/draw2.gif) **Drawing 01:** ECN Researc centre.


## Mast (relative positions with reference to the reference POSITION) 
MASTS
1. 12 [m] (0,0,0)

## Project description
Power performance measurements on the LW 5/2.5. These measurements are never carried out due to problems with the turbine. The wind data that were available, is included in the international winddatabase.


## Measurement system
 The measurements setup consists of a mixture of cups, vanes and a thermometers.


1. [List of mast signals](./ts_wind/ecn_ems/ts_mast_signals.csv)
2. [List of additional signals](./ts_wind/ecn_ems/add_signals.csv) Note: The statistics for these signals are only included in the header of the packed ASCII file.

## Nominal values

![](./ts_wind/ecn_ems/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/ecn_ems/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/ecn_ems/nom_dir.gif)
**Figure n3:** Nominal wind direction

## ACKNOWLEDMENTS		
- INSTITUTION       :	Energy research Center of the Netherlands (ECN)
- ADDRESS 	        :   PO Box 1, NL1755ZG Petten, The Netherlands
- TEL/FAX 	        :   +31 224 564115 / +31 224 568214
- CONTACTS 	        :   Peter Eecen
- LINKS 	        :   http://www.ecn.nl/
- COLLABS 	        :   Lagerwey
- FUND AGENTS       :   Internal
- PERIOD 	        :   2002-05-28 - 2003-08-26
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.18319544'

## PUBLICATIONS	    : 
1. NA

## Public data
1. ecn_ems data  (NetCDF) 
2. Time series of measurements, sampled with 1 Hz 


