## Background Information
- CLASSIFICATION: mountain(sharp contours - separation expected), forest(forest)
- COUNTRY : Japan
- ALTITUDE : 357 [m]
- POSITION : [36˚ 11' 40'' N 140˚ 7'52'' E](https://geohack.toolforge.org/geohack.php?pagename=mttsukuba&params=36_11_40_N_140_7_52_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This dataset includes time series of wind speed and wind direction measurements from a 30m masts on Mt. Tsukuba, which belongs to the Tsukuba Research Center, Japan. The period includes more than 2400 hours of measurements, which starts in 2001.

## Map

![A map of Japan](./ts_wind/mttsukub/japan.jpg)
**Map 1:** Location in Japan.
![A map of Japan](./ts_wind/mttsukub/kantou.jpg)
**Map 2:** Location of MT Tsukuba.

![A map of Japan](./ts_wind/mttsukub/3dsitemap01.jpg)
**Map 3:** Three-dimensional terrain map around the mesurement site-1.

![A map of Japan](./ts_wind/mttsukub/3dsitemap02.jpg)
**Map 4:** Three-dimensional terrain map around the mesurement site-2.

## Photos
![](./ts_wind/mttsukub/photo01.jpg) **Photo 01:** Photograph of the site.

![](./ts_wind/mttsukub/mastphoto01.jpg) **Photo 02:** View of the mast and the mounted anemometers.

![](./ts_wind/mttsukub/mastphoto02.jpg) **Photo 03:** View of the mast and the mounted anemometers including the 3D-sonic anemometer at level 24m agl.

## Mast (relative positions to the reference POSITION) 

1. 30.0 [m] (0,0,0)

## WIND TURBINES
NA

## Report
NA

## Project description
The site location on the MT Tsukuba mountain are operated by the Tsukuba Research Center.

## Measurement system
A 30 m was erected on MT Tsukuba and data from the sonic anemometer is accible from the public.

1. [List of mast signals](./ts_wind/mttsukub/ts_mast_signals.csv)

2. [List of additional signals](./ts_wind/mttsukub/add_signals.csv) Note: The statistics for these signals are only included in the header of the packed ASCII file.

ts_mast_signals.csv)

## Nominal values

![](./ts_wind/mttsukub/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/mttsukub/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/mttsukub/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wind/mttsukub/w_dist.gif)
**Figure n4:** Nominal wind speed distribution.

![A map of Denmark](./ts_wind/mttsukub/ti_dist.gif)
**Figure n5:** Nominal turbulence distribution.

![A map of Denmark](./ts_wind/mttsukub/dir_dist.gif)
**Figure n6:** Nominal wind direction distribution. 

## ACKNOWLEDMENTS	:	 
- INSTITUTION       :  Mechanical Engineering Laboratory(MEL)
- ADDRESS 	        :  Namiki 1-2, mttsukuba, Ibaraki, 305-8564, Japan  
- CONTACTS 	        :  Tetsuya Kogaki 
- LINKS 	        :  http://www.mel.go.jp/e/index.html / 
- FUND AGENTS       :  The Japanese government 
- PERIOD 	        :  1989 - 1998 
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14459103'

## PUBLICATIONS	    : 
NA

## Public data
1. Run statistics; mttsukub.nc (NetCDF) 
2. Raw time series = mttsukub.zip (ascii) each with a duration of 3600 sec and sampled with 4 Hz.