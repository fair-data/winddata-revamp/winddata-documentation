## Background Information
- CLASSIFICATION: flat(flat landscape), pastoral(open fields and meadows)
- COUNTRY : UK
- ALTITUDE : 304 [m]
- POSITION : [52˚ 8' 19.94'' N 4˚ 21'15.18'' E](https://geohack.toolforge.org/geohack.php?pagename=capel&params=52_8_19.94_N_4_21_15.18_W_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This Resource dataset consists of 10-minute wind speed and wind direction measurements from three tall met mast at Capel Cynon, Wales in UK. The period includes 6 years of measurements, which starts in 1989.

## Map
![A map of Wales](./Resource/capel/msmap.gif)
**Figure 1:** A map of Wales, UK.

## Photos
![](./Resource/capel/photo_04.jpg) **Photo 01:** Picture of the landscape.
![](./Resource/capel/photo_05.jpg) **Photo 02:** Another picture of the typical landscape.
![](./Resource/capel/photo_03.gif) **Photo 03:** Vector wind vane.

## Drawings
![](./Resource/capel/layout.gif) **Drawing 01:** Layout of the 40 & 50m masts.

## Reports
1. [Presentation of Vector 100A cupanemometer](./Resource/capel/ds-a100.pdf)
2. [Presentation of Vector w200 wind vane](./Resource/capel/ds-a100.pdf)

## Mast (relative positions with reference to the reference POSITION) 
1. 50 [m] (0,0,0)
2. 40 [m] (216,-1,0)
3. 40 [m] (1,326,0)

## Project description
In 1989 three meteorological masts, one 50m high and two 40m heigh, and supporting data logging equipment were installed at a site near Capel Cynon, Dyfed, Wales. Data were collected from the 50m mast for six years and four months.

## Measurement system
The main loggers were a Campell Scientific CR10 loggers. These collected 10 min averages on 5 seconds readings from wind speed sensors and directions sensors. They also collected ten minute averages of two seconds readings to calculate turbulence intensity at 10m, 25m and 40m heights. The spectral loggers were IBM compatible PCs. These performed two operations: Taking sets of data at 4Hz for the 10m, 25m and 40m anemometers when the wind speed was above 10 m/s. Approximately six set of data were taken for each of the twelve directions sectors.
Mast 1: Grid ref.:238.909 kmE,251.612 kmN boom:299 degW.
Mast 2: Grid ref.:239.125 kmE,249.880 kmN boom:338 degW.
Mast 3: Grid ref.:240.661 kmE,251.938 kmN boom:0 degW.

1. [List of mast signals](./Resource/capel/mast_signals.csv)


## Distributions
![A map of Denmark](./Resource/capel/w_dist.gif)
**Figure n4:** Nominal wind speed distribuion.

![A map of Denmark](./Resource/capel/dir_dist.gif)
**Figure n6:** Nominal wind direction distribuion. 

## ACKNOWLEDMENTS	:	ETSU; UK
- INSTITUTION       :	Future Energy Solutions, AEA Technology
- ADDRESS 	        :   Future Energy Solutions, AEA Technology B153, Harwell, Didcot, Oxon OX11 0QJ
- TEL/FAX 	        :   +44 (0) 1235 433565 / +44 (0) 1235 433434
- CONTACTS 	        :   Mike Payne
- LINKS 	        :   http://www.aeat.co.uk/cms/ 
- COLLABS 	        :   CSM Associates,Energy Research Unit, RAL
- FUND AGENTS       :
- PERIOD 	        :   1989 - 1995
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14135627'

## PUBLICATIONS	    : 
NA

## Public data
1. Resource data  (NetCDF) 


