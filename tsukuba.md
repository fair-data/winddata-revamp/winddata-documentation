## Background Information
- CLASSIFICATION: flat(flat landscape), rural(agriculture with some buildings)
- COUNTRY : Japan
- ALTITUDE : 30 [m]
- POSITION : [36˚ 10' 19'' N 140˚ 1'44'' E](https://geohack.toolforge.org/geohack.php?pagename=tsukuba&params=36_10_19_N_140_1_44_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This dataset includes time series of wind speed and wind direction measurements from a 15m masts at Tsukuba Research site, Japan. The period includes more than 1350 hours of measurements, which starts in 1997.

## Map

![A map of Japan](./ts_wind/tsukuba/japan.jpg)
**Map 1:** Location in Japan.

![A map of Japan](./ts_wind/tsukuba/kantoumap05.jpg)
**Map 2:** Layout north of Tokyo.

![A map of Japan](./ts_wind/tsukuba/no2rc.jpg)
**Map 3:** Site layout with location of mast and turbine - inside the research center.

## Photos
![](./ts_wind/tsukuba/photo01.jpg) **Photo 01:** Aerial photo of the site.

![](./ts_wind/tsukuba/mast01.jpg) **Photo 02:** View of the mast and the top mounted propeller.

![](./ts_wind/tsukuba/mast02.jpg) **Photo 03:** View of the mast and the top mounted propellor

♥
## Mast (relative positions to the reference POSITION) 

1. 15.1 [m] (0,0,0)

## WIND TURBINES
1. WINDMEL-II II 16.5 [kW] (30,22.6,0)

## Report
NA

## Project description
The wind turbine WINDMEL-II (WIND turbine at Mechanical Engineering Laboratory) has been elected by MEL in 1994 within the framework of the New Sunshine (NSS) project aiming at researching and developing various renewable energy technologies. The objective is to evaluating a flexible design concept of wind turbines. The WINDMEL-II (Two blades, diameter 15m) was desinged based on a flexible wind turbine technology such as variable speed operation, teetered hub, soft-desigend tower, and AC-DC-AC link.

## Measurement system
The WINDEMEL-II has been elected in the Tsukuba Research Center No.2, Agency of Industrial Science and Technology (AIST). The measurement system consists of 1 meteorological mast, which lies 37.5 m to the northeast of the WINDMEL-III. The height of the mast was 15.14 m, which is same as the hub height of the WINDMEL-II. Wind measurements are carried out using a propeller anemometer to evaluate the performance of the WINDMEL-II. The measurement data are transmitted from the sensors to the data logger by an optical fiber cable. All measurements are sampled with 1 Hz.

1. [List of mast signals](./ts_wind/tsukuba/ts_mast_signals.csv)

ts_mast_signals.csv)

## Nominal values

![](./ts_wind/tsukuba/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/tsukuba/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/tsukuba/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wind/tsukuba/w_dist.gif)
**Figure n4:** Nominal wind speed distribution.

![A map of Denmark](./ts_wind/tsukuba/ti_dist.gif)
**Figure n5:** Nominal turbulence distribution.

![A map of Denmark](./ts_wind/tsukuba/dir_dist.gif)
**Figure n6:** Nominal wind direction distribution. 

## ACKNOWLEDMENTS	:	 
- INSTITUTION       :  Mechanical Engineering Laboratory(MEL)
- ADDRESS 	        :  Namiki 1-2, Tsukuba, Ibaraki, 305-8564, Japan  
- CONTACTS 	        :  Tetsuya Kogaki 
- LINKS 	        :  http://www.mel.go.jp/e/index.html / 
- FUND AGENTS       :  The Japanese government 
- PERIOD 	        :  1989 - 1998 
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14413253'

## PUBLICATIONS	    : 
NA

## Public data
1. Run statistics; tsukuba.nc (NetCDF) 
2. Raw time series = tsukuba.zip (ascii) each with a duration of 600 sec and sampled with 8 Hz.
