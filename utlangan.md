## Background Information
- CLASSIFICATION: flat(flat landscape), coastal(water and land)
- COUNTRY : Sweden
- ALTITUDE : 1 [m]
- POSITION : [56˚ 0' 52.34'' N 15˚ 47'15.91'' E](https://geohack.toolforge.org/geohack.php?pagename=utlangan&params=56_0_52.34_N_15_47_15.91_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This dataset consists of raw data and 10-minute statistics of wind speed and wind direction measurements from a 22m mast near utlangan, Swenden. The period includes more than 27 hours of measurements from June 1989.

## Map
![A map of Denmark](./ts_wind/utlangan/Sweden.png)
**Map 1:** Location of utlangan in the southern part of Sweden

![A map of Denmark](./ts_wind/utlangan/Blekinge.png)
**Map 1:** Location of utlangan in the swedish archipelago in Blekinge

![A map of Denmark](./ts_wind/utlangan/mapquest.gif)
**Map 1:** Location of utlangan in the swedish archipelago

## Photos
![](./ts_wind/utlangan/photo_1.gif) **Photo 01:**photo from the mast towards north

![](./ts_wind/utlangan/photo_2.gif) **Photo 01:**Photo from the mast towards north north east

![](./ts_wind/utlangan/photo_3.gif) **Photo 01:**Photo from the mast towards south west


![](./ts_wind/utlangan/photo_4.gif) **Photo 02:** Meteorological mast from previous experiment


## Reports
NA

## Mast (relative positions with reference to the reference POSITION) 
1. 22.5 [m] (0,0,0)


## Project description
Meteological measurements from a farmland site in the swedish archipelago.

## Measurement system
Only a tiny portion of the measured data are available. The instrumentation consists of 3-component hotwires located in three levels: 2.5 12 & 22.5 m. Derived signals for horizontal wind sped and wind direct are included as signals. 

1. [List of mast signals (time series of wind speed)](./ts_wind/utlangan/ts_mast_signals.csv)

## Nominal values

![](./ts_wind/utlangan/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/utlangan/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/utlangan/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
NA

## ACKNOWLEDMENTS	:	 
- INSTITUTION       :	Dept. of Meteorology, Uppsala University
- ADDRESS 	        :   Uppsala University, Box 516 S-751 20 Uppsala, Sweden
- TEL/FAX 	        :   +46-18 542792 / +46-18 544 706
- CONTACTS 	        :   Mikael Magnusson / Ann-Sofi Smedman
- LINKS 	        :   http://www.met.uu.se/
- PERIOD 	        :   1989-05-30	- 1989-06-17
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.16895431'


## PUBLICATIONS	    : 
NA

## Public data
1. Run statistics; utlangan.nc (NetCDF) 
2. Raw time series = utlangan.zip (ascii), with a duration of 600 sec and sampling frequency = 20 Hz.


