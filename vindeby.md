## Background Information
- CLASSIFICATION: flat(flat landscape), offshore(open sea)
- COUNTRY : Denmark
- ALTITUDE : 0 [m]
- POSITION : [54˚ 58' 5.39'' N 11˚ 7'38.406'' E](https://geohack.toolforge.org/geohack.php?pagename=vindeby&params=54_58_5.394_N_11_7_38.406_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This dataset consists of time series and run statistics of wind speed and wind direction measurements from one onshore and two offshore masts near the Vindeby offshore wind farm in Denmark (Decommissioned 2017). The period includes more than 2300 hours of measurements, which starts in 1994.
Furthermore, the Vindeby resource dataset (Vindeby2) consists of 30-minute statistics of wind speed and wind direction measurements from he three mast. The period includes more than 5 years of measurements from 1993.
## Map
![A map of Denmark](./ts_wind/vindeby/map_2.gif)
**Figure 1:** Location of the Vindeby offshore wind farm.
![A map of Denmark](./ts_wind/vindeby/msmap.gif)
**Figure 2:** Location of Vindeby offshore, Denmark
![A map of Denmark](./ts_wind/vindeby/map_3.gif)
**Figure 3:** The configuration of the wind farm and positions of the masts at Vindeby


## Photos
![](./ts_wind/vindeby/r3r3a_lrg.jpg) **Photo 01:** Gill Solent sonic anemometer.
![](./ts_wind/vindeby/vindeby_2.jpg) **Photo 02:** Vindeby wind offshore wind turbines.

## Graphs
![](./ts_wind/vindeby/GRAPH01.gif) **Graph 01:** Wind speed distribution, h = 60m, period= 1988 - 92.

## Drawings
![](./ts_wind/vindeby/vindeby_layout.gif) **Drawing 01:** Layout for Vindemasts.
![](./ts_wind/vindeby/draw_1.gif) **Drawing 02:** Configuration of landbased mast.
![](./ts_wind/vindeby/draw_2.gif) **Drawing 03:** Configureation of the two offshore masts.


## Reports
1. [The Vindeby project: A description](https://orbit.dtu.dk/files/12310854/ris_r_741.pdf)
2. [The Vindeby project: Cluster effect](https://orbit.dtu.dk/files/12674798/ris_r_1188.pdf)

## Mast (relative positions with reference to the reference POSITION) 
1. 45 [m] (-494,191,0)
2. 45 [m] (172,-245,0)
3. 45 [m] (-194,-1410,2)

## Windturbines

1. Bonus 450kW 450 [kW] (-688,983,0)
2. Bonus 450kW 450 [kW] (-515,737,0)
3. Bonus 450kW 450 [kW] (-344,491,0)
4. Bonus 450kW 450 [kW] (-172,246,0)
5. Bonus 450kW 450 [kW] (0,0,0)
6. Bonus 450kW 450 [kW] (-538,1283,0)
7. Bonus 450kW 450 [kW] (-366,1037,0)
8. Bonus 450kW 450 [kW] (-194,791,0)
9. Bonus 450kW 450 [kW] (-22,546,0)
10. Bonus 450kW 450 [kW] (150,300,0)
11. Bonus 450kW 450 [kW] (322,54,0)

## Project description
Vindeby wind farm is located off the northwestem coast of the island of Lolland, consists of 11 Bonus 450kW turbines arranged in two rows oriented along an axis of 325-145°. The most southerly turbine in the array is approximately 1.5km from land and the turbine spacing is 300m both along and between the rows. The water depth is between 1.1 and 5.1 m.

## Measurement system
ln order to study the meteorological aspects of the wind farms and to provide information on wind how in the coastal three meteorlogical masts have been Erected, one onland and two offshore. The landmast is located nearly to the most southerly turbine in the array. The two offshore masts are placed at distances equal to the turbine spacing (300m), one to the west and one to the south of the firts row. The topography at Vindeby is very flat and lies close to sea level. No topographic enhancement of the wind speed is expected. To the south of the land mast the terrain is mainly open farmland with a few scattered houses and trees, with open sea to the north. The coastline runs approximately along the line of 285~l05°.
Instruments have been installed at the same heights (with reference to sea level) on all three 45m masts. Two turbines (4W and SE) have been instrumented for basic structural measurements like power and bending moment in the tower buttom. 

1. [List of mast signals](./ts_wind/vindeby/ts_mast_signals.csv)

2. [List of additional signals](./ts_wind/vindeby/add_signals.csv) Note: The statistics for these signals are only included in the header of the packed ASCII file.

## Nominal values

![](./ts_wind/vindeby/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/vindeby/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/vindeby/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wind/vindeby/w_dist.gif)
**Figure n4:** Nominal wind speed distribuion.

![A map of Denmark](./ts_wind/vindeby/ti_dist.gif)
**Figure n5:** Nominal turbulence distribuion.

![A map of Denmark](./ts_wind/vindeby/dir_dist.gif)
**Figure n6:** Nominal wind direction distribuion. 

## ACKNOWLEDMENTS	:	
- INSTITUTION       :	Risoe National laboratory
- ADDRESS 	        :   Wind energy and Atmospheric Physics Dept., PB49, DK-4000 Roskilde
- TEL/FAX 	        :   +45 4677 5000 / +45 4677 5083
- CONTACTS 	        :   http://www.risoe.dk/vea/ / http://mist.ats.orst.edu/
- LINKS 	        :   R.Barthelmie / Peter Sanderhof
- COLLABS 	        :   M.S. Courtney, P. Sanderhoff, L. Mahrt, J. Edson, J. Wilcza
- FUND AGENTS       :   Danish Ministry of Energy, ELKRAFT, JOULEII, Office of Naval Research
- PERIOD 	        :   1993 - 2000
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14387480'

## PUBLICATIONS	    : 
1. 

## Public data
1) 
1. Run statistics; vindeby_all.nc (NetCDF) 
2. Raw time series = vindeby.zip (zip) each with a duration of 1800 sec
 and sampled with 2.5, 5, 10 and 20 Hz.
