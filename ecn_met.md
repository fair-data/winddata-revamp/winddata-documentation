## Background Information
- CLASSIFICATION: flat(flat landscape), coastal(water and land)
- COUNTRY : Netherlands
- ALTITUDE : 12 [m]
- POSITION : [52˚ 47' 8'' N 4˚ 40'20'' E](https://geohack.toolforge.org/geohack.php?pagename=ecn_met&params=52_47_8_N_4_40_20_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This resource dataset consists of 1-minute wind speed and wind direction measurements from a met mast at ECN, Petten, The Netherlands. The period includes more than 142 hours of measurements, which starts in 2002. Furthermore, some 4 Hz time series from the 3D Sonic have been included. 

## Map
![A map of Denmark](./Resource/ecn_met/msmap.gif)
**Figure 1:** Detailed image of the test site
![A map of Denmark](./Resource/ecn_met/lokatie.jpg)
**Figure 2:** Detailed image of the test site

![A map of Denmark](./Resource/ecn_met/plattegrond.jpg)
**Figure 3:** Detailed image of the test site

## Photos
![](./Resource/ecn_met/picnorth.jpg) **Photo 01:** View to the north
![](./Resource/ecn_met/picnortheast.jpg) **Photo 02:** View to the northeast
![](./Resource/ecn_met/piceast.jpg) **Photo 03:** View to the east
![](./Resource/ecn_met/picsoutheast.jpg) **Photo 04:** View to southeast
![](./Resource/ecn_met/picsouth.jpg) **Photo 05:** View to the south
![](./Resource/ecn_met/picsouthwest.jpg) **Photo 06:** View to the southwest
![](./Resource/ecn_met/picwest.jpg) **Photo 07:** View to the west
![](./Resource/ecn_met/picnorthwest.jpg) **Photo 08:** View to the northwest

View to the west
View to the northwest

## Mast (relative positions with reference to the reference POSITION) 
MASTS
1. 12 [m] (0,0,0)

## Project description
Power performance measurements on the LW 5/2.5. These measurements are never carried out due to problems with the turbine. The winddata that were available are included in the international winddatabase.


## Measurement system
 The measurements setup consists of a mixture of cups, vanes and a sonic anemometer.


1. [List of mast signals](./Resource/ecn_met/Mast_signals.csv)
2. [List of additional signals](./Resource/ecn_met/add_signals.csv) Note: The statistics for these signals are only included in the header of the packed ASCII file.

## Nominal values

![](./Resource/ecn_met/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./Resource/ecn_met/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./Resource/ecn_met/nom_dir.gif)
**Figure n3:** Nominal wind direction

## ACKNOWLEDMENTS		
- INSTITUTION       :	Energy research Center of the Netherlands (ECN)
- ADDRESS 	        :   PO Box 1, NL1755ZG Petten, The Netherlands
- TEL/FAX 	        :   +31 224 564115 / +31 224 568214
- CONTACTS 	        :   Peter Eecen
- LINKS 	        :   http://www.ecn.nl/
- COLLABS 	        :   Lagerwey
- FUND AGENTS       :   Internal
- PERIOD 	        :   2002-05-28 - 2003-08-26
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14307731'

## PUBLICATIONS	    : 
1. 

## Public data
1. Resource data  (NetCDF) 
2. Time series of sonic measurements, sampled with 4 Hz 


