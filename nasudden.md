## Background Information
- CLASSIFICATION: flat(flat landscape), coastal(water and land)
- COUNTRY : Sweden
- ALTITUDE :  5 [m]
- POSITION : [57˚ 4' 20.69'' N 18˚ 13'14.68'' E](https://geohack.toolforge.org/geohack.php?pagename=nasudden&params=57_4_20.69_N_18_13_14.68_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This dataset consists of time series and 10-minute statistics of wind speed and wind direction measurements from a tall met mast at Näsudden, Gotland Sweden. The period includes more than 3 years of measurements, which starts in 1992.

## Map
![A map of Denmark](./ts_wind/nasudden/msmap.gif)
**Figure 1:** A map of Gotland, Sweden

## Photos
![](./ts_wind/nasudden/photo_1.gif) **Photo 01:** From the mast towards north
![](./ts_wind/nasudden/photo_2.gif) **Photo 01:** From the mast towards east
![](./ts_wind/nasudden/photo_3.gif) **Photo 03:** From the mast towards south
![](./ts_wind/nasudden/photo_4.gif) **Photo 04:** From the mast towards west
![](./ts_wind/nasudden/photo_5.jpg) **Photo 05:** Both turbines seen towards west
![](./ts_wind/nasudden/photo_6.jpg) **Photo 06:** Nasudden II, 3 MW wind turbine
![](./ts_wind/nasudden/photo_7.jpg) **Photo 07:** Nordic 1000, 1000 kW wind turbine
![](./ts_wind/nasudden/photo_8.jpg) **Photo 08:** Nordic 1000, 1000 kW wind turbine
![](./ts_wind/nasudden/photo_9.jpg) **Photo 09:** Details from met.mast

![](./ts_wind/nasudden/photo_10.jpg) **Photo 10:** The huge meteorologi mast at Näsudden.

![](./ts_wind/nasudden/photo_11.jpg) **Photo 11:** Cup anemometer used at Näsudden.

## Drawings
![](./ts_wind/nasudden/layout.gif) **Drawing 01:** Layout of the 135m tall mast and 2 wind turbines.
![](./ts_wind/nasudden/site_layout.gif) **Drawing 02:** Location of turbines and mast.

## Mast (relative positions with reference to the reference POSITION) 
1. 135 [m] (0,0,0)

## Windturbines
1. Kvaerner Turbin AB WTS80-3/1 2 [kW] (160,0,0)
2. Nordic 1000kW

## Project description
Meteologocal mesurements from Nasudden site at Gotland, Sweden

## Measurement system
 20 Hz data at two or three levels on a 135 m tower during several campaigns of 2-4 weeks. IMPORTANT sh1,sh2, sh3 - derived speeds dh1,dh2,dh3 - derived direction

1. [List of mast signals](./ts_wind/nasudden/mast_signals.csv)

## Nominal values

![](./ts_wind/nasudden/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/nasudden/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/nasudden/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wind/nasudden/w_dist.gif)
**Figure n4:** Nominal wind speed distribution.

![A map of Denmark](./ts_wind/nasudden/ti_dist.gif)
**Figure n5:** Nominal turbulence distribution.

![A map of Denmark](./ts_wind/nasudden/dir_dist.gif)
**Figure n6:** Nominal wind direction distribution. 

## ACKNOWLEDMENTS		
- INSTITUTION       :	Dept. of Meteorology, Uppsala University
- ADDRESS 	        :   Uppsala University, Box 516 S-751 20 Uppsala, Sweden
- TEL/FAX 	        :   +46-18 542792 / +46-18 544 706
- CONTACTS 	        :   Mikael Magnusson / Ingemar Carlen
- LINKS 	        :   http://www.met.uu.se/
- COLLABS 	        :   FFA & Teknikgruppen AB, S-19121 Sollentona
- PERIOD 	        :   1980-01-01 - 1997-12-31
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14153213'

## PUBLICATIONS	    : 
1. 

## Public data
1. Run statistics; Nasudden.nc (NetCDF) 
2. Raw time series; nasudden.zip (stored in zipped files) 

