## Background Information
- CLASSIFICATION: flat(flat landscape), pastoral(open fields and meadows)
- COUNTRY : Netherlands
- ALTITUDE : -0.5 [m]
- POSITION : [51˚ 58' 14'' N 4˚ 55'35'' E](https://geohack.toolforge.org/geohack.php?pagename=cabauw&params=51_58_14_N_4_55_35_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
The Cabauw dataset includes two different dataset:
The first resource dataset consists of hourly wind speed and wind direction measurements recorded on the Cabauw mast in the Netherland. This dataset includes 14 years of measurements, which starts in 1986. 
The second Cabauw dataset consists of raw time series (2 Hz) and run statistics of wind speed and wind direction measurements from the tall 213m mast. This dataset includes more than 480 hours of time series from the period 1985-1986.  

## Map
![](./Resource/Cabauw/map_1.gif)
**Figure 1:** Location map of the provinces South Holland including Cabauw site.

![](./Resource/Cabauw/map_2.gif)
**Figure 2:** The surrounding of the meteo tower

![](./Resource/Cabauw/map_3.gif)
**Figure 3:** site layout of the 213 m tower
 
![](./Resource/Cabauw/msmap.gif)
**Figure 4:** Map of the surrounding of the meteo tower

## Photos
![](./Resource/Cabauw/photo_1.GIF) **Photo 01:** The KNMI Meteo tower, Cabauw, Utrecht.PT100 and cup anemometer located at h=3 m.
![](./Resource/cabauw/photo_2.gif) **Photo 02:** Booms at theKNMI Meteo tower, Cabauw, Utrecht, NL.
![](./Resource/cabauw/photo_3.gif) **Photo 03:** The KNMI Meteo tower, Cabauw, Utrecht, NL, from the Internet.


## Graphs
NA

## Drawings
![](./Resource/cabauw/layout.gif) **Drawing 01:** Layout of 90 mast.
Layout of the 213 m tower with location of propellor anemometers.

## Reports
1. [Description of measurement setup - pdf file](./Resource/Cabauw/documentation.pdf)



## Mast (relative positions with reference to the reference POSITION) 
1. 213 [m] (0,0,-0.5)


## Project description
 In the frame work of the project "a manual of design wind data for wind turbines " [1], measurements from the Royal Netherlands Meteological Institute (KNMI) were used extensively. In this project a set of wind measurements of about 800 hr, measured in 1985 and 1986, was used. An important subject in this project was the frequency of occurrence of wind gusts and of wind direction changes [2]. Wind data files in which average values (averaging time 600s) greater than 15 m/s are present at 20 m height, were classified as wind files with strong winds. The wind data in this subset (50 hrs) are made available for the "Database on Wind Characteristics". The KNMI allows the use of these data under the following conditions: Users of the data refer to the source of the data e.g. with the following sentence: The Cabauw wind data were made available by the Royal Netherlands Meteological Institute (KNMI). The KNMI appreciates to be informed how the data were treated. and likes to get relevant reports. In general commercial use of the data is not allowed. Information about the terms for commercial use can be obtained at the KNMI, attention to W.A.A. Monna, Section atmosferic research, P.O. Box 201, 3730 AE De Bilt, NL. [Measurement_System].

## Measurement system
The Cabauw meteo mast is a tubular tower with a height of 213 m and a diameter of 2 m. Guy wires are attached at four levels. From 20m upwards horizontal trussed measurement booms are installed at intervals of 20 m. At each level there are three booms, extending 10.4 m from the centerline of the tower. These booms point to the directions 10, 130, 250 degrees relative to North. The SW and N booms are used for wind velocity and wind direction measurements. These booms carry at the end two lateral extensions with a length of 1.5 m and a diameter of about 4 cm. The Cabauw meteo mast is described in detail in [3].

1. [List of mast signals, resource](./Resource/cabauw/Mast_signals.csv)
2. [List of mast signals, 2Hz](./Resource/cabauw/ts_mast_signals.csv)

## Nominal values

![](./Resource/Cabauw/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./Resource/Cabauw/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./Resource/Cabauw/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./Resource/Cabauw/w_dist.gif)
**Figure n4:** Nominal wind speed distribuion.

![A map of Denmark](./Resource/Cabauw/ti_dist.gif)
**Figure n5:** Nominal turbulence distribuion.

![A map of Denmark](./Resource/Cabauw/dir_dist.gif)
**Figure n6:** Nominal wind direction distribuion. 

## ACKNOWLEDMENTS	:	
- INSTITUTION       :	Dept. of Renewable Energy, the netherlands Energy Research Foundation, ECN
- ADDRESS 	        :   P.O.Box 1, NL 1755 ZG Petten, the Netherlands
- TEL/FAX 	        :   +31 224 5642278 / +31 224 563214
- CONTACTS 	        :	J.W.M. Dekker
- LINKS 	        :   http://www.ecn.nl/
- COLLABS 	        :   TNO-IMET, Apeldoorn,NL; KNMI, de Bilt, NL
- FUND AGENTS       :   NOVEM, Utrecht, NL
- PERIOD 	        :   1986-07-01 - 2000-12-31
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14153192'

## PUBLICATIONS	    : 
NA 

## Public data
1. Resource data
    - cabauw_all.nc  (NetCDF) 
    - cabauw_concurrent.nc (NetCDF)

2. Run statistics stored as NetCDF format
    - cabauw_ts_all.nc (NetCDF) 
    - cabauw_ts_concurrent.nc (NetCDF)

3. Raw time series (ascii), each with a duration of 3600 sec, sampled with 2 Hz.: 
    - Cabauw.zip


