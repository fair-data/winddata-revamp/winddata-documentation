## Background Information

- CLASSIFICATION: flat(flat landscape), coastal(water and land)
- COUNTRY : Denmark
- ALTITUDE : 0 [m]
- POSITION : [5 19'9.82'' N 10 58'52.81''E](https://geohack.toolforge.org/geohack.php?pagename=sprogoe&params=55_19_59.82_N_10_58_52.81_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)
## Short summary
This dataset consists of time series and statistics of wind speed and wind direction measurements from two 70m masts at Sprogoe, Great belt, Denmark. The period includes more than 500 hours of measurements from 1990-1991.

## Map
![A map of Denmark](./ts_wind/sprogoe/msmap.gif)
**Figure 1:** Location of Gedser Rev SE to Gedser.

Layout of the mast arrangement
The location of mast array on Sprogø

## Photos
![](./ts_wind/sprogoe/Picture1.png) **Photo 01:** Sprogoe mast array
![](./ts_wind/sprogoe/Picture2.png) **Photo 02:** Sprogoe mast array,  zoom of the top section
![](./ts_wind/sprogoe/photo_1.gif) **Photo 03:** Airal photo of the mast location.


## Drawings
![](./ts_wind/sprogoe/layout.gif) **Drawing 01:** layout of the masts 

## Reports
NA

## Mast (relative positions with reference to the reference POSITION)
1. 70 [m] (0,0,0)
2. 70 [m] (38.6,10.4,0)

## Windturbines
NA

## Project description
 With the purpose of supporting determination of the design wind loads on the East Bridge of The Great Belt Link, Risoe National Laboratory has made an investigation of the structure of the turbulence at the height of the suspended span on the suspension bridge. The experiment: In the period extending from June 1990 to the middle of june 1991 omni-directional anemometers, mounted on 70 m high meteorological towers at Sprogø, have measured the wind velocity vectors at three horizontally displaced points. The Measuring Site: The measurements were recorded on a small island, Sprogø, located in the middle of The Great Belt between Funen and Zealand. The meteorological masts were erected on the eastern side of some flow obstacles present on the island (hill and buildings), which enables free access for northern, eastern and sourtherly wind directions. Acknowledgements: The Sprogø experiment was funded by A/S Storebæltsforbindelsen under contract no. 413.85. A/S Storebæltsforbindelsen has kindly given permission to include the measured time series from the Sprogø experiment in "Database on Wind Characteristics". Address: A/S Storebæltsforbindelsen, Vester Søgade 10, 1601 Kbh. V, telf.: +45 3393 5200
## Measurement system
Measuring mast: The measuring setup consists of 2 meteorological masts each with a height of 70 m and 40 m horizontal separation. A 15 m horizontal boom is mounted symmetrically on top of the second mast in such a way that the whole construction has the form of the letter "T". Instrumentation: The basis for the coherence measurements consists of an array of 3 sonic anemometers - mounted at a height of 70 m. Close to each sonic anemometer is installed a cup anemometer, and furthermore a single wind vane is mounted in the centre of the "T" boom. The data acquisition system: The data acquisition system is based upon a personal computer (pc) running the Risø DAQ data acquisition software. Signals are sampled continously with time series data being logged to disk when certain wind speed and wind direction criteria are satisfied. Data scanning and recording: All data are sampled with 40 Hz. For sonic anemometes channels the stored data is reduced to 10 Hz by block averaging. Similarly, wind vane and cup anemometer signals are reduced to 2 Hz.

1. [List of mast signals](./ts_wind/sprogoe/ts_mast_signals.csv)

2. [List of additional signals](./ts_wind/sprogoe/add_signals.csv) Note: The statistics for these signals are only included in the header of the packed ASCII file.

## Nominal values

![](./ts_wind/sprogoe/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/sprogoe/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/sprogoe/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wind/sprogoe/w_dist.gif)
**Figure n4:** Nominal wind speed distribuion.

![A map of Denmark](./ts_wind/sprogoe/ti_dist.gif)
**Figure n5:** Nominal turbulence distribuion.

![A map of Denmark](./ts_wind/sprogoe/dir_dist.gif)
**Figure n6:** Nominal wind direction distribuion. 

## ACKNOWLEDMENTS	:	
- INSTITUTION       	:   Risoe National Laboratory 
- Department    :    WindEnergy and Atmosheric Physics Department
- ADDRESS 	        :   Postbox 49, DK-4000, Roskilde Denmark
- TEL/FAX 	        :   +45 46677 5012 / +45 4677 5970
- CONTACTS 	        :  Soeren Larsen 
- LINKS 	        :   http://www.risoe.dk/vea/ / http.//www.storebaelt.dk/
- COLLABS 	        :   A/S Storebaeltsforbindelsen
- FUND AGENTS       	:   A/S Storebaeltsforbindelsen, contract no. 413.85
- PERIOD 	        :   1990-06-01 - 1991-06-15
- Naming_authority  	: 'DTU Data'
- DOI               	: 'https://doi.org/10.11583/DTU.14494380'


## PUBLICATIONS	    : 

## Public data
Run statistics (NetCDF format);
1) sprogoe_all.nc 
2) sprogoe_concurrent.nc 

Raw time series (ascii) each with a duration of 600 sec, sampled with 2 and 10 Hz:
3) sprogoe.zip
