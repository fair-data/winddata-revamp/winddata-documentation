## Background Information

- CLASSIFICATION: flat(flat landscape), offshore(open sea)
- COUNTRY : Denmark
- ALTITUDE : 0 [m]
- POSITION : [54 35'50.41'' N 11 43'25.97''E](https://geohack.toolforge.org/geohack.php?pagename=roedsand&params=54_35_50.41_N_11_43_25.97_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This dataset consists of time series and statistics of wind speed and wind direction measurements from one 45m offshore mast at Roedsand, Denmark. The period includes more than 600 hours of measurements from 1997-1998.

## Map
![A map of Denmark](./ts_wind/roedsand/map_01.gif)
**Figure 1:** A map of Denmark

![A map of Denmark](./ts_wind/roedsand/msmap.gif)
**Figure 1:** Location of Gedser Rev SE to Gedser.


## Photos
![](./ts_wind/roedsand/photo_01.gif) **Photo 01:** Roedsand mast photo

## Drawings
![](./ts_wind/roedsand/layout.gif) **Drawing 01:** Site layout. 
![](./ts_wind/roedsand/draw_01.gif) **Drawing 02:** layout for the  masts  

## Reports
## Reports
1. [Description of measurement setup - pdf file](./ts_wind/roedsand/Site_documentation.pdf)
1. [Description of the Rødsand field measurement, Risø-R-1268 (EN)](./ts_wind/roedsand/ris-r-1268.pdf)

## Mast (relative positions with reference to the reference POSITION)
1. 45 [m] (0,0,0)

## Windturbines
NA

## Project description
 Characterising wind and turbulence for offshore wind energy development.

## Measurement system
The mast at Roedsand has been instrumented with 3 cup anemometers, 1 vane and 1 sonic anemometer together with several thermometers.

The data acquisition system DAQ has used for data recording (5 and 20 Hz) and was operated by Rebecca Barthelmie and Peter Zanderhoff, Risoe National Laboratories.

The measured time series from Roedsand / Gedser Syd has been collected, validated and provided by: Senior Scientist, Rebecca Barthelmie Ph.D, Email: r.barthelmie@risoe.dk Department Wind Energy and Atmosphere Physics, Riso National Laboratory, 4000 Roskilde, Denmark.

Instrumentation, operation and maintenance at Roedsand is funded by SEAS Distribution A.m.b.A.

1. [List of mast signals](./ts_wind/roedsand/ts_mast_signals.csv)

2. [List of additional signals](./ts_wind/roedsand/add_signals.csv) Note: The statistics for these signals are only included in the header of the packed ASCII file.

## Nominal values

![](./ts_wind/roedsand/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/roedsand/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/roedsand/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wind/roedsand/w_dist.gif)
**Figure n4:** Nominal wind speed distribuion.

![A map of Denmark](./ts_wind/roedsand/ti_dist.gif)
**Figure n5:** Nominal turbulence distribuion.

![A map of Denmark](./ts_wind/roedsand/dir_dist.gif)
**Figure n6:** Nominal wind direction distribuion. 

## ACKNOWLEDMENTS	:	
- INSTITUTION       	:   Risoe National Laboratory
- ADDRESS 	        :   Postbox 49, DK-4000, Roskilde Denmark
- TEL/FAX 	        :   +45 4677 5017 / +45 4675 5619
- CONTACTS 	        :  Rebecca Barthelmie
- LINKS 	        :   http://www.risoe.dk/vea/
- COLLABS 	        :   SEAS Wind Energy Centre, Haslev
- FUND AGENTS       	:   Danish Ministry & ELKRAFT
- PERIOD 	        :   1997-01-01 - 2001-12-31
- Naming_authority  	: 'DTU Data'
- DOI               	: 'https://doi.org/10.11583/DTU.14494788'

## PUBLICATIONS	    : 
NA
## Public data
1) roedsand_all.nc (NetCDF)
2) roedsand_concurrent.nc (NetCDF)

Raw time series each with a duration of 600 sec, sampled with 5 and 20 Hz:
roedsand.zip
