## Background Information
- CLASSIFICATION: flat(flat landscape), pastoral(open fields and meadows)
- COUNTRY : France
- ALTITUDE : 362 [m]
- POSITION : [47˚ 46' 33.8'' N 5˚ 39'4.9'' E](https://geohack.toolforge.org/geohack.php?pagename=larosa&map_47_46_33.8_N_5_39_4.9_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This resource dataset consists of 10-minute statistics of wind speed and wind direction measurements from a small met mast at La Rosa, Pressigny, in France. The period includes 4 years of measurements, which starts in 2002.

## Map
![A map of Denmark](./Resource/larosa/msmap.gif)
**Figure 1:** A map of Eastern France

## Photos
![](./Resource/larosa/map_01.jpg) **Photo 01:** Aeral foto of location in eastern part of France.

## Mast (relative positions with reference to the reference POSITION) 
1. 30 [m] (0,0,0)

## Project description
Wind resource measurements in France and Greece.
## Measurement system
30 mast with a logger. 

1. [List of mast signals](./Resource/larosa/mast_signals.csv)


## Distributions
![A map of Denmark](./Resource/larosa/w_dist.png)
**Figure n1:** Nominal wind speed distribuion.

![A map of Denmark](./Resource/larosa/dir_dist.png)
**Figure n3:** Nominal wind direction distribuion. 

## ACKNOWLEDMENTS	:	Arnulf Knittel
- INSTITUTION       :	Hrafnkel SARL
- ADDRESS 	        :   13 rue de savigny, F52500 Pressigny, France
- TEL/FAX 	        :   +33 325 0987 73
- CONTACTS 	        :   Arnulf Knittel
- PERIOD 	        :   2002 - 2006
- Naming_authority  	: 'DTU Data'
- DOI               	: 'https://doi.org/10.11583/DTU.14153243'


## PUBLICATIONS	    : 
1. 

## Public data
1. Resource data  (NetCDF) 


