## Background Information
- CLASSIFICATION: hill(rolling hills), scrub(bushes and small trees)
- COUNTRY : US
- ALTITUDE : 1390 [m]
- POSITION : [35˚ 2' 32.64'' N 118˚ 22'5.54'' E](https://geohack.toolforge.org/geohack.php?pagename=oakcreek&params=35_2_32.64_N_118_22_5.54_W_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This dataset consists of raw time series and statistics of wind speed and wind direction measurements from two met mast located in Oakcreek, CA, US. The period includes more than 3700 hours of measurements, which starts in 1998.

## Map

![A map of Denmark](./ts_wind/oakcreek/msmap.gif)
**Map 2:** Location of Oakcreek, Tehachapi, California
![A map of Denmark](./ts_wind/oakcreek/map_1.gif)
**Map 2:** Contour map for Oak Creek, Tehachapi, California

## Photos
![](./ts_wind/oakcreek/photo_1.gif) **Photo 01:** 2 Met. masts and wind turbines
![](./ts_wind/oakcreek/photo_2.gif) **Photo 01:** Met. masts seen for turbine hub
![](./ts_wind/oakcreek/r3r3a_lrg.jpg) **Photo 01:** Gill Solent anemometer


![A map of Denmark](./ts_wind/oakcreek/map_01.gif)
**Map 3:** Geographical location of all 11 TCS sites in USA


## Drawings
![](./ts_wind/oakcreek/layout.gif) **Drawing 01:** Site layout

![](./ts_wind/oakcreek/r3_drawing_lrg.jpg) **Drawing 02:**Gill Solent anemometer

## Reports
1. [Description of measurements setup - pdf file](./ts_wind/oakcreek/oakcreek.pdf)

## Mast (relative positions with reference to the reference POSITION) 
1. 78 [m] (-18.2,26.2,1)
2. 78 [m] (-5.5,48.3,1)

# Wind turbines
1. NEG-Micon 650kW 650 [kW] (0,0,1)
2. NEG-Micon 650kW 650 [kW] (26.5,45.9,1)
3. NEG-Micon 650kW 650 [kW] (67.5,80.4,1)

## Project description
Verification of the structural integrity of a wind turbine involves analysis of fatigue loading as well as ultimate loading. With the trend of persistently growing turbines, the ultimate loading seems to become relatively more important. For wind turbines designed according to the wind conditions prescribed in the IEC-61400 code, the ultimate load is often identified as the leading load parameter. The objective of the Oak Creek project is to conduct a combined experimental and theoretical investigation of blade-, rotor- and tower loads caused by extreme wind load conditions occuring during normal operation as well as in stand still situations (where mean wind speeds exceeds the cut-out wind speed), with the purpose of establishing an improved description of the ultimate loading of three bladed pitch- and stall controlled wind turbines.

## Measurement system
The measurements are performed in a wind farm situated at a high wind site in Oak Creek, near Tehachapi in California. The wind farm consists of wind turbines erected on a ridge in a very complex terrain. The prevailing wind direction is 320 degrees and thus perpendicular to the ridge. The turbines are closely spaced - the inter turbine spacing is 53 m, corresponding to approximately 1.2 rotor diameters. The wind turbines are tree bladed NECMicon 650 kW stall regulated turbines with hub heights and rotor diameters equal to 55 m and 44 m, respectively. The wind field is measured from two 80 m heigh meteorological towers erected less than one rotor diameter in front of one of the wind turbines (in the direction of the prevailing wind direction), and the distance between the two meteorological towers is 25.5 m, corresponding to 0.58 rotor diameters. Thus, detailed information of the inflow field to the particular turbine rotor is provided. The layout of the wind farm and the measurement setup is illustrated. The instrumentation of the meterological towers included sensors at multiple levels. Basically, similar instruments on each of the two masts have been installed in roughly the same level relative to the terrain level. The monitoring system is running continously, and the data are reduced and stored as 10-minutes statistics suplemented with intensive time series recordings covering periods where the mean wind speeed exceeds a specified threshold (15 m/s). Consequently, there are time gaps in the the time series. The monitoring sample rate is 32 Hz. Detailed information on the individual sensors is provided from the Master Sensor List. In general, all specified instrument heights are given relative to the base of the relevant meterological tower.

1. [List of mast signals ](./ts_wind/oakcreek/ts_mast_signals.csv)

2. [List of additional signals](./ts_wind/oakcreek/add_signals.csv) Note: The statistics for these signals are only included in the header of the packed ASCII file.

## Nominal values

![](./ts_wind/oakcreek/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/oakcreek/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/oakcreek/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wind/oakcreek/w_dist.gif)
**Figure n4:** Nominal wind speed distribution.

![A map of Denmark](./ts_wind/oakcreek/ti_dist.gif)
**Figure n5:** Nominal turbulence distribution.

![A map of Denmark](./ts_wind/oakcreek/dir_dist.gif)
**Figure n6:** Nominal wind direction distribution. 

## ACKNOWLEDMENTS	:	 
- INSTITUTION       :	Risoe National Laboratory
- ADDRESS 	        :   Department of Wind Energy and Atmospheric Physics 
- CONTACTS 	        :  Gunner C. Larsen / Soeren.M.Petersen
- LINKS 	        :  http://www.risoe.dk/vea/
- FUND AGENTS       :  Danish Ministry of Energy 
- PERIOD 	        :  1997 - 1999 
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14381273'



## PUBLICATIONS	    : 
NA

## Public data
1. Run statistics; oakcreek.nc (NetCDF) 
2. Raw time series = oakcreek.zip (ascii) each with a duration of 600 sec and sampled with 8 and 16 Hz.


