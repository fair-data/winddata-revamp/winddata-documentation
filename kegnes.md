## Background Information
- CLASSIFICATION: flat(flat landscape), coastal(water and land)
- COUNTRY : Denmark
- ALTITUDE : 0 [m]
- POSITION : [54˚ 51' 18.43'' N 9˚ 56'6.22'' E](https://geohack.toolforge.org/geohack.php?pagename=kegnes&params=54_51_18.43_N_9_56_10.6.22_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This resource dataset consists of 10-minute wind speed and wind direction measurements from the met mast at Kegnes, Als in Denmark. The period includes 11 years of measurements, which starts in 1991.

## Map
![A map of Denmark](./Resource/kegnes/msmap.gif)
**Figure 1:** A map of Denmark

## Photos
![](./Resource/kegnes/kegnaes_large.jpg) **Photo 01:** Picture of the met mast.

## Mast (relative positions with reference to the reference POSITION) 
MASTS
1. 23 [m] (0,0,0)

## Project description
The meteorologist of Risø have since the start of Risø in 1956 collected climatological data from measuring masts both within Denmark and outside the country. The data have mostly been collected to obtain relevant climate statistics for the locality in question. Examples can be: Establishment of a climatology of atmospheric dispersion for a planned site for a power plant or an other production facility. Estimation of the the local windspeed distribution for evaluation of wind power resource or of the extreme wind load on structures and buildings. Description of seasonal and long term exchange between the atmosphere and different types of vegetation. The data have also proven useful to in connection with events of various kinds, such as tracking storm surges across the country, estimating contaminated regions for transient events as different as nuclear and industrial accidents and air borne diseases.

## Measurement system
 Data from the Risø sites consist typically of atmospheric pressure and wind direction, wind speed in several heights, and temperature in at least two heights, such that climatology for wind speed and direction, wind variation with height and thermal stability can be established. At some stations also measurements of humidity, incoming solar radiation, and wind variability are obtained. Typically the measurements consist of ten minuttes averages recorded every ten minuttes. Details of the data are reported on the individual pages for each measuring station. Further information Further information can be obtained from Gunnar Jensen, the department of Wind Energy and Atmospheric Physics, Risø National Laboratory. Phone +4546775007. E-mail: gunnar.jensen@risoe.dk

1. [List of mast signals](./Resource/kegnes/mast_signals.csv)

## Distributions
![A map of Denmark](./Resource/kegnes/w_dist.png)
**Figure d1:** Nominal wind speed distribuion.

![A map of Denmark](./Resource/kegnes/dir_dist.png)
**Figure d2:** Nominal wind direction distribuion. 

## ACKNOWLEDMENTS		
- INSTITUTION       :	Risoe National Laboratories
- ADDRESS 	        :   Post box 49, DK4000 Roskilde, Denmark
- TEL/FAX 	        :   +45 46775007 / +45 46755619
- CONTACTS 	        :   Gunner Jensen / Gunner Larsen
- LINKS 	        :   http://www.risoe.dk/vea-data/ / http://www.risoe.dk/vea/
- FUND AGENTS       :   Internal
- PERIOD 	        :   1956-01-01 - 2001-12-31
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14135618'

## PUBLICATIONS	    : 
1. 
## Public data
1. Resource data  (NetCDF) 


