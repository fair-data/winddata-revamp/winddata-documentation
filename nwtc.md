## Background Information
- CLASSIFICATION: flat(flat landscape), pastoral(open fields and meadows)
- COUNTRY : US 
- ALTITUDE : 1855 [m]
- POSITION : [39˚ 54' 42.57'' N 105˚ 13'56.69'' E](https://geohack.toolforge.org/geohack.php?pagename=nwtc&params=39_54_42.57_N_105_13_56.69_W_)
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This dataset consists of 10 min statistics and 1 Hz raw turbulence array measurements from a 24 hours array measurement campaign, conducted on 3 mast at NWTC in Boulder, US.

## Map
![A map of Denmark](./ts_wind/nwtc/msmap.gif)
**Map 1:** Map of test field area near Denver, CO, USA
## Drawings

![](./ts_wind/nwtc/layout.gif) **Drawing 01:** Layout of 3 met. masts (10kB).
![](./ts_wind/nwtc/draw_1.jpg) **Drawing 02:** Array layout (29 kB)

## Photos
![](./ts_wind/nwtc/kaijo.jpg) **Phote 1:** Kaijo DA-600 sonic anemometer

![](./ts_wind/nwtc/photo_1.jpg) **Phote 2:** Downwind view of array and turbine (239kB)
![](./ts_wind/nwtc/photo_2.jpg) **Phote 3:** Upwind view of array and turbine (98kB)
![](./ts_wind/nwtc/photo_3.jpg) **Phote 4:** Instrumentation and Computer rack (160kB)
![](./ts_wind/nwtc/photo_4.jpg) **Phote 5:** Rack of Sonic Translators (143kB)

![](./ts_wind/nwtc/photo_5.jpg) **Phote 6:** LIST Installation, Sideways Orientation (190kB)
![](./ts_wind/nwtc/photo_6.jpg) **Phote 7:** 65G - 3 levels (90kB) (143kB)
![](./ts_wind/nwtc/photo_7.jpg) **Phote 8:** Direction 292 deg. in upwind direction with sonic array in front of the right WTGS (1.9MB)
## Reports
NA

## Mast (relative positions with reference to the reference POSITION) 
1. 61 [m] (0,0,0)
2. 37 [m] (10.5,18.2,0)
3. 37 [m] (-10.5,-18.2,0)

## WIND TURBINES
1. NREL ART_600kW 600 [kW] (54.6,-31.5,0)

## Project description
The accurate numerical dynamic simulation of new large-scale wind turbine designs operating over a wide range of inflow environments is critical because it is usually impractical to test prototypes in a variety of locations. Large turbines operate in a region of the atmospheric boundary layer that currently may not be adequately simulated by present turbulence codes.
The objective of this experiment is to obtain simultaneously collected turbulence information from the inflow array and the corresponding structural response of the turbine. The turbulence information will be used for comparison with that predicted by currently available codes and establish any systematic differences. These results will be used to improve the performance of the turbulence simulations. The sensitivities of key elements of the turbine aeroelastic and structural response to a range of turbulence-scaling parameters will be established for comparisons with other turbines and operating environments. In this paper, we present an overview of the experiment, and offer examples of two observed cases of inflow characteristics and turbine response collected under daytime and nighttime conditions, and compare their turbulence properties with predictions.

## Measurement system
The inflow instrumentation was mounted on three towers located 1.5-rotor diameters upstream of the turbine rotor. A total of five high-resolution Kaijo Model DA-600 ultrasonic anemometers/thermometers, which have a 10-Hz data bandwidth and a minimum resolution of 0.005 m/s or less, were deployed. In addition, cup anemometers and wind vanes were installed on the 61-m central tower at three levels, along with air temperature, fastresponse temperature, temperature difference between 3 and 61 m, and dew point temperature sensors. Barometric pressure was measured at a height of 3 m. GPS-based time was recorded to a resolution of 1 millisecond. The raw data was collected at rate of 40 samples per second.


1. [List of mast signals (ts_wind data)](./ts_wind/nwtc/ts_mast_signals.csv)

2. [List of additional signals](./ts_wind/nwtc/add_signals.csv) Note: The statistics for these signals are only included in the header of the packed ASCII file.

## Nominal values

![](./ts_wind/nwtc/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/nwtc/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/nwtc/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wind/nwtc/w_dist.gif)
**Figure n4:** Nominal wind speed distribution.

![A map of Denmark](./ts_wind/nwtc/ti_dist.gif)
**Figure n5:** Nominal turbulence distribution.

![A map of Denmark](./ts_wind/nwtc/dir_dist.gif)
**Figure n6:** Nominal wind direction distribution. 

## ACKNOWLEDMENTS	:	Neil Kelley / Maureen Hand 
- INSTITUTION       :	National Wind Technology Center (NWTC) at National Renewable Energy Laboratory (NREL)
- ADDRESS 	        :   1617 Cole Blvd, Golden, CO
- TEL/FAX 	        :   303-384-6902
- CONTACTS 	        :   Neil Kelley / Maureen Hand
- LINKS 	        :   http://www.nrel.org/
- FUND AGENTS       :   Department of Energy's (DOE) Pacific & Laboratory (PNL).
- PERIOD 	        :   2000-12-17
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.18321797'


## PUBLICATIONS	    : 
1. [The NREL Large-Scale Turbine Inflow and Response Experiment ― Preliminary Results](./ts_wind/nwtc/AIAA2002.pdf)

## Public data
1. Run statistics; nwtc.nc (NetCDF) 
2. Raw time series; nwtc.zip (zip)

