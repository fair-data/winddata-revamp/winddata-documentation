## Background Information
- CLASSIFICATION: flat(flat landscape), sand(sand cover)
- COUNTRY : Egypt
- ALTITUDE : 0 [m]
- POSITION : [27˚ 18' 21'' N 33˚ 42'51'' E](https://geohack.toolforge.org/geohack.php?pagename=hurghada&params=27_18_21_N_33_42_51_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary

This resource dataset consists time series of wind speed and wind direction measurements from two masts at Hurghada WF, Egypt. The period includes more than 1350 hours of measurements, which starts in 2000.

## Map
![A map of Denmark](./ts_wind/hurghada/msmap.gif)
**Figure 1:** Location of Hurghada in Egypt.
![A map of Denmark](./ts_wind/hurghada/map_02.gif)
**Figure 2:** Location of Hurghada area in the bottom of Gulf of Suez.


## Photos
![](./ts_wind/hurghada/photo_01.jpg) **Photo 01:** Photo of 3 Nordtank wind turbines and 2 met. masts.

![](./ts_wind/hurghada/photo_02.jpg) **Photo 02:** Photo of near by wind turbines - 1.

![](./ts_wind/hurghada/photo_03.jpg) **Photo 03:** Photo of near by wind turbines - 2 (Ventis).

![](./ts_wind/hurghada/photo_04.jpg) **Photo 04:** Photo of near by wind turbines - 3 (Ventis).

![](./ts_wind/hurghada/photo_05.jpg) **Photo 05:** Photo of near by wind turbines - 4 (Ventis).

## Drawings
![](./ts_wind/hurghada/draw_01.gif) **Draw 01:** Location of Hurghada WETC.
![](./ts_wind/hurghada/layout.gif) **Draw 02:** Sensor layout on met. mast.


## Mast (relative positions to the reference POSITION) 

1. 30 [m] (0,0,0)
2. 30 [m] (26,86,0)
3. 25 [m] (0,498,0)

## WIND TURBINES
1. Nordtank 300 kW 300 [kW] (-75,27,0)
2. Nordtank 300 kW 300 [kW] (-50,195,0)
3. Nordtank 300 kW 300 [kW] (-34,195,0)

## Report
NA

## Project description
Characterising wind and turbulence for a desert location equipped with wind turbines during stable conditions. The measurements was ordered for a 2 month period.

## Measurement system
Two standard meteorological masts with a height of 30 m, each equipped with 2 cups and one vane has been used. Signals has been recorded continiuosly with 8 Hz. The stability measurements are recorded on a third mast equipped with Anderaa meteorological instrumentation. Instrumentation and measurement setup has been done in coorperation between Risø National Laboratories and Hurghada Wind Technology Center and the equipment used are identical to standard Risø measurement setup. Monitoring of the measurement program has been done by the staff at the Hurghada Wind Energy Technology Center. Note: Each met. mast has its own measurement computer and it is been necessary to merge all the time series but due to errors in the synchronisation (+/- 60 seconds) some signals appr. 5% are not valid. Acknowledgements: Staff at the Hughhada Wind Energy Technology Center, Hurghada, Egypt.

1. [List of mast signals (ts_wind data)](./ts_wind/hurghada/ts_mast_signals.csv)
2. [List of additional signals](./ts_wind/hurghada/add_signals.csv) Note: The statistics for these signals are only included in the header of the packed ASCII file.

## Nominal values

![](./ts_wind/hurghada/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/hurghada/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/hurghada/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wind/hurghada/w_dist.gif)
**Figure n4:** Nominal wind speed distribution.

![A map of Denmark](./ts_wind/hurghada/ti_dist.gif)
**Figure n5:** Nominal turbulence distribution.

![A map of Denmark](./ts_wind/hurghada/dir_dist.gif)
**Figure n6:** Nominal wind direction distribution. 

## ACKNOWLEDMENTS	:	 
- INSTITUTION       :  Risoe National Laboratory
- ADDRESS 	        :  Post box 49, DK-4000 Roskilde, Denmark 
- CONTACTS 	        :  Uwe Paulsen / Gunner C. Larsen 
- LINKS 	        :  http://www.risoe.dk/vea/
- COLLABS 	        :	Hurghada Wind Energy Technology Center, Egypt
- FUND AGENTS       :  Danish Ministry of Energy 
- PERIOD 	        :  1997 - 1999 
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14401889'

## PUBLICATIONS	    : 
NA

## Public data
1. Run statistics; hurghada.nc (NetCDF) 
2. Raw time series = hurghada.zip (ascii) each with a duration of 600 sec and sampled with 8 Hz.
