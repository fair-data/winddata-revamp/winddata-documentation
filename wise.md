## Background Information
- CLASSIFICATION: flat(flat landscape), pastoral(open fields and meadows)
- COUNTRY : US
- ALTITUDE : 1015 [m]
- POSITION : [33˚35' 54.16'' N102˚ 0'25.09'' E](https://geohack.toolforge.org/geohack.php?pagename=wise&params=33_35_54.16_N_102_0_25.09_W_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This resource dataset consists of hourly statistics of wind speed and wind direction measurements from the tall WISE met mast in Lubbock, TX, US. The period includes more than 7500 hours of measurements, which starts in 2005.

## Map
![Local site map](./Resource/wise/tower_location.gif)
**Figure 1:** Location of the tall WISE mast, west of Lubbock,TX

## Photos
![](./Resource/wise/fig01.jpg) **Photo 01:** Tower arrangement, platform and top

## Drawings
![](./Resource/wise/fig02.jpg) **Drawing 01:** Instrumentation - elevation scheme

![](./Resource/wise/fig03.jpg) **Drawing 02:** Typican instrumentation - Detail

## Mast (relative positions with reference to the reference POSITION) 
1. 200 [m] (0,0,0)

## Project description
The WISE data consists 1 hours mean values available for wind shear and direction analysis, stored as resource data.

## Measurement system
Research facilities - 200m Tower: A 200 m data acquisition tower is available to measure and record atmosperic conditions at ten levels, with a variety of instruments to include sonic and u-v-w anemometers providing wind profile and perticulate data.

1. [List of mast signals](./Resource/wise/mast_signals.csv)


## ACKNOWLEDMENTS	

- ACKNOWLEDMENTS	:	Kevin Walters, Texas Tech Wind Science 
- INSTITUTION       :	Wind Science and Engineering Research Center 
- ADDRESS 	        :   10th and Akron, Lubbock, TX 79409
- TEL/FAX 	        :   
- CONTACTS 	        :   K.Walter / Dr. Andy Swift
- LINKS 	        :   http://www.wind.ttu.edu/index.php/ 
- PERIOD 	        :   2005 - 2007
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14236640'


## PUBLICATIONS	: 
1. [WIND POWER SYSTEMS IN THE STABLE NOCTURNAL BOUNDARY LAYER](./Resource/wise/Walter_Kevin_Diss.pdf)
2. [OBSERVATIONS OF EXTREME SPEED AND DIRECTIONAL WIND SHEAR IN THE US GREAT PLAINS](./Resource/wise/Extreme.pdf)

## Public data
1. Resource data (NetCDF) 

