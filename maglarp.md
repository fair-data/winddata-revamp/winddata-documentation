## Background Information
- CLASSIFICATION: flat(flat landscape), rural(agriculture with some buildings)
- COUNTRY : Sweden
- ALTITUDE : 18 [m]
- POSITION : [55˚ 23' 15.41'' N 13˚ 4'13.12'' E](https://geohack.toolforge.org/geohack.php?pagename=maglarp&params=55_23_15.41_N_13_4_13.12_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)


## Short summary
This dataset consists of raw data and 10-minute statistics of wind speed and wind direction measurements from a 120m mast near Maglarp, Sweden. The period includes more than 40 hours of measurements, from 1981.

## Map
![A map of Denmark](./ts_wind/maglarp/mapquest.gif)
**Map 1:** Location of Maglarp in Sweden

## Photos
![](./ts_wind/maglarp/photo_1.gif) **Photo 01:** Photo towards east, with the mast in the centre

![](./ts_wind/maglarp/photo_2.gif) **Photo 02:** The wind turbine - WTS3

## Drawings
NA

## Reports
NA

## Mast (relative positions with reference to the reference POSITION) 
1. 10 [m] (0,0,0)

## WIND TURBINE
1. Hamilton Standard WTS3 3 [MW]

## Project description
The project has been initiated as part of a larger measurement program on the WTS-3 wind turbine. The site is located on a low sloping hill in sligthly rolling farm land.

## Measurement system
20 Hz data data are recorded at 2 levels (13 & 84 m) on a 120 m mast during one month. The instrumentation consists only of 3-component hotwires. IMPORTANT sh1,sh2 - derived speeds dh1,dh2 - derived directions.

1. [List of mast signals (time series of wind speed)](./ts_wind/maglarp/ts_mast_signals.csv)

## Nominal values

![](./ts_wind/maglarp/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/maglarp/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/maglarp/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wind/maglarp/w_dist.gif)
**Figure n4:** Nominal wind speed distribution.

![A map of Denmark](./ts_wind/maglarp/ti_dist.gif)
**Figure n5:** Nominal turbulence distribution.

![A map of Denmark](./ts_wind/maglarp/dir_dist.gif)
**Figure n6:** Nominal wind direction distribution. 

## ACKNOWLEDMENTS	:	 
- INSTITUTION       :	Dept. of Meteorology, Uppsala University
- ADDRESS 	        :   Uppsala University, Box 516 S-751 20 Uppsala, Sweden
- TEL/FAX 	        :   +46-18 542792 / +46-18 544 706
- CONTACTS 	        :   Mikael Magnusson / Ann-Sofi Smedman
- LINKS 	        :   http://www.met.uu.se/
- PERIOD 	        :   1981 
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.15028671'

## PUBLICATIONS	    : 
NA

## Public data
1. Run statistics; maglarp.nc (NetCDF) 
2. Raw time series = maglarp.zip (ascii), duration of 600 sec and sampling frequency = 20 Hz (3 component hotwires).


