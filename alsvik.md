## Background Information
- CLASSIFICATION: flat(flat landscape), coastal(water and land)
- COUNTRY : Sweden
- ALTITUDE :  0 [m]
- POSITION : [57˚ 4' 48.06'' N 18˚ 10'15.27'' E](https://geohack.toolforge.org/geohack.php?pagename=alsvik&params=57_8_48.06_N_18_10_15.27_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This dataset consists of time series and 10-minute statistics of wind speed and wind direction measurements from a two met mast at Alsvik WF, Gotland Sweden. The period includes more than 3 years of measurements, which starts in 1992.

## Map
![A map of Denmark](./ts_wind/alsvik/msmap.gif)
**Figure 1:** A map of Gotland, Sweden
![A map of Denmark](./ts_wind/alsvik/map_1.gif)
**Figure 1:** Site layout, consisting of 2 masts and 4 wind turbines

## Photos
![](./ts_wind/alsvik/cup_vanes.jpg) **Photo 01:** Combine cup-vanes used for the Alsvik measurements

## Drawings
![](./ts_wind/alsvik/layout.gif) **Drawing 01:** Layout of the 135m tall mast and 2 wind turbines.
![](./ts_wind/alsvik/site_layout.gif) **Drawing 02:** Location of turbines and mast.

## Mast (relative positions with reference to the reference POSITION) 
1 54 [m] (0,0,0)
2 54 [m] (100,-50,0)

## Windturbines
1. Danwin 180kW 180 [kW] (0,70,2)
2. Danwin 180kW 180 [kW] (10,-50,2)
3. Danwin 180kW 180 [kW] (12,-120,2)
4. Danwin 180kW 180 [kW] (115,-30,2)

## Project description
Meteorological mesurements from flat, coastal site on Gotland equipped with four wind turbines.

## Measurement system
 1 Hz data at eight levels on two 54 m towers during four years. 20 Hz data at three levels on one 54 m tower during one year.

1. [List of mast signals](./ts_wind/alsvik/mast_signals.csv)

2. [List of additional signals](./ts_wind/alsvik/add_signals.csv) Note: The statistics for these signals are only included in the header of the packed ASCII file.

## Nominal values

![](./ts_wind/alsvik/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/alsvik/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/alsvik/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wind/alsvik/w_dist.gif)
**Figure n4:** Nominal wind speed distribution.

![A map of Denmark](./ts_wind/alsvik/ti_dist.gif)
**Figure n5:** Nominal turbulence distribution.

![A map of Denmark](./ts_wind/alsvik/dir_dist.gif)
**Figure n6:** Nominal wind direction distribution. 

## ACKNOWLEDMENTS		
- INSTITUTION       :	Dept. of Meteorology, Uppsala University
- ADDRESS 	        :   Uppsala University, Box 516 S-751 20 Uppsala, Sweden
- TEL/FAX 	        :   +46-18 542792 / +46-18 544 706
- CONTACTS 	        :   Mikael Magnusson / Ingemar Carlen
- LINKS 	        :   http://www.met.uu.se/
- COLLABS 	        :   FFA & Teknikgruppen AB, S-19121 Sollentona
- PERIOD 	        :   1990-01-01 - 1994-12-31
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14237138'

## PUBLICATIONS	    : 
1. [wake effects in Alsvik wind park](./ts_wind/alsvik/alsvik_report.pdf)
2. [A new approach for evaluating measured wake data](./ts_wind/alsvik/alsvik_report_2.pdf)
3. [Fatigure loads on wind turbine blades in a wind farm](./ts_wind/alsvik/FFA_TN1992-21.pdf)


## Public data
1. Run statistics; alsvik.nc (NetCDF) 
2. Raw time series each with a duration of 3600 sec and sampled with 1 Hz.


