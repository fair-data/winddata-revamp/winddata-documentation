# Winddata.com on DATA.DTU

Winddata.com was previously hosted on a Risø DTU but the mai n contenst has been transferred to DATA.DTU.

## Resource data 
- Turbulence measurements: [data collection](https://data.dtu.dk/account/collections/5405286)
    includes all long time resource measurements defined as 10-minute statistics. 

## Turbulence measurements
- Turbulence measurements: [data collection](https://data.dtu.dk/account/collections/5405292)

## Wind turbine load measurements

## Wind farm measurements 

## SCADA data & Project data (meta)
SCADA Data: This document list a number of internal database with restricted access hosted by DTU Wind Energy. The databases are accessible with reference to NDA contract between the data provider and DTU Wind Energy.
IMPORTANT Access to these database are restricted to employees at DTU Wind Energy. 
project Data: This document list a number of internal project database hosted by DTU Wind Energy. The database accessible depend on the project managers permission.

IMPORTANT Access to these database are usually restricted to employees at DTU Wind Energy.   