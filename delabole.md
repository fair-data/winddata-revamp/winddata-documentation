## Background Information
- SITE : Delabole 
- CLASSIFICATION: flat(flat landscape), pastoral(open fields and meadows)	
- COUNTRY 	  	:	UK
- ALTITUDE 	  	:	240 [m]
- POSITION  	:	[50 37' 49.77'' N 4. 42' 30.51''w](https://tools.wmflabs.org/geohack/geohack.php?pagename=delabole&params=50_37_49.77_N_4_42_30.51_w_)
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This dataset includes SCADA measurements from 10 wind turbines and meteorological measurements from an onshore wind farm, located in Delabole, UK.
The measurements are stored as 10-minute statistitics and consists of 6800 hours recorded during the period 01-05-1993 until 30-04-1994.

## Map
![](./wind_farm/delabole/map_01.gif)
**Figure 1:** Location of Delabole wind Farm - 1

![](./wind_farm/delabole/msmap.gif)
**Figure 2:** Location of Delabole wind Farm - 2

![](./wind_farm/delabole/map_02.gif)
**Figure 3:** Layout of Delabole wind Farm

![](./wind_farm/delabole/layout.gif)
**Figure 4:** Instruments at Delabole wind Farm

![](./wind_farm/delabole/delabole.gif)
**Figure 5:** Schematics layout of Delabole wind Farm

## Photos
![](./wind_farm/delabole/photo_01.jpg) **Photo 01:** wind farm picture - 1.

![](./wind_farm/delabole/photo_02.jpg) **Photo 02:** wind farm picture - 2.

![](./wind_farm/delabole/photo_03.gif) **Photo 03:** VECTOR Instruments A100 Cup anemometer.

## Graphs
![](./wind_farm/delabole/v44.gif) **Graph 01:** wind speed at mast #1, h=44 m, during monitoring period
![](./wind_farm/delabole/ppower.gif) **Graph 02:** windfarm park power during monitoring period
![](./wind_farm/delabole/pcurve.gif) **Graph 03:** windfarm power curve, all wind speeds, mast #1, h=44 m

## Drawings
![](./wind_farm/delabole/map_03.gif) **Drawing 01:** Data acquisition system schematic.

## Reports
1. [Description of Delabole wind Farm Construction](./wind_farm/delabole/caddet.pdf) 

2. [VECTOR Instruments A100 Cup anemometer - specifications](./wind_farm/delabole/ds-a100.pdf) 
3. [VECTOR Instruments w200 wind vane specifications](./wind_farm/delabole/ds-w200.pdf) 

4. [Delabole wind Farm Technical Performance Analysis MAY 1993 - APRIL 1994](./wind_farm/delabole/report.pdf) 

## Mast
1.  44 [m] (0,0,0)
2.  33 [m] (473,907,0)

## windturbines
1. winddane wD34 400 [kw] (290,868,0)
2. winddane wD34 400 [kw] (45,774,0)
3. winddane wD34 400 [kw] (510,711,0)
4. winddane wD34 400 [kw] (290,613,0)
5. winddane wD34 400 [kw] (419,432,0)
6. winddane wD34 400 [kw] (187,400,0)
7. winddane wD34 400 [kw] (-19,226,0)
8. winddane wD34 400 [kw] (335,161,0)
9. winddane wD34 400 [kw] (81,0,0)
10. winddane wD34 400 [kw] (323,-77,0)

# wIND TURBINE TYPE : winddane wD34

- RATED_POwER 	  	:	400 [kw] at 13 [m/s]
- HUB_HEIGHT 	  	:	32 [m]
- ROTOR_DIAMETER	:	38.8 [m]
- DESCRIPTION 	  	:	 
- PHOTO 	  		:

## Project description
In August 1991 North Cornwall District Council granted planning permission for the UK's first wind farm at Deli Farm near Delabole in North Cornwall. The wind farm was commissioned in December 1991 and was the subject of much local and national interest. ETSU, acting for the Department of Trade and Industry (DTI), commissioned a series of studies into the impact, performance and public acceptability of the farm. The Delabole wind Farm is situated about 2 km to the North west of Camelford in North Cornwall and is operated by wind Electric Ltd. The site is very open with an approximate elevation of 240 m above sea level, gently sloping away towards the South?west. It is bounded to the North?East )by a minor road (B3314) and the South and East by the course of an abandoned railway. Other than the Cornish hedges (5 ? 6 ft) around fields, the ground cover is low (1 ? 2 ft) agricultural crops, mostly grass with some fields put to linseed. At certain periods of the year this area is ploughed or contains stubble. There are ten wind turbines installed at the Delabole wind Farm. They are all windane 34's manufactured by winddane / Danish wind Technology A/S, rated at 400 kw and were commissioned in December 1991. References: Sumner,J.; Masson,C. "Influence of Atmospheric Stability on wind Turbine Power Performance Curves" Journal of Solar Energy Engineering, November 2006, Vol. 128, p. 531-538
## Measurement system
Two meteorological masts were erected on the farm. Mast 1, located ENE from Turbine 9, was the most comprehensively equipped, with Vector Instruments A100 cup anemometers at 10 m, 20 m, 33 m and 44 m heights, a w250P wind vane also from Vector Instruments at a height of 44 m, a Unidata 6522A barometric pressure gauge, a Campbell Scientific ARG100 tipping bucket rain gauge and two Skye Instruments temperature probes mounted at 5 m and 44 m. Mast 2 was equipped with two anemometers one at 10 m, the other at 33 m. This mast was also fitted with a wind vane at a height of 33 m. The data acquisition system hardware consisted of a Dell 486 PC with 130 Mb hard disc, tape streamer and colour VGA screen running the OS/2 operating system. This PC was situated in the wind farm site office with a second similar PC connected via a network and situated in the Deli Farm office. The turbines were connected to the data acquisition computer via a Quest data concentrator supplied by winddane. Additional sensors connected to remote data loggers passed data via radio transmitters. A receiver was installed at the wind farm office and connected to the data acquisition computer. The data from the two meteorological masts, the sub?station electrical data and two sites of noise monitoring equipment (required by a concurrent ETSU monitoring contract ? w/32/00289/00/00) were acquired by these systems. A system diagram of the data acquisition system is shown in Figure 2.3. The software for the data acquisition system was purpose written. As well as acquiring basic data, it computed derived performance parameters and allowed all these data to be downloaded on a daily basis via a modem.

- 1. [List of wind farm signals](./wind_farm/delabole/wf_signals.csv)

## Nominal values
![](./wind_farm/delabole//nom_speed.gif)
**Figure 1:** Nominal wind speed

![](./wind_farm/delabole//nom_ti.gif)
**Figure 2:** Nominal turbulence

![](./wind_farm/delabole//nom_dir.gif)
**Figure 3:** Nominal wind direction

## Distributions
![](./wind_farm/delabole/w_dist.gif)
**Figure 4:** Nominal wind speed distribuion.

![](./wind_farm/delabole/ti_dist.gif)
**Figure 5:** Nominal turbulence distribuion.

![](./wind_farm/delabole/dir_dist.gif)
**Figure 6:** Nominal wind rose.

## ACKNOwLEDMENTS	:	
- INSTITUTION       :Future Energy Solutions, AEA Technology, ETSU	
- ADDRESS 	        :Harwell, Didcot, Oxon OX11 0QJ, UK
- TEL/FAX 	        :+44 (0) 1235 433565 / +44 (0) 1235 433434
- CONTACTS 	        :Mike Payne
- LINKS 	        :http://w.aeat.co.uk/cms/
- COLLABS 	        :CSM Associates,Energy Research Unit, RAL
- FUND AGENTS       :DTI, UK
- PERIOD 	        :
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14077004'

## PUBLICATIONS	    : 
1. NA

## Public data
1. wind farm data  (NetCDF) 


