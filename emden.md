## Background Information
- CLASSIFICATION: flat(flat landscape), coastal(water and land)
- COUNTRY : Germany
- ALTITUDE : 0 [m]
- POSITION : [53˚ 20' 18.7'' N 7˚ 9'28.2'' E](https://geohack.toolforge.org/geohack.php?pagename=emden&params=53_20_18.7_N_7_9_28.2_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This dataset consists of time series and 10-minute statistics of wind speed and wind direction measurements from one 68m mast at Emden, Germany. The period includes 130 hours of measurementsfrom 1997.

## Map
![A map of Denmark](./ts_wind/emden/msmap.gif)
**Map 1:** Location of Emden, Germany. 

![A map of Denmark](./ts_wind/emden/map_1.gif)
**Map 2:** Environment map of the measuring site

## Photos
![](./ts_wind/emden/photo_1.gif) **Photo 01:** photo from turbine_1 in northern direction

![](./ts_wind/emden/photo_2.gif) **Photo 02:** photo from turbine_1 in eastern direction
![](./ts_wind/emden/photo_3.gif) **Photo 03:** photo from turbine_1 in southern direction
![](./ts_wind/emden/photo_4.gif) **Photo 04:** photo from WT_1 in w-dir. at mast erection
![](./ts_wind/emden/photo_5.gif) **Photo 05:** photo of cup anemometer
![](./ts_wind/emden/photo_6.gif) **Photo 04:** photo of cup/vane

## Drawings
![](./ts_wind/emden/layout.gif) **Drawing 01:** Site layout.

## Reports
NA

## Mast (relative positions with reference to the reference POSITION) 
1. 68 [m] (0,0,0)

## Project description
NA

## Measurement system
NA


1. [List of mast signals (time series of wind speed)](./ts_wind/emden/ts_mast_signals.csv)

## Nominal values

![](./ts_wind/emden/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/emden/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/emden/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wind/emden/w_dist.gif)
**Figure n4:** Nominal wind speed distribution.

![A map of Denmark](./ts_wind/emden/ti_dist.gif)
**Figure n5:** Nominal turbulence distribution.

![A map of Denmark](./ts_wind/emden/dir_dist.gif)
**Figure n6:** Nominal wind direction distribution. 

## ACKNOWLEDGEMENT	
- INSTITUTION       :	Deutches Windenergie-Institut GmbH
- ADDRESS 	        :   Ebertstr. 96, 26382 Wilhelmshaven, Germany
- TEL/FAX 	        :   +4421 4808 0 / ++4421 480843
- CONTACTS 	        :   Christian Hinsch
- LIKS 	        :   http://www.dewi.de/
- COLLABS 	        :   VDMA (Germany  nachinery and plant manufacturers' ass.)
- FUND AGENTS       :   BMBF (German Research Ministry) and AVIF (Research soc. work. all. iron,steel,,.)
- PERIOD 	        :   1997
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14484657'


## PUBLICATIONS	    : 
NA

## Public data
1. Run statistics; emden_all.nc (NetCDF) 
2. Run statistics; emden_concurrent.nc (NetCDF)

Raw time series (ascii) each with a duration of 600 sec, sampled with 20 Hz:
3. emden.zip (zip)

