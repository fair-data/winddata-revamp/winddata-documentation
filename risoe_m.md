## Background Information
- CLASSIFICATION: flat(flat landscape), coastal(water and land)
- COUNTRY : Denmark
- ALTITUDE : 2 [m]
- POSITION : [55˚ 41' 39.2'' N 12˚ 5'17'' E](https://geohack.toolforge.org/geohack.php?pagename=risoe_m&params=55_41_39.2_N_12_5_17_E_)
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)


## Short summary
This resource dataset consists of 10-minute wind speed and wind direction measurements from the tall met mast at Risø, Roskilde in  Denmark. The period includes 12 years of measurements, which starts in 1995.

## Map
![A map of Denmark](./Resource/risoe_m/msmap.gif)
**Figure 1:** A map of Sealand, Denmark
![A map of Denmark](./Resource/risoe_m/risoemap.gif)
**Figure 2:** A map of Sealand, Denmark

## Photos
![](./Resource/risoe_m/pic_01.jpg) **Photo 01:** Risoe met mast.
![](./Resource/risoe_m/risomast.jpg) **Photo 02:** 125m met mast located at DTU Risoe Campus, Denmakr.


## Drawings
![](./Resource/risoe_m/layout.gif) **Drawing 01:** Layout of 90 mast.


## Mast (relative positions with reference to the reference POSITION) 
1. 125 [m] (0,0,0)

## Project description
The meteorologist of Risø have since the start of Risø in 1956 collected climatological data from measuring masts both within Denmark and outside the country. The data have mostly been collected to obtain relevant climate statistics for the locality in question. Examples can be: Establishment of a climatology of atmospheric dispersion for a planned site for a power plant or an other production facility. Estimation of the the local windspeed distribution for evaluation of wind power resource or of the extreme wind load on structures and buildings. Description of seasonal and long term exchange between the atmosphere and different types of vegetation. The data have also proven useful to in connection with events of various kinds, such as tracking storm surges across the country, estimating contaminated regions for transient events as different as nuclear and industrial accidents and air borne diseases.

## Measurement system
Data from the Risø sites consist typically of atmospheric pressure and wind direction, wind speed in several heights, and temperature in at least two heights, such that climatology for wind speed and direction, wind variation with height and thermal stability can be established. At some stations also measurements of humidity, incoming solar radiation, and wind variability are obtained. Typically the measurements consist of ten minuttes averages recorded every ten minuttes. Details of the data are reported on the individual pages for each measuring station. Further information Further information can be obtained from Gunnar Jensen, the department of Wind Energy and Atmospheric Physics, Risø National Laboratory. Phone +4546775007. E-mail: gunnar.jensen@risoe.dk

1. [List of mast signals](./Resource/risoe_m/Mast_signals.csv)


## ACKNOWLEDMENTS	:	
- INSTITUTION 		:	Risoe National Laboratories
- ADDRESS 	        :	Post box 49, DK4000 Roskilde, Denmark
- TEL/FAX 	        :	+45 46775007 / +45 46755619
- CONTACTS 	        :	Gunner Jensen / Gunner Larsen
- LINKS 	        :	http://www.risoe.dk/vea-data/ / http://www.risoe.dk/vea/
- FUND AGENTS      	:	Internal
- PERIOD 	        :	1956-01-01  - 2001-12-31
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14153204' 

## Public data
1. Resource data  (NetCDF) 


