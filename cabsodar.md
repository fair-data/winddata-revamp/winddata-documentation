## Background Information
- CLASSIFICATION: flat(flat landscape), coastal(water and land)
- COUNTRY : Netherlands
- ALTITUDE : 0.5 [m]
- POSITION : [51˚ 58' 14'' N 4˚ 55' 35'' E](https://geohack.toolforge.org/geohack.php?pagename=CabSodar&params=51_58_14_N_4_55_35_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This resource dataset consists of 10-minute wind speed and wind direction measurements from a Sodar located next to the Cabauw mast, The Netherlands. The period includes more than 1320 hours of measurements, which starts in 2000. The height range is from 20 to 200m and the Sodar is treated as an artificial mast.

## Map
![](./Resource/cabsodar/map.jpg) **Figure 1:**  Location of Cabauw
![](./Resource/cabsodar/msmap.gif) **Figure 2:** ECN test sites

## Drawings

![](./Resource/cabsodar/relative_position.jpg) **Drawing 01:** Drawing of MiniSODAR location, relative to meteomast.

![](./Resource/cabsodar/layout.gif) **Drawing 02:** Site layout.

## Photos
![](./Resource/cabsodar/picture_sodar.jpg) **Photo 01:** Picture of MiniSODAR location taken from meteomast.
![](./Resource/cabsodar/east.jpg) **Photo 02:**Photo of MiniSODAR - in direction East
![](./Resource/cabsodar/meteomast.jpg) **Photo 03:**Picture of the mast, h=214 m
![](./Resource/cabsodar/North.jpg) **Photo 04:**Photo of MiniSODAR - in direction North (with met. mast in background)
![](./Resource/cabsodar/west.jpg) **Photo 05:**Photo of MiniSODAR - in direction West
![](./Resource/cabsodar/south.JPG) **Photo 06:**Photo of MiniSODAR, - in direction South

## Mast (relative positions with reference to the reference POSITION) 
MASTS
1. 213 [m] (0,0,-0.5)
2.   0 [m] (2,-304,-0.5)

WIND TURBINES
NA

## Project description
ECN performed measurements to validate a MiniSODAR measurement system. A number of different measurements were done. These were hardware test, background noise and vibration measurements. The SODAR measurements are compared against the 213-metres height meteorological mast owned by the KNMI, located at Cabauw in the Netherlands. We appreciate KNMI for providing the measurement data from their meteorological mast. Comparisons are performed at different height levels and using different SODAR settings. Two different models of the MiniSODAR were used for this purpose.


## Measurement system
 he Cabauw meteo mast is a tubular tower with a height of 213 m and a diameter of 2 m. Guy wires are attached at four levels. From 20m upwards horizontal trussed measurement booms are installed at intervals of 20 m. At each level there are three booms, extending 10.4 m from the centerline of the tower. These booms point to the directions 10, 130, 250 degrees relative to North. The SW and N booms are used for wind velocity and wind direction measurements. These booms carry at the end two lateral extensions with a length of 1.5 m and a diameter of about 4 cm. Measured values from the meteorological sensors are already aggregated by the KNMI into 10-minute averaged signals. Wind speed and wind direction measurements are available at 80, 140 and 200 meter heights. Two MiniSODAR models from AeroVironment are used in this project; model 3000 and model 4000.


1. [List of mast signals](./Resource/cabsodar/Mast_signals.csv)

## Nominal values

![](./Resource/cabsodar/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./Resource/cabsodar/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./Resource/cabsodar/nom_dir.gif)
**Figure n3:** Nominal wind direction

## ACKNOWLEDMENTS		
- INSTITUTION       :	Energy research Center of the Netherlands (ECN)
- ADDRESS 	        :   PO Box 1, NL1755ZG Petten, The Netherlands
- TEL/FAX 	        :   +31 224 564115 / +31 224 568214
- CONTACTS 	        :   Peter Eecen
- LINKS 	        :   http://www.ecn.nl/
- COLLABS 	        :   Lagerwey
- FUND AGENTS       :   Internal
- PERIOD 	        :   2000-11-23 - 2001-02-28 
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.15180765'

## PUBLICATIONS	    : 
NA

## Public data
1. Resource data  (NetCDF) 



