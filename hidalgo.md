## Background Information
- CLASSIFICATION: hill(rolling hills), scrub(bushes and small trees)
- COUNTRY : Mexico
- ALTITUDE : 2811 [m]
- POSITION : [20˚ 0' 28'' N 98˚ 33'9'' E](https://geohack.toolforge.org/geohack.php?pagename=hidalgo&params=20_0_28_N_98_33_9_W_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This resource dataset consists of 10-minute statistics of wind speed and wind direction measurements from a singel 32m met mast located in Hidalgo, Mexico. The period includes 5 years of measurements, which starts in 2002. 

## Map
![A map of Denmark](./Resource/hidalgo/mexico-country-map-600x450.jpg)
**Figure 1:** A map of Mexico, Hidalgo is located next to Pachuca NE to Mexico City.
![A map of Denmark](./Resource/hidalgo/msmap.gif)
**Figure 2:** A map of Hidalgo, Mexico.

## Photos
![](./Resource/hidalgo/pic_01.jpg) **Photo 01:** Site orogpraphy
![](./Resource/hidalgo/pic_02.jpg) **Photo 02:** Measurement tower, 30m

## Reports
3. [Site loacation of Hidalgo](./Resource/hidalgo/LdatospCP.pdf)
2. [Description of Hidalgo setup(ES)](./Resource/hidalgo/sCerropelon.pdf)



## Mast (relative positions with reference to the reference POSITION) 
1. 30 [m] (0,0,0)

## Project description
Measurements are performed at different locations in Mexico for estimating the wind energy resource.

## Measurement system
Standard measurement system consisting of a 30 m mast equipped with NRG cups and Campbell model CR10X logger.

1. [List of mast signals](./Resource/hidalgo/Mast_signals.csv)

## Distributions
![A map of Denmark](./Resource/hidalgo/w_dist.png)
**Figure 1:** Nominal wind speed distribuion.
![A map of Denmark](./Resource/hidalgo/ti_dist.png)
**Figure 2:** Nominal turbulence distribuion.
![A map of Denmark](./Resource/hidalgo/dir_dist.png)
**Figure 3:** Nominal wind direction distribuion.


## ACKNOWLEDMENTS		
- INSTITUTION       :	Instituto de Investigaciones Eltricas,Division de Energia Alternas
- ADDRESS 	        :   Av. REFORMA No. 113 COL. PALMIRA, 62490 TEMIXCO, MORELOS, MEXICO
- TEL/FAX 	        :   +52 73 18 38 11 x 7251 / +52 73 18 24 36
- CONTACTS 	        :   Marco Borja 
- PERIOD 	        :   2002-01-01 -
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14135570'

## PUBLICATIONS	    : 
1. 

## Public data
1. Resource data  (NetCDF) 


