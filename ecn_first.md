## Background Information
- CLASSIFICATION: flat(flat landscape), coastal(water and land)
- COUNTRY : Netherlands
- ALTITUDE : 9 [m]
- POSITION : [52˚ 47' 8'' N 4˚ 40'20'' E](https://geohack.toolforge.org/geohack.php?pagename=ecn&params=52_47_8_N_4_40_20_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
The dataset includes time series of wind data from an array of 4 masts, each with a height of 41m and located  at ECN, Petten, The Netherlands. The period includes 49 hours of turbulence measurements from an array of masts., which starts in 1992. Furthermore, some 4 Hz time series from the 3D Sonic anemometers have been included. 

## Map
![A map of Denmark](./ts_wind/ecn/map_1.gif)
**Figure 1:** A map of The Netherlands
![A map of Denmark](./ts_wind/ecn/map_2.gif)
**Figure 2:** Location map for the Netherlands
![A map of Denmark](./ts_wind/ecn/map_3.gif)
**Figure 3:** Topographical map of the neighbourhood
![A map of Denmark](./ts_wind/ecn/map_4.gif) **Figure 4:** site layout with location of mast and turbine (HAT 25)


## Photos
NA

## Masts (relative positions with reference to the reference POSITION) 

1. 41 [m] (28.4,39.9,3.7)
2. 41 [m] (-61.5,13.6,3.7)
3. 41 [m] (-63.6,-17.6,3.7)
4. 41 [m] (47.7,-5.4,3.7)

## WIND TURBINE (HAT 25)
1. ECN 300kW/25m 300 [kW] (0,0,9)

## Project description
The primary goal was to design and build and test a flexible rotor (two blades, diameter 21.6 m) with passive tip-pitch control and a teetered hub with an elastomeric bearing. The FLEXTEETER rotor was tested at the 25m HAWT test facility of ECN. During the project the wind input was measured with a set of four meteo masts. The wind data obtained as part of the test of FLEXTEETER are available for the "Database on Wind Characteristics".


## Measurement system
 The measurement system consists of 4 meteorological mast, mast no. 1, 2, 3 were placed West of the turbine and mast 4 at the East side. The measurements system was based on a PDP datalogger and the data were transferred to a DIGITAL VAX system (VAX VMS operating system with data handling routines and programs developed at ECN). The data to be measured were divided in two groups: group 1, wind data with a recording frequency of 4 Hz; group 2, turbine operational data and mechanical load data with a recording frequency of 32 Hz. The maximum duration of a consecutive measurement with 32 channels in both groups was about 18 hrs.


1. [List of mast signals](./ts_wind/ecn/ts_mast_signals.csv)

## Nominal values

![](./ts_wind/ecn/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/ecn/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/ecn/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wind/ecn/w_dist.gif)
**Figure n4:** Nominal wind speed distribution.

![A map of Denmark](./ts_wind/ecn/ti_dist.gif)
**Figure n5:** Nominal turbulence distribution.

![A map of Denmark](./ts_wind/ecn/dir_dist.gif)
**Figure n6:** Nominal wind direction distribution. 
## ACKNOWLEDMENTS		
- INSTITUTION       :	Dept. of Renewable Energy, The Netherlands Energy Research Foundation, ECN
- ADDRESS 	        :   PO Box 1, NL1755ZG Petten, The Netherlands
- TEL/FAX 	        :   +31 224 564115 / +31 224 568214
- CONTACTS 	        :   J.W.M.Dekker
- LINKS 	        :   http://www.ecn.nl/
- FUND AGENTS       :   NOVEM, NL
- PERIOD 	        :   1991-01-01 - 1993-01-31
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.18319037'

## PUBLICATIONS	    : 
NA

## Public data
1. Run statistics; ecn_all.nc (NetCDF) 
2. Run statistics; ecn_concurrent.nc (NetCDF)

Raw time series (ascii) each with a duration of 3600 sec, sampled with 4 Hz:
3. ecn.zip (zip)


