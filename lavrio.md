## Background Information
- CLASSIFICATION: mountain(sharp contours - separation expected), pastoral(open fields and meadows)
- COUNTRY : Greece
- ALTITUDE : 122 [m]
- POSITION : [37˚ 46' 4'' N 24˚ 3'45'' E](https://geohack.toolforge.org/geohack.php?pagename=lavrio&params=37_46_4_N_24_3_45_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)


## Short summary
This dataset consists of time series and 10-minute statistics of wind speed and wind direction measurements from two 40m mast at Lavrio, Greece. The period includes more than 790 hours of measurements, which starts in 1995.

## Map
![A map of Denmark](./ts_wind/lavrio/msmap.gif)
**Figure 1:** A map of Greece
## Drawing
![A map of Denmark](./ts_wind/lavrio/lavrio_layout.gif)
**Drazwing 1:** Site layout

## Photos
![](./ts_wind/lavrio/photo_1.gif) **Photo 01:** site view of masts and wt
![](./ts_wind/lavrio/photo_2.gif) **Photo 02:** site view of masts and wt
![](./ts_wind/lavrio/photo_3.gif) **Photo 03:** Maintenance of wt


## Reports
NA

## Mast (relative positions with reference to the reference POSITION) 
1. 40 [m] (12.25,26.4,-1.2)
2. 40 [m] (-3.55,28.9,-1.2)

## WIND TURBINES
1. Wind World W110 XT 110 [kW] (0,0,0) (refence)

## Project description
NA

## Measurement system
NA

1. [List of mast signals](./ts_wind/lavrio/ts_mast_signals.csv)
2. [List of additional signals](./ts_wind/lavrio/add_signals.csv) Note: The statistics for these signals are only included in the header of the packed ASCII file.

## Nominal values

![](./ts_wind/lavrio/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/lavrio/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/lavrio/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wind/lavrio/w_dist.gif)
**Figure n4:** Nominal wind speed distribution.

![A map of Denmark](./ts_wind/lavrio/ti_dist.gif)
**Figure n5:** Nominal turbulence distribution.

![A map of Denmark](./ts_wind/lavrio/dir_dist.gif)
**Figure n6:** Nominal wind direction distribution. 

## ACKNOWLEDMENTS	
- INSTITUTION       :	C.R.E.S, Greece
- ADDRESS 	        :   C.R.E.S., 19th km, Marathons Ave, 19009, Pikermi, Attika, Greece
- TEL/FAX 	        :   -6039871 / -6039876
- CONTACTS 	        :   A.N.Fragoulis
- LINKS 	        :   http://www.hydro.lavrio.gr/
- COLLABS 	        :   RISO (DK), ECN (NL), CIEMAT (ES)
- FUND AGENTS       :   EU (DGXVII), Greek secretariat for Research & Technology
- PERIOD 	        :   1995-1996
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14484993'


## PUBLICATIONS	    : 
NA

## Public data
1. Run statistics; lavrio_all.nc (NetCDF)
2. Run statistics; lavrio_concurrent.nc (NetCDF)

Raw time series (ascii) each with a duration of 600 sec, sampled with 20 Hz:
3. lavrio.zip (zip)


