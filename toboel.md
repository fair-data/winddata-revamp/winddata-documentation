## Background Information
- CLASSIFICATION: flat(flat landscape), pastoral(open fields and meadows)
- COUNTRY : Denmark
- ALTITUDE : 16 [m]
- POSITION : [55˚ 25' 51.96'' N 8˚ 52'4.95'' E](https://geohack.toolforge.org/geohack.php?pagename=toboel&params=55_25_51.96_N_8_52_4.95_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This dataset consists of raw time series and statistics of wind speed and wind direction measurements from two met mast located in toboel wind turbines, Denmark. The period includes more than 1100 hours of measurements, which starts in 1999.

## Map

![A map of Denmark](./ts_wind/toboel/msmap.gif)
**Map 2:** Location of toboel in Denmark

## Photos
![](./ts_wind/toboel/photo_01.jpg) **Photo 01:** Westwards from wind turbine #1, towards met. mast #1, which is located in the centre of the photo.
![](./ts_wind/toboel/photo_02.jpg) **Photo 02:** Northwards from wind turbine #1.
![](./ts_wind/toboel/photo_03.jpg) **Photo 03:** Southwestwards from wind turbine #1.
![](./ts_wind/toboel/photo_04.jpg) **Photo 04:** Southwards from wind turbine #1, (before wind turbine #2 was erected).
![](./ts_wind/toboel/photo_05.jpg) **Photo 05:** Flatness of terrain, sector SE - SW, seen from wind turbine 1
![](./ts_wind/toboel/photo_06.jpg) **Photo 06:** Flatness of terrain, sector SW - NW, seen from wind turbine 1.
![](./ts_wind/toboel/photo_07.jpg) **Photo 07:** Flatness of terrain, sector NW - NE, seen from wind turbine 1.
![](./ts_wind/toboel/turbine.jpg) **Photo 08:** The NM900/52 wind turbine and met. mast #1 seen towards SW.


## Drawings
![](./ts_wind/toboel/layout.gif) **Drawing 01:** Measurement setup - mast configuration.

![](./ts_wind/toboel/draw_01.gif) **Drawing 02:** Siteplan for mast 1 & 2 and wind turbines 1 & 2.

## Reports
NA

## Mast (relative positions with reference to the reference POSITION) 
1. 64 [m] (-126,-34,0)
2. 49 [m] (-239,-64,0)

# Wind turbines
1. NEG-Micon NM 900 / 52 900 [kW] (0,0,0)
2. NEG-Micon NM 900 / 52 900 [kW] (-193,52,0)

## Project description
A dual test of two NM900 wind turbines was performed in Tobøl, Denmark, which includes a met mast in front of each of the wind turbines. Part of the meteorological measurements from this test have been made public, but there is no signals from the turbines. 
1. Note: this dataset includes extreme recordings of wind speed during the storm passing Denmark 3. December 1999. 

## Measurement system
Two standard, masts have been used for this project together high quality instruments and logggers. Each mast has been equipped with and individuual logger system. 

1. [List of mast signals](./ts_wind/toboel/ts_mast_signals.csv)

2. [List of additional signals](./ts_wind/toboel/add_signals.csv) Note: The statistics for these signals are only included in the header of the packed ASCII file.

## Nominal values

![](./ts_wind/toboel/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/toboel/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/toboel/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wind/toboel/w_dist.gif)
**Figure n4:** Nominal wind speed distribution.

![A map of Denmark](./ts_wind/toboel/ti_dist.gif)
**Figure n5:** Nominal turbulence distribution.

![A map of Denmark](./ts_wind/toboel/dir_dist.gif)
**Figure n6:** Nominal wind direction distribution. 

## ACKNOWLEDMENTS	:	 
- INSTITUTION       :	Risoe National Laboratory
- ADDRESS 	        :   Department of Wind Energy and Atmospheric,  Physics Post box 49, DK4000 Roskilde, Denmark
- CONTACTS 	        :  Gunner C. Larsen / Soeren.M.Petersen
- LINKS 	        :  http://www.risoe.dk/vea/
- FUND AGENTS       :  Danish Ministry of Energy & NEG Micon
- PERIOD 	        :  1998 - 2000 
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14398730'

## PUBLICATIONS	    : 
NA

## Public data
1. Run statistics; toboel_1.nc (NetCDF) 
2. Run statistics; toboel_2.nc (NetCDF) 
3. Raw time series = toboel_1.zip (ascii) each with a duration of 600 sec and sampled with 8 Hz.
4. Raw time series = toboel_2.zip (ascii) each with a duration of 600 sec and sampled with 32 Hz.



