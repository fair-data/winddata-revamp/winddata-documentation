## Background Information
- CLASSIFICATION: hill(rolling hills), coastal(water and land)
- COUNTRY : Greenland
- ALTITUDE : 78 [m]
- POSITION : [66˚ 55' 27.12'' N 53˚ 40' 3.42'' E](https://geohack.toolforge.org/geohack.php?pagename=simiut&params=66_55_27.12_N_53_40_3.42_W_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This resource dataset consists of 10 minute wind speed and wind direction measurements from a small met mast on Dumpen, outside   Sisimiut,Greenland. The period includes 1 year of measurements, which starts in 2004.

## Map
![A map of Denmark](./Resource/sisimiut/msmap.gif)
**Figure 1:** A map of the central Greenland, with the former name Holsteinsborg, now Sisimiut.

![A map of Denmark](./Resource/sisimiut/sisimiut_by_air.jpg)
**Figure 2:** A picture of Sisimiut from the air.

![A map of Denmark](./Resource/sisimiut/dumpen.jpg)
**Figure 3:** A local map, which includes Dumped where the mast has been locaed.

## Drawings
![](./Resource/sisimiut/layout.gif) **Drawing 01:** Mast layout.


## Reports
1. [Presentation of wind measurements in Greenland](./Resource/sisimiut/Sisimiut-wind-measurements.pdf)

## Mast (relative positions with reference to the reference POSITION) 
1. 10 [m] (0,0,0) (mast 1.)

## Project description
  In August 2004 a meteorological mast was erected outside Sisimiut in Greenland The main purpose is to measure the wind resource on a stormy location near Sisimiut. The Met.mast has been erected by Hans Hindrichsen and Poul Linnert Christiansen during the summer camp in August 2004.

## Measurement system
he meteorological data in terms of wind speed and wind direction at 10 m height, are recorded as 10 minute statistics with a standard NRG-system datalogger. The measurement campaign was initiated in August 2004.

1. [List of mast signals](./Resource/sisimiut/ts_mast_signals.csv)


## Nominal values

![](./Resource/sisimiut/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./Resource/sisimiut/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./Resource/sisimiut/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./Resource/sisimiut/w_dist.gif)
**Figure n4:** Nominal wind speed distribution.

![A map of Denmark](./Resource/sisimiut/ti_dist.gif)
**Figure n5:** Nominal turbulence distribution.

![A map of Denmark](./Resource/sisimiut/dir_dist.gif)
**Figure n6:** Nominal wind direction distribution. 

## ACKNOWLEDMENTS		
- Acknowledgemt     : Hans Hinrichsen, Sisimiut
- E-mail: 
- INSTITUTION       : Technical University of Denmark
- ADDRESS 	        : Fluid Mechanics Section, MEK, B403-DTU, 2800 Lyngby
- TEL/FAX 	        : +45 45 25 43 18 / +45 45 93 06 63
- CONTACTS 	        : Kurt S. Hansen / Martin O.L. Hansen
- LINKS 	        : www.afm.dtu.dk / www.mek.dtu.dk
- COLLABS 	        : Danish arctic centre
- FUND AGENTS       : Internal
- PERIOD 	        : 2004-08-05 00:00:00 - 
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.xxxxx/DTU.14153231'

## Public data
1. Resource data  (NetCDF) 