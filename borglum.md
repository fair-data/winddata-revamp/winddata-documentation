## Background Information
- CLASSIFICATION: flat(flat landscape), pastoral(open fields and meadows)
- COUNTRY : Denmark
- ALTITUDE : 9 [m]
- POSITION : [57˚ 20' 24'' N 9˚ 48'36'' E](https://geohack.toolforge.org/geohack.php?pagename=borlum&params=57_20_24_N_9_48_36_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This resource dataset consists of 10-minute statistics of wind speed and wind direction measurements from a met mast at Børglum, Denmark. The period includes 4 years of measurements, which starts in 1997.

## Map
![A map of Denmark](./Resource/borglum/msmap.gif)
**Figure 1:** A map of Denmark

## Photos
![](./Resource/borglum/borglummast.jpg) **Photo 01:** The 32 m mast at Borglum.

## Mast (relative positions with reference to the reference POSITION) 
1. 32 [m] (0,0,0)

## Project description
The meteorologist of Risø have since the start of Risø in 1956 collected climatological data from measuring masts both within Denmark and outside the country. The data have mostly been collected to obtain relevant climate statistics for the locality in question. Examples can be: Establishment of a climatology of atmospheric dispersion for a planned site for a power plant or an other production facility. Estimation of the the local windspeed distribution for evaluation of wind power resource or of the extreme wind load on structures and buildings. Description of seasonal and long term exchange between the atmosphere and different types of vegetation. The data have also proven useful to in connection with events of various kinds, such as tracking storm surges across the country, estimating contaminated regions for transient events as different as nuclear and industrial accidents and air borne diseases.

## Measurement system
 Data from the Risø sites consist typically of atmospheric pressure and wind direction, wind speed in several heights, and temperature in at least two heights, such that climatology for wind speed and direction, wind variation with height and thermal stability can be established. At some stations also measurements of humidity, incoming solar radiation, and wind variability are obtained. Typically the measurements consist of ten minuttes averages recorded every ten minuttes. Details of the data are reported on the individual pages for each measuring station. Further information Further information can be obtained from Gunnar Jensen, the department of Wind Energy and Atmospheric Physics, Risø National Laboratory. Phone +4546775007. E-mail: gunnar.jensen@risoe.dk

1. [List of mast signals](./Resource/borglum/Mast_signals.csv)


## Distributions
![A map of Denmark](./Resource/borglum/w_dist.png)
**Figure n4:** Nominal wind speed distribuion.

![A map of Denmark](./Resource/borglum/dir_dist.png)
**Figure n5:** Nominal wind direction distribuion. 

## ACKNOWLEDMENTS	
- INSTITUTION       :	Risoe National Laboratories
- ADDRESS 	        :   Post box 49, DK4000 Roskilde, Denmark
- TEL/FAX 	        :   +45 46775007 / +45 46755619
- CONTACTS 	        :   Gunner Jensen / Gunner Larsen
- LINKS 	        :   http://www.risoe.dk/vea-data/
- PERIOD 	        :   1956-01-01 - 2001-12-31
- Naming_authority  :   'DTU Data'
- DOI               :   'https://doi.org/10.11583/DTU.14153231'


## PUBLICATIONS	    : 
1. 

## Public data
1. Resource data  (NetCDF) 

