## Background Information

- CLASSIFICATION: flat(flat landscape), offshore(open sea
- COUNTRY : Denmark
- ALTITUDE : 0 [m]
- POSITION : [54 30'11.7'' N 12 5'12.48''E](https://geohack.toolforge.org/geohack.php?pagename=gedsrev&params=54_30_11.7_N_12_5_12.48_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This dataset consists of time series and statistics of wind speed and wind direction measurements from one 48m offshore mast at Gedser Rev, DenamrkGermany. The period includes more than 700 hours of measurementsfrom 1998.

## Map
![A map of Denmark](./ts_wind/gedsrev/map_01.gif)
**Figure 1:** A map of Denmark

![A map of Denmark](./ts_wind/gedsrev/msmap.gif)
**Figure 1:** Location of Gedser Rev SE to Gedser.


## Photos
![](./ts_wind/gedsrev/photo_01.gif) **Photo 01:** Gedser Rev met. mast photo

## Drawings
![](./ts_wind/gedsrev/layout.gif) **Drawing 01:** Site layout. 
![](./ts_wind/gedsrev/draw_01.gif) **Drawing 02:** layout for the  Masts 

## Reports
NA

## Mast (relative positions with reference to the reference POSITION)
1. 48 [m] (0,0,0)

## Windturbines
NA

## Project description
 Characterising wind and turbulence for offshore wind energy development.
## Measurement systemTwo met. masts has been erected off-shore, near Gedser, Denmark.
The mast at Roedsand has been instrumented with 3 cup anemometers, 1 vane and 1 sonic anemometer together with several thermometers.

The met.mast near Gedser Rev has been instrumented with 3 cup anemometers, one vane and 3 thermometers.

The data acquisition system DAQ has used for data recording (5 and 20 Hz) and was operated by Rebecca Barthelmie and Peter Zanderhoff, Risoe National Laboratories.

The measured time series from Roedsand / Gedser Syd has been collected, validated and provided by: Senior Scientist, Rebecca Barthelmie Ph.D, Email: r.barthelmie@risoe.dk Department Wind Energy and Atmosphere Physics, Riso National Laboratory, 4000 Roskilde, Denmark.

Instrumentation, operation and maintenance at Roedsand is funded by SEAS Distribution A.m.b.A.

1. [List of mast signals](./ts_wind/gedsrev/ts_mast_signals.csv)
2. [List of additional signals](./ts_wind/gedsrev/add_signals.csv) Note: The statistics for these signals are only included in the header of the packed ASCII file.
## Nominal values

![](./ts_wind/gedsrev/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/gedsrev/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/gedsrev/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wind/gedsrev/w_dist.gif)
**Figure n4:** Nominal wind speed distribuion.

![A map of Denmark](./ts_wind/gedsrev/ti_dist.gif)
**Figure n5:** Nominal turbulence distribuion.

![A map of Denmark](./ts_wind/gedsrev/dir_dist.gif)
**Figure n6:** Nominal wind direction distribuion. 

## ACKNOWLEDMENTS	:	
- INSTITUTION       :   Risoe National Laboratory
- ADDRESS 	        :   Postbox 49, DK-4000, Roskilde Denmark
- TEL/FAX 	        :   +45 4677 5017 / +45 4675 5619
- CONTACTS 	        :  Rebecca Barthelmie
- LINKS 	        :   http://www.risoe.dk/vea/
- COLLABS 	        :   SEAS Wind Energy Centre, Haslev
- FUND AGENTS       :   Danish Ministry, ELKRAFT
- PERIOD 	        :   1997-01-01 - 2001-12-31
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14493795'

## PUBLICATIONS	    : 

## Public data
1. Run statistics; gedsrev_all.nc (NetCDF) 
2. Raw time series = gedsrev.zip (zip) each with a duration of 1800 sec, sampled with 5 Hz.
