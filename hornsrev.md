## Background Information
- CLASSIFICATION: offshore(open sea)
- COUNTRY : Denmark
- ALTITUDE : o [m]
- POSITION : [55˚ 31' 12.48'' N 7˚ 47'13.02'' E](https://geohack.toolforge.org/geohack.php?pagename=hornsrev&params=55_31_12.48_N_7_46_13.02_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This dataset consists of raw time series and 10-minute statistics of wind speed and wind direction measurements from an offshore met mast next to the Horns Rev #1 WF. The period includes appr. 5000 hours of measurements in the peruiod 1999-2001.

## Map
![A map of Denmark](./ts_wind/hornsrev/msmap.gif)
**Map 1:** Location of the offshore HR1M2 mast 

## Photos
![](./ts_wind/hornsrev/photo_1.gif) **Photo 01:** Horns rev offshore met. mast.
![](./ts_wind/hornsrev/photo_2.jpg) **Photo 02:** The METEK Sonic anemometer.


## Drawings
![A map of Denmark](./ts_wind/hornsrev/map_1.gif)
**Drawing 1:** Geographical layout of the wind farm and the measurement systems at Horns Rev.
![](./ts_wind/hornsrev/draw_1.jpg) **Drawing 2:** The meteorological mast at Horns Rev; the 3-D sonic was located 50m above msl.

## Reports
1. [Measurement report for the first year of operation - as pdf file](./ts_wind/hornsrev/N227sne.pdf)
2. [Description of measurement setup - pdf file](./ts_wind/hornsrev/description.pdf)

## Mast (relative positions with reference to the reference POSITION) 
1. 62 [m] (0,0,0)

## Project description
The purpose of the workare to investigate the wind and sea climates for the first of the two offshore wind farms in the Elsam area, i.e. the 150 MW Horns Rev wind farm to be built and commissioned by the end of year 2002. The Horns Rev site is located at a reef approx.14 km off Jutland in the North Sea in a very harsh environment. The water depths at the site vary between 6 and 12 m. In-depth knowledge of the environment at the offshore site was crucial to enable the evaluation of vital information such as wind farm energy production prognoses, assessment of structural loads and the prediction of global long-term seabed changes and local scouring around the foundations.

## Measurement system
A square lattice mast was erected on a mono pile close to the wind farm site at Horns Rev and mid May 1999 the meteorological measurement programme was initiated and later in June 1999 the marine measurement programme started. Cup anemomenters are located at 15, 30, 45 and 62 m above sea level. Vanes are located 28, 43 and 60 above sea level. The 3-D sonic measurements are recorded at 50 m above sea level. Cup and vane signals are stored as 10 minute statistics while only 3-D sonic measurements are stored as time series. The time series from the sonic has been indexed in 10-minute statistics (NetCDF), while the remaining statistics for cup/vanes only exist in the raw time series files. 

1. [List of mast signals (time series of wind speed)](./ts_wind/hornsrev/ts_mast_signals.csv)
2. [List of additional signals](./ts_wind/hornsrev/add_signals.csv) Note: The statistics for these signals are only included in the header of the packed ASCII file.
## Nominal values

![](./ts_wind/hornsrev/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/hornsrev/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/hornsrev/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wind/hornsrev/w_dist.gif)
**Figure n4:** Nominal wind speed distribution.

![A map of Denmark](./ts_wind/hornsrev/ti_dist.gif)
**Figure n5:** Nominal turbulence distribution.

![A map of Denmark](./ts_wind/hornsrev/dir_dist.gif)
**Figure n6:** Nominal wind direction distribution. 

## ACKNOWLEDMENTS	:	
- INSTITUTION       :	Elsamprojekt, Fredericia
- ADDRESS 	        :   Kraftvaerksvej 53, DK-7000 Fredericia
- TEL/FAX 	        :   +45 792333 / +45 75564477
- CONTACTS 	        :   Søren Neckelmann / Jan Petersen
- LINKS 	        :   http://www.elsamprojekt.dk/
- COLLABS 	        :   Elsam, Department of Energy Engineering DTU 
- FUND AGENTS       :   Elsam
- PERIOD 	        :   1999-2001
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14381714'


## PUBLICATIONS	    : 
NA

## Public data
1. Run statistics; hornsrev.nc (NetCDF) 
2. Raw time series = hornsrev.zip (ascii) each with a duration of 600 sec and sampled with 20 Hz.

