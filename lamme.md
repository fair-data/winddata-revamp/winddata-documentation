## Background Information

- CLASSIFICATION: flat(flat landscape), pastoral(open fields and meadows)
- COUNTRY : Denmark
- ALTITUDE : -3 [m]
- POSITION : [55 47'41'' N 11 26'52''E](https://geohack.toolforge.org/geohack.php?pagename=lamme&params=55_47_41_N_11_26_52_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This dataset includes the result of a Danish measurement campaign with an array of cup anemometers in the Lammefjord, Denmark. 
The period includes 663 hours of time series during the period 01-08-1987 until 01-06-1988.

## Map
![A map of Denmark](./ts_wind/Lamme/map_1.gif)
**Figure 1:** A map of Denmark

## Photos
![](./ts_wind/Lamme/photo_1.gif) **Photo 01:** Cup anemometer used at Lamme site
![](./ts_wind/Lamme/photo_2.gif) **Photo 02:** Vane used at Lamme site
![](./ts_wind/Lamme/photo_3.gif) **Photo 03:** Sonic instruments used at Lamme site
![](./ts_wind/Lamme/photo_4.gif) **Photo 04:** Mast layout for Lamme site

## Graphs
NA

## Drawings
![](./ts_wind/Lamme/map_2.gif) **Drawing 01:** Mast layout for Lamme site
![](./ts_wind/Lamme/lamme_layout.gif) **Drawing 02:** layout for the 4 Masts 

## Reports
NA

## Mast (relative positions with reference to the reference POSITION)
1. 45 [m] (0,0,0)
2. 30 [m] (-13.12,15.09,0)
3. 30 [m] (-17.06,19.62,0)
4. 10 [m] (-20.67,8.53,0)

## Windturbines
NA

## Project description
 An attempt to gather data continuously for one year with sufficient spatial and temporal resolution for wind turbine design studies. Completed with around 90% availability with a longest uninterrupted series of 103 days. The Measuring Site: The current project built upon a previous field experiment conducted to collect data for lateral coherence research (Courtney 1987). For this purpose, a site was required possessing homogeneous flow, preferably when the wind was from the prevailing south-westerly direction. A site was located at Lammefjord,a reclaimed fjord on the Danish island of Zealand. Lammefjord is amongst the flattest terrain in Denmark. Much of the land lies slightly below sea-level and because of difficulties with drainage, most of the buildings are grouped together on areas that were above water level prior to reclamation. The remainder of the land is used for agriculture, predominantly with root crops such as carrots and potatoes. The location of the measuring masts is indicated in Figure 1. To the south-east and north-west lie the towns of Faarvejle Stationby and Faarvejle respectively. To the south-east (Faarvejle Stationby) the nearest buildings are about 1 km from the measuring masts whilst in the opposite direction (Faavejle) the distance is about 0.5 km. A road connects the two towns and this passes about 150 m to the north-east of the masts. Apart from the road, the terrain to the north-east is open with the nearest buildings about 800 m away. To the south and south-west, the terrain is flat and completely unobstructed for a distance of between 2.5 and 3.0 km. Between the south-west and north-west the terrain is identical but the fetch reduces sharply to a little over 1 km. The old sea bed is bounded by a drainage canal, beyond which the terrain rises steeply, especially between west and north-west. Acknowledgements: The Lammefjord measurements were partly funded by the CEC, Directorate- General for Science, Research and Development under contract number EN-3W-002-DK(B). Preparation of the CD-ROMs has been partially supported by the EU funded JOULE project "Measuring and modelling in complex terrain", contract number JOUR-0067-C(MB). Acknowledgements: Mike Courtney, Risoe National Labotories

## Measurement system
An array of cups, vanes and one sonic anemometer were sampled by a pc. Data were stored on a magneto-optisk WORM drive. Data were recorded at 16 Hz for the sonic and 8 Hz for the cups and vanes. Measuring Masts: Since the data are primarily intended for wind turbine research, the aim was to instrument a vertical plane corresponding to that formed by the rotor of a medium sized wind turbine. This was accomplished by using three measuring masts erected so as to form a vertical plane 30 x 30m, perpendicular to the prevailing wind. An array of cup anemometers and wind direction vanes were distributed over this area as shown in Figure 2. A sonic anemometer was mounted at a height of 46m. In relation to the vertical plane formed by the three masts, a 10m mast was erected 15m upstream (in the prevailing wind direction). This was instrumented with a cup anemometer and a wind vane at 10m height. The masts are referred to as masts 1-4, with the mast to the left in Figure 2 as number 1, the mast in the centre as 2 and that to the right as mast 3.The upstream 10m mast is mast 4. The following table gives the (x,y) coordinates of the instruments mounted on each mast (including boom offset) in a coordinate system with x in the plane of the masts pointing towards north-west (319 deg) and y pointing perpendicular to the mast plane towards the south-west (229 deg). Dimensions are in meters. Mast x y Instrumentation 1 0.0 0.0 cups(3), vanes(3), sonic, climatology 2 20.0 0.0 cups(2), vanes(2) 3 30.0 0.0 cups(3), vanes(3) 4 22.7 15.3 cup, vane ("upstream" mast) Instrument station numbers have been assigned according to the following table. Station Mast Height Instrumentation sonic 1 46 Sonic (X, Y, Z, T) 1 1 10 Cup and vane (cos, sin) 2 1 20 Cup and vane (cos, sin) 3 1 30 Cup and vane (cos, sin) 4 2 10 Cup and vane (cos, sin) 5 2 30 Cup and vane (cos, sin) 6 3 10 Cup and vane (cos, sin) 7 3 20 Cup and vane (cos, sin) 8 3 30 Cup and vane (cos, sin) 9 4 10 Cup and vane (cos, sin) 10 1 various Cup Climatological Measuring System: An independent climatological measuring system was also installed, with all the relevant sensors mounted on mast 1. This instrumentation comprised: cup anemometers at 3, 10 and 45m wind direction vane at 10m global radiation relative humidity (horse-hair hygrometer) absolute temperature at 10m difference temperature 10m - 2m difference temperature 40m - 10m barometric pressure Observations were recorded every 10 minutes using an Aanderaa battery powered data logging system. Note that with the exception of the cup anemometer speed, all recorded observations are instantaneous values once per 10 minutes. Cup mean speed is derived by counting pulses over the 10 minute period. Cup Anemometer: The cup anemometers used were the Risoe model 70, fitted with carbon fibre cups. This instrument has a length constant of 1.7m. A two-pole magnet driven by the cup shaft is used to open and close a reed-contact switch, producing two pulses per revolution. Wind speed was derived using the Risoe P1225 Wind Speed Transmitter. This device is a microprocessor controlled frequency to voltage converter. The output voltage is updated on each incoming pulse such that the signal is proportional to the frequency derived from the preceding two pulses. Before installation, but following a two month "run-in" period, the cup anemometers were calibrated in a wind tunnel. Re-calibration of the cup anemometers after the completion of the experiment showed that the instrument characteristics had remained essentially constant. Individual calibrations have been used for each of the cup anemometers. Wind speed signals were sampled at 16 Hz through a first order RC filter with a -3db frequency of 35 Hz. Each two consecutive 16 Hz scans were averaged so that the data are stored at 8 Hz.

1. [List of mast signals](./ts_wind/lamme/Mast_signals.csv)

## Nominal values

![](./ts_wind/Lamme/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/Lamme/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/Lamme/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wind/Lamme/w_dist.gif)
**Figure n4:** Nominal wind speed distribuion.

![A map of Denmark](./ts_wind/Lamme/ti_dist.gif)
**Figure n5:** Nominal turbulence distribuion.

![A map of Denmark](./ts_wind/Lamme/dir_dist.gif)
**Figure n6:** Nominal wind direction distribuion. 

## ACKNOWLEDMENTS	:	
- INSTITUTION       	:   Risoe National Laboratory
- ADDRESS 	        :   Postbox 49, DK-4000, Roskilde Denmark
- TEL/FAX 	        :   +45 4677 5017 / +45 4675 5619
- CONTACTS 	        :   Michael Courtney
- LINKS 	        :   http://www.risoe.dk/vea/
- COLLABS 	        :
- FUND AGENTS       	:   European Commision
- PERIOD 	        :   1987-06-01 - 1988-06-01
- Naming_authority  	: 'DTU Data'
- DOI               	: 'https://doi.org/10.11583/DTU.14230343'


## PUBLICATIONS	    : 

## Public data
1. Run statistics; lamme_all.nc (NetCDF) 
2. Raw time series = lamme.zip (zip) each with a duration of 3600 sec, sampled with 8 and 16 Hz.
