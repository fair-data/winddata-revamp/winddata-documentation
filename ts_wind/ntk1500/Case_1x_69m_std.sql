SELECT DISTINCT s3.`cid`-6000 as wt, COUNT(DISTINCT r.`rid`)AS periods, AVG(s.`ws`) AS ws57m, AVG(s.`wd`) AS wd57m, AVG(w.`ws_69`) AS wrfws69m, AVG(w.`wd_69`) 
AS wrfwd69m, COUNT(DISTINCT s2.`cid`) AS online_turbines, AVG(s3.`mean`) AS wsi, AVG(s1.`L`) AS L57, STD(s3.`mean`) AS wsi_std, AVG(s2.`mean`) AS POWER, STD(s2.`mean`) AS power_std
FROM `runs` r, `sonics_stats` s, `wrf_oct13_p1` w, `stability` s1, `scada` s2, `speeds` s3
WHERE
(s.`rn` = r.`rname`)
AND (r.`month` = 10)
AND (s.`cid` = 33.0)
AND (s.`wd` > 80) AND (s.`wd` <= 100)
AND (w.`rname` = r.`rname`)
AND (w.`ws_69` > 9.0) AND (w.`ws_69` <= 12)
AND (s1.`rid` = r.`rid`)
AND (s1.`cid` = 33.0) AND (s1.`cL` >= -1.0) AND (s1.`cL` <= 1.0)
AND (s2.`rid` = r.`rid`)
AND (s2.`cid` > 1000.0) AND (s2.`cid` < 2000.0)
AND (s2.`qa` >= 1.0) AND (s2.`qa` <= 1.0)
AND (s3.`rid` = r.`rid`)
AND (s3.`cid` > 6000.0) AND (s3.`cid` < 7000.0)
GROUP BY s3.`cid`
ORDER BY s3.`cid`, 2, 3, 4, 5, 6, 7, 8, 9, 10,11,12