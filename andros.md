## Background Information
- CLASSIFICATION: mountain(sharp contours - separation expected), pastoral(open fields and meadows)
- COUNTRY : Greece
- ALTITUDE : 325 [m]
- POSITION : [37˚ 57' 52'' N 24˚44'50'' E](https://geohack.toolforge.org/geohack.php?pagename=andros&params=37_57_52_N_24_44_50_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This dataset consists of time series and 10-minute statistics of wind speed and wind direction measurements from two 40m mast at Andros, Greece. The period includes more than 600 hours of measurements, which starts in 1994.

## Map
![A map of Denmark](./ts_wind/andros/msmap.gif)
**Figure 1:** A map of Greece
## Drawing
![A map of Denmark](./ts_wind/andros/andros_layout.gif)
**Drazwing 1:** Site layout

## Photos
![](./ts_wind/andros/andros.jpg) **Photo 01:** site view of masts and wt

## Reports
NA

## Mast (relative positions with reference to the reference POSITION) 
1. 40 [m] (12.25,26.4,-1.2)
2. 40 [m] (-3.55,28.9,-1.2)

## WIND TURBINES
1. Vestas V27 225 [kW] (0,0,0)

## Project description
The main objectives of the project were to a)develop draft design guidelines for wind turbines in simple terrain wind farms, b)investigate the behavior of wts in undulating and complex terrain wind farms and c)to extend simple terrain design guidelines to complex terrain

## Measurement system
Full scale measurements at a wind farm sited in mountainous terrain consisting of seven V27-225kW fixed speed, variable pitch wts. Measurements of wind speed and direction were carried out at three heights a.g.l. on two meteorological masts using six cup anemometers and six vanes, in order to define the operational characteristics of one of the seven wts in relation to the wind regime of the site.

1. [List of mast signals](./ts_wind/andros/ts_mast_signals.csv)

## Nominal values

![](./ts_wind/andros/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/andros/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/andros/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wind/andros/w_dist.gif)
**Figure n4:** Nominal wind speed distribution.

![A map of Denmark](./ts_wind/andros/ti_dist.gif)
**Figure n5:** Nominal turbulence distribution.

![A map of Denmark](./ts_wind/andros/dir_dist.gif)
**Figure n6:** Nominal wind direction distribution. 

## ACKNOWLEDMENTS	
- INSTITUTION       :	C.R.E.S, Greece
- ADDRESS 	        :   C.R.E.S., 19th km, Marathons Ave, 19009, Pikermi, Attika, Greece
- TEL/FAX 	        :   -6039871 / -6039876
- CONTACTS 	        :   E.E. Morfiakakis
- LINKS 	        :   http://www.hydro.andros.gr/
- COLLABS 	        :   Garrad Hassan (UK), ECN (NL), KEMA (NL), FFA (Sweden)
- FUND AGENTS       :   EU (DGXVII), Greek secretariat for Research & Technology
- PERIOD 	        :   1993-1995
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14493837'


## PUBLICATIONS	    : 
NA

## Public data
1. Run statistics; andros_all.nc (NetCDF)
2. Run statistics; andros_concurrent.nc (NetCDF)

Raw time series (ascii) each with a duration of 600 sec, sampled with 1 Hz:
3. andros.zip (zip)


