## Background Information
- CLASSIFICATION: flat(flat landscape), offshore(open sea) 
- COUNTRY : Sweden
- ALTITUDE : 0 [m]
- POSITION : [57˚ 2' 9.25'' N 18˚ 8'46.5'' E](https://geohack.toolforge.org/geohack.php?pagename=boockstig&params=57_2_9.25_N_18_8_46.5_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This dataset consists of raw data and 10-minute statistics of wind speed and wind direction measurements from a 50m offshore mast at Boockstigen, Gotland, Sweden. The period includes more than 1200 hours of offshore measurements, which starts in 2000.

## Map
![A map of Denmark](./ts_wind/bockstig/msmap.gif)
**Map 1:** Location of offshore site Bockstigen in Sweden.

## Photos
![](./ts_wind/bockstig/photo_01.jpg) **Photo 01:** The Bockstigen wind farm in strong winds (seen from the air).


## Drawings
![](./ts_wind/bockstig/layout.gif) **Drawing 01:** Site layout.
![](./ts_wind/bockstig/Draw_01.png) **Drawing 02:** Location of the wind farm, southwest of the island Gotland.
![](./ts_wind/bockstig/draw_02.png) **Drawing 03:** Orientation of meteorological mast and wind turbines.

## Reports
1. [Description of measurement setup - pdf file](./ts_wind/bockstig/bockstigen_setup.pdf)
2. [Instrumentation and initial evaluation of the 2.5 MW BOCKSTIGEN off-shore windfarm.](./ts_wind/bockstig/EWEC99.pdf)
3. [Vaisala combined cup vane VMS01 - description](./ts_wind/bockstig/Cupvanes.pdf)


## Mast (relative positions with reference to the reference POSITION) 
1. 50 [m] (0,0,0)

## WIND TURBINES
1. Wind World W-3700 500 [kW] (490.7,159.5,0)
2. Wind World W-3700 500 [kW] (171.5,-39.6,0)
3. Wind World W-3700 500 [kW] (-153.6,-236.5,0)
4. Wind World W-3700 500 [kW] (-178,86.8,0)
5. Wind World W-3700 500 [kW] (-215.2,483.3,0)

## Project description
The Bockstigen off-shore wind farm consists of five 500 kW wind turbines erected three km of the SW coast of the island of Gotland on the Baltic. The project was launched in order to gain experience from bottom mounted offshore wind turbine installation. Specifcally, the method of drilling a hole for a monopile foundation, directly into the limestone rock, was in focus for the investigations.

## Measurement system
Measurements are performed in one meteorological mast, and on one instrumented turbine (no measurements in the rotating system). Data are sampled by use of several loggers (DataScan 20 Hz), and the signals are transfered through an optical fibre cable (4500 m) to an onshore cabin. Data is continiously stored on video8 tapes.

1. [List of mast signals (time series of wind speed)](./ts_wind/bockstig/ts_mast_signals.csv)

## Nominal values

![](./ts_wind/bockstig/nom_speed.gif)

**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/bockstig/nom_ti.gif)

**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/bockstig/nom_dir.gif)

**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wind/bockstig/w_dist.gif)

**Figure n4:** Nominal wind speed distribution.

![A map of Denmark](./ts_wind/bockstig/ti_dist.gif)

**Figure n5:** Nominal turbulence distribution.

![A map of Denmark](./ts_wind/bockstig/dir_dist.gif)

**Figure n6:** Nominal wind direction distribution. 

## ACKNOWLEDMENTS	:	Teknikgruppen SE & FOI 
- INSTITUTION       :	FFA (FOI / Flygteknik)
- ADDRESS 	        :   S-172 90 Stockholm
- TEL/FAX 	        :   +46 8 55503000
- CONTACTS 	        :   Goran Ronsten
- COLLABS 	        :	MIUU , Teknikgruppen AB
- LINKS 	        :   http://www.met.uu.se/
- FUND AGENTS      	:	NUTEK , STEM
- PERIOD 	        :   2000 - 2004
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.15028272'

## PUBLICATIONS	    : 
1. [Power Performance and Park Efficiency of the Bockstigen Wind Farm](./ts_wind/bockstig/report_01.pdf)
2. [Drift och underhåll av mätsystemet vid Bockstigen-Valar](./ts_wind/bockstig/report_02.pdf)

## Public data
1. Run statistics bockstig.nc (NetCDF) 

2. Raw time series bockstig.zip (ascii), duration of 600 sec and 
sampling frequency 1 & 17.4Hz (tower accellerations).


