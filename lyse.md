## Background Information
- CLASSIFICATION: mountain(sharp contours - separation expected), coastal(water and land)
- COUNTRY : Sweden
- ALTITUDE : 9 [m]
- POSITION : [5˚ 18' 55.35'' N 11˚ 23'37.4'' E](https://geohack.toolforge.org/geohack.php?pagename/lyse&params=58_18_55.35_N_11_23_37.4_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This dataset consists of time series and 10-minute statistics of wind speed and wind direction measurements from a met mast at Lysekil in the Swedish archipelago. The period includes 4 years of measurements, which starts in 1993.

## Map
![A map of Denmark](./ts_wind/lyse/msmap.gif)
**Figure 1:** A map of Sweden
![A map of Denmark](./ts_wind/lyse/map_2.gif)
**Figure 2:** An old map of the Lyse area, 1 indicates the tower and 2 the island Blackhall.
![A map of Denmark](./ts_wind/lyse/map_3.gif)
**Figure 3:** Site map of Lysekil and Gothenborg

## Short summary
This dataset consists of raw data and 10-minute statistics of wind speed and wind direction measurements from a met mast located at Lysekil, Sweden. The period includes 4 years of measurements, which starts in 1993.

## Photos
![](./ts_wind/lyse/photo_1.gif) **Photo 01:** Nordic 400 in the complex terrain of Vattensfall's Wind Power station at Lysekil in the South West S
![](./ts_wind/lyse/photo_2.gif) **Photo 02:** Since 1992 the Nordic 400 prototype has been operated by Vattenfall in the rugged terrain at Lysekil
![](./ts_wind/lyse/photo_3.gif) **Photo 03:** 65 m Mast on Lysekil.

## Drawings
![](./ts_wind/lyse/map_1.gif) **Drawing 01:** Site layout for Lysekil installation.

## Mast (relative positions with reference to the reference POSITION) 
1. 65 [m] (0,0,0)

## Windturbines
1. NWP 400kW D=35m HH=40m
2. Bonus 400KW, MkII

## Project description
At Lyse Wind Power Station the NWP 400 wind turbine was erected in 1992. The wind turbine is a two-bladed, upwind machine with a hub height of 40 m, and a rotor diameter of 35 m.

## Measurement system
Lyse wind power station is situated at an artificial island created around two islets. On each of the rocky islets wind turbines are erected. The above mentioned NWP400 to the north and a Bonus 400 kW MkII to the south. Between the two turbines a 66 m high meteorological tower is situated, which is equipped with wind speed and direction sensors of the MIUU type (Lundin et al., 1990) at 7 levels. At the uppermost 5 levels two anemometers are placed at each level. Temperature profile is also recorded. All measurements are sampled with 1 Hz and stored on 1 GB streamer tape.

1. [List of mast signals](./ts_wind/lyse/ts_mast_signals.csv)

2. [List of additional signals](./ts_wind/lyse/add_signals.csv) Note: The statistics for these signals are only included in the header of the packed ASCII file.
## Nominal values

![](./ts_wind/lyse/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/lyse/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/lyse/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wind/lyse/w_dist.gif)
**Figure n4:** Nominal wind speed distribuion.

![A map of Denmark](./ts_wind/lyse/ti_dist.gif)
**Figure n5:** Nominal turbulence distribuion.

![A map of Denmark](./ts_wind/lyse/dir_dist.gif)
**Figure n6:** Nominal wind direction distribuion. 

## ACKNOWLEDMENTS		
- INSTITUTION       :	Dept. of Meteorology, Uppsala University
- ADDRESS 	        :   Uppsala University, Box 516 S-751 20 Uppsala, Sweden
- TEL/FAX 	        :   +46-18 542792 / +46-18 544 706
- CONTACTS 	        :   Mikael Magnusson / Ann-Sofi Smedman
- LINKS 	        :   http://www.met.uu.se/
- PERIOD 	        :   1993-01-01 00:00:00 - 1995-12-31 00:00:00
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14153252'

## PUBLICATIONS	    : 
1. [Wind characterisation for design and comparison with standards](./ts_wind/lyse/lyse_report.pdf)
2. [High resolution climatological wind measurements](./ts_wind/lyse/lyse_report_2.pdf)

## Public data
1. Run statistics; lyse.nc (NetCDF) 
2. Time series; 1993_lyse.zip; 1994_lyse.zip; 1995_lyse.zip & 1996_lyse.zip (stored in zipped files) 

