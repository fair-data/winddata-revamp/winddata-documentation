## Background Information
- CLASSIFICATION: hill(rolling hills), coastal(water and land)
- COUNTRY : Greenland
- ALTITUDE : 0 [m]
- POSITION : [69˚ 15' 3'' N 53˚ 30' 49'' E](https://geohack.toolforge.org/geohack.php?pagename=godhavn&params=69_15_3_N_53_30_49_W_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This resource dataset consists of 10 minute wind speed and wind direction measurements from a small met mast at Qeqertarsuaq (Godhavn),Greenland. The period includes 10 years of measurements, which starts in 1991.

## Map
![A map of Denmark](./Resource/godhavn/disko.gif)
**Figure 1:** A map of SW Greenland including Godhavn and Diskofjord

![A map of Denmark](./Resource/godhavn/map_1.jpg)
**Figure 2:** Location of the climae station at the arctic station.

## Photos
![](./Resource/godhavn/photo_01.jpg) **Photo 01:** The Arctic Station main building as viewed from the Heliport. Photo: H. A. Thomsen
![](./Resource/godhavn/photo_02.jpg) **Photo 02:** The Arctic Station main building. The Qeqertarsuaq soccer field and large, grounded icebergs from the Ilulissat (Jakobshavn) glacier. Photo: H. A. Thomsen
![](./Resource/godhavn/photo_03.jpg) **Photo 03:** Examples of monitoring photos from 1993 and 1994.


## Reports
1. [Meteorological observations in 2000 at the Arctic Station, Qeqertarsuaq entral West Greenland](./Resource/godhavn/13note.pdf)
2. [Aanderaa wind speed sensor 2740](./Resource/godhavn/Wind_Speed_2740.pdf)
3. [Aanderaa wind Direction sensor 3590](./Resource/godhavn/Wind_Dir_3590_D300.pdf.pdf)


## Mast (relative positions with reference to the reference POSITION) 
1. 10 [m] (0,0,0) (mast 1.)

## Project description
In October 1990 a new automatic meteorological station was established at the Arctic Station, Qeqertarsuaq (Godhavn), managed by the University of Copenhagen. The Arctic Station is situated at the south coast of Disko Island about one km east of the town Qeqertarsuaq (69° 15' N.lat., 53°34'W.lon.). The town stands on a small rocky promontory and due to the western position of Disko Island the area is exposed to an arctic maritime climate. From Qeqertarsuaq the bay, Disko Bugt, stretches towards south to Aasiaat (60 km) toward southeast to Qasigiannguit (105 km) and toward east to Ilulissat (90 km). Winds from west pass the Davis Strait and cross an open sea surface for about 600 km. The largest fetch is found in a SSW direction with an extension of more than 2000 km. The meteorological station is installed at one of the Arctic Station's buildings, situated about 300 m from the coast, 20 m a.s.l. Generally the terrain rises to an altitude of 200 m about 1.5 km N of the coast. Further N altitudes increase rapidly to the edge of a mountain plateau at 600 800 m above sea level. The orientation of this steep slope extends for several kilometres both E and W, only interrupted by valley mouths (Fig. 1). The Arctic Station and Qeqertarsuaq stand on a low bench of bedrock (gneiss),but the main frame of the landscape is shaped in Tertiary basalt breccia and plateau lava. Reference: Geografisk Tidsskrift Vol. 95, page 95-104 (ISBN 0016-7223)

## Measurement system
The meteorological data are monitored by AANDERAA scanning unit and a storage unit. The following parameters are logged - wind speed (30 minute average) - maximum gust speed (2 seconds) - wind direction (30 minute average) - air temperature (30 minute average) - relative humidity (30 minute average) All mounted 9.5 m above terrain - pluviograph - pyranometer (incoming solar radiation) - pyranometer (reflected solar radiation) all mounted on a mast, 2 m above terrain plus temperatures (5, 60, 175 & 300cm below ground surface) Reference: Geografisk Tidsskrift Vol. 95, page 95-104 (ISBN 0016-7223)

1. [List of mast signals](./Resource/godhavn/Mast_signals.csv)


## ACKNOWLEDMENTS		
- Acknowledgement    : Niels Nielsen, Institute of Geography, University of Copenhagen.
- E-mail: nn@geogr.ku.dk 
- INSTITUTION       : Institute of Geography, University of Copenhagen	
- ADDRESS 	        : Øster Voldgade 10, DK-1350 Copenhagen K
- TEL/FAX 	        : Ole Humlum / N.Nielsen
- CONTACTS 	        : Niels Nielsen
- LINKS 	        : http://www.geogr.ku.dk/
- COLLABS 	        : Copenhagen University
- FUND AGENTS       : Internal
- PERIOD 	        : 1991-2001
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14153210'

## Public data
1. Resource data  (NetCDF) 