## Background Information
- CLASSIFICATION: hill(rolling hills), scrub(bushes and small trees)
- COUNTRY : Greece
- ALTITUDE : 219 [m]
- POSITION : [37˚ 58' 26'' N 23˚ 47'16'' E](https://geohack.toolforge.org/geohack.php?pagename=ntua&params=37_58_26_N_23_47_16_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This resource dataset consists of 10-minute statistics of wind speed and wind direction measurements from a small met mast at NTUA in Athens, Greece. The period includes more than 5 years of measurements, which starts in 1993.

## Map
![A map of Denmark](./Resource/ntua/msmap.gif)
**Figure 1:** A map of Greece

![A map of Denmark](./Resource/ntua/map_01.jpg)
**Figure 1:** A map of NTUA campus including the met station

## Photos
![](./Resource/ntua/photo_01.jpg) **Photo 01:** View towards SW. anemometer located at h=3 m.
![](./Resource/ntua/photo_02.jpg) **Photo 02:** View towards SE.
![](./Resource/ntua/photo_03.jpg) **Photo 03:** View of the mast.

## Reports
1. [Presentation of Anderaa cup anemometer](./Resource/ntua/cup.pdf)
2. [Presentation of Anderaa wind vane](./Resource/ntua/vane.pdf)
3. [Presentation of Anderaa bucket](./Resource/ntua/buckett.pdf)

## Mast (relative positions with reference to the reference POSITION) 
1. 5 [m] (0,0,0)

## Project description
The automatic meteorological station is operated by the Department of Water Resources, NTUA, Greece. The NTUA Online Weather Data is a project of assist. prof. Demetris Koutsoyiannis. Dr. Nikos Mamassis is responsible for the supervision and maintenance of the station. Antonios Christofides is responsible for the software and the web site. Many other people have also supported the project. K. Konstandinides installed the station and helped whenever there was a malfunction. Nassos Papakostas created the first fingering service with online weather data, and recently offered significant help in communication matters. Panagiotis Christias developed the first version of the web site, including the charts. Rania Tsoumani, then a student, made measurements with traditional instruments and compared them to the sensors' indications. K. Kouridakis constructed the station site. D. Kouvas of SCIENTACT SA installed the new logger and the new instruments and helped in various technical issues. Anna Patrikiou supervised the development of the new web site in page appearance matters. Gianna Stamataki prepared the location maps. Andreas Sakellariou and Alexandros Manetas offered additional computer support. We would also like to thank Konstandina Sakka, responsible for the telephone network of NTUA, for her understanding and her suggestions, and generally the whole of the Network Operations Center of NTUA for their support in various issues on communications.

## Measurement system
An Aanderaa logger and sensor scanning unit are used for the old digital sensors, with a local data storage unit. The sensors are scanned every ten minutes, after a signal produced by a timer built in the scanning unit. The measurements are sent to the radio transmitter and to the data storage unit. The data are received by a PC at the Laboratory, 1 km away, via the radio receiver. A Delta-T logger for the new analog sensors (2000), with built-in memory for data storage, and built-in clock. The sensors are read every five minutes (sampling period) and their measurements are stored every ten minutes. The communication with the Linux PC at the Laboratory is achieved via a serial interface, modems and cable. 2001-11-02 Configuration change: Wind direction was set to sampling every 5 minutes, storage every 10 minutes, for both sensors. This means that the logger produced the average of two measurements, which is incorrect; for example, if the two measurements were 355 and 5, the average would be 180, whereas the correct value would be 0. Because of this problem, all wind direction measurements for these two sensors (15 and 17) are unreliable until now. The configuration was corrected a few minutes after 17:20 EET, so that sampling from one value only is performed. Due to stopping and restarting of logging, some variables at 17:30 are incorrect.

1. [List of mast signals](./Resource/ntua/Mast_signals.csv)

## ACKNOWLEDMENTS	
- INSTITUTION       :	Department of Water Resources
- ADDRESS 	        :   Iroon Politechniou 5, 157 80 Zografoy, Athens,Greece
- TEL/FAX 	        :   +30 (1) 772 2848
- CONTACTS 	        :   Nikos Mamassis / Antonios Christofides
- LINKS 	        :   http://www.hydro.ntua.gr/
- COLLABS 	        :   NTUA, Greece
- FUND AGENTS       :   Breek Government
- PERIOD 	        :   1993 - 2000
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14135555'

## PUBLICATIONS	    : 
1. 

## Public data
1. Resource data  (NetCDF) 


