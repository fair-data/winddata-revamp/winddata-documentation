
# Description of Egmont Aan Zee (OWEZ) offshore wind farm dataset
## Data access
Access to the SCADA data from OWEZ offshore wind farm are covered by a NDA with the owner NoordZeeWind B.V. 
- Contact person: Gunner Chr. Larsen; DTU Wind Energy; [ORCID:0000-0002-6482-8208](http://orcid.org/0000-0002-6482-8208)
- Data administrtion: Steen-Arne Sørensen DTU Wind Energy 

//Kurt S. Hansen, DTU Wind Energy; [ORCID:0000-0001-7109-3855](http://orcid.org/0000-0001-7109-3855)
## NZ offshore Wind farm
Windpark Egmond aan Zee (OWEZ) is the first large scale offshore wind farm built off the Dutch North Sea coast. It consists of 36 Vestas V90-3MW wind turbines, each with nameplate capacity of 3 MW. In total the farm has a capacity of 108 MW. 
The SCADA measurements represent a period from 1. January 2005 - 31. December 2011 
The OWEZ wind farm include 36 x 3.0 MW wind turbines and 1 mast. 

![wind farm layout](./OWEZ/OWEZ_layout.png)
**Figure 1:** Egmond aan Zee wind farm layout including the mast
1. [List of wind turbine and mast positions](./OWEZ/sites.csv)


## OWEZ wind turbines
- Vestas: 3.0MW, 
- Diameter =90m, 
- hub height =70m, 
- Control =Var pitch & var speed
- Data period: 2007-2008

## OWEZ mast
The 116m mast includes signals from 21, 70 & 116 amsl.
- wind speed,
- wind direction
- temperatures
- stratification  

## Contents of database
The database consists of 10 - minute SCADA values from the wind  turbines and 10-minute statistics from the met mast. 
Signals from the wind turbine includes: 
- Active power, 
- pitch, 
- rotor speed, 
- yaw position
- nacelle wind speed

Furthermore,
- Structural loads from WT7 used in the liste publication 
- Structural loads from WT8 - not validated


## Data quality
All the SCADA and other values have been quality controlled and the quality parameter (quality or qa) have been updated with reference to the definition in TB=modes.

## Access to the MySQL database
- Database server: 	130.226.56.150
- Database name: 	NoordZee 

Dagtabase access tools	SQL eq. HeidiSQL or Matlab

## Database structure
![wind farm layout](./OWEZ/offshore_db.png)
**Figure 2:** Structure of the OWEZ database

The database structure includes 10 different tables where the most important tables have been listed below:

- TB=amok, contains stability data based on air & water temperature and wind speed @21m from mast measurements with the Amok software. 
+ TB=channels; defines each signal including heigh and mounting and table location of the variables [channel_id], where each signal is characterized with name, unit and type.	
+ TB=dirs. includes all directions from sonics and vanes; optional values is calibrated wind turbine yaw position.	
- TB=modes, operational modes with reference to the quality parameter of the power signal.
- TB=operation, include scada values for the operational parameters: pitch, rotorspeed, yaw position ao.
- TB=powers, includes all 10-minute averaged power values where quality=1 indicates the power has been accepted. 
- TB=quality = modes.
- TB=runs, defines each 10-minute time stamp including year, month, day, hour and minute [rid].
- TB=sites;  includes an identification of park and masts [site_id].
+ TB=speeds, includes all speed values rom the masts and the wind tubine nacelle wind speed (wsn). 
	

## Publications
1. Larsen, T. J., Aagaard Madsen , H., Larsen, G. C., & Hansen, K. S. (2013). Validation of the dynamic wake meander model for loads and power production in the Egmond aan Zee wind farm. Wind Energy, 16(4), 605-624. https://doi.org/10.1002/we.1563

