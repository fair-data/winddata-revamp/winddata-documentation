## Introduction <a name="introduction"></a>
This document list a number of internal database with restricted access, hosted by DTU Wind Energy. The databases are accessible with reference to NDA contracts between the data provider and DTU Wind Energy and are stored behind our firewall.

IMPORTANT Access to these database are restricted to employees at DTU Wind Energy. 

//Kurt S. Hansen

- Data DOI               : 'https://doi.org/10.11583/DTU.14578848'

## List of databases

1. [Horns Rev 1 OWF](HR1.md)
Danish offshore wind farm from 2003 with 80 x 2 MW wind turbines

2. [Lillgrund OWF](Lillgrund.md)
Swedish offshore wind farm from 20xx with 48 x 2.3 MW wind turbines, owned by Vattenfall.

3. [Rødsand OWF](RS2.md)
Danish offshore wind farm from 20xx with 90 x 2.3 MW wind turbines, owned by EON etal.

4. [Egmond aan Zee](OWEZ.md)
Dutch offshore wind farm from 2007 with 36 x 3.0 MW wind turbines, owned by NoordZeeWind B.V.

5. [HR_M2](HRM2.md)
Resource data from Horns Rev mast M2 (1999-2004) owned by Energinet.dk.

6. [LS_M1](LSM1.md)
Resource data from Laesoe Syd mast M1 (1999-2004) owned by Energinet.dk.

7. [San Gregorio Magno](SG.md)
Italien onshore (complex terrain) wind farm from 2009 with 17 x 2.3 MW wind turbines, owned by Sorgenia Green SRL, Italy

8. [Navarra](https://gitlab.windenergy.dtu.dk/fair-data/winddata-revamp/winddata-documentation/-/blob/master/NDAs/narvarra.md)
Spanish onshore wind farm (complex terrain) from 2004 with 43 small wind turbines.

9. [Robres](robres.md)
Spanish onshore (complex terrain) wind farm from 2007 with 13 x V90 wind turbines.

