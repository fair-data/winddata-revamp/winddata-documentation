## Table of contents
1. [Introduction](#introduction)
2. [Nysted OWF](#Nysted)
3. [Horns Rev 1 OWF](#HR1)
4. [Anhold OWF](#ANH)
2. [Lillgrund OWF](#Lillgrund)
2. [Rødsand OWF](#RS2)


## Introduction <a name="introduction"></a>


This document list a number of internal database in DTU Wind Energy, which are internal available with reference to NDA contract with the provider.
Access to these database are restricted to employees at DTU Wind Energy.


# Description of Nysted offshore wind farm dataset <a name="Nysted"></a>
## Nysted NDA
Access to the SCADA data from Nysted offshore wind farm are covered by a NDA with former owner E2/DONG.
Contact person: Charlotte B. Hasager 
Data administrtion: Steen-Arne Sørensen DTU Wind Energy 

## Nysted Wind farm
The Nysted Wind Farm (also known as Rødsand I) is a Danish offshore wind farm close to the Rødsand sand bank near Lolland. Gravity base foundations are used rather than piles due to ice conditions. The wind farm was commisioned in 1983.<br />
The wind farm includes 72 2.3 MW Bonus wind turbines and 5 x 70m offshore masts.


![wind farm layout](./Nysted/ny_layout.png)
**Figure 1:** Nysted wind farm layout including wake masts
1. [List of wind turbine and past positions](./Nysted/sites.csv)


## Nysted wind turbines
- Bonus: 2.3MW, 
- Diameter =84m, 
- hub height =69m, 
- Control =Active stall
- Data period: 2004-2006

## Nysted masts
- MM1 & MM2: Mast with wind speed, wind direction & temperatures , h=70m
- MM3 & MM4: wake masts with wind speed, wind direction & temperatures, h=70 m.
- MM5: Located inside the WF and with poor availability


## Contents of database
The database consists of 10 - minute SCADA values from the wind  turbinesand 10-minute statistics from the 5 met mast. 
Signal from turbine includes 
- Active power, 
- pitch, 
- rotor speed, 
- yaw position
- nacelle wind speed

Access to the MySQL database:
- Database server: 	130.226.56.150
- Dabatase name: 	Nysted
- Access tools	SQL eq. HeidiSQL or Matlab

## Database structure
![wind farm layout](./Nysted/offshore_db.png)
**Figure 2:** Nysted wind farm layout including wake masts
### Basic tables:	
- Runs:	Time stamps; id, runname, yr, mo,..  
- Channels: 	Definition list of available signals: numbers, names & location.
- Speeds:	Wind speed; mean, max, min, stdev & ti values
- Dirs:	Wind direction; mean, max, min, stdev.
- Powers:	Power values; mean, max, min, stdev.
- Operational params:	Pitch, rotorspeed & yaw position; mean & stdev.
- Temps: temperatures; Mean & stdev
- Adds: others meteorological params

Publications
	1) Evaluation of wind farm efficiency and wind turbine wakes at the Nysted offshore wind farm by 
	Rebecca Jane Barthelmie, L.E. Jensen; 10.1002/we.408
	2) Model-based Analysis of Wake-flow Data in the Nysted Offshore Wind Farm, March 2009 Wind Energy 12(2):125-135, DOI: 10.1002/we.314
	
