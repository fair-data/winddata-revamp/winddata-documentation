
# Description of Horns Rev 1 offshore wind farm dataset <a name="HR1"></a>
## Data access
Access to the SCADA data from HR1 offshore wind farm are covered by a NDA with the owners Vattenfall and E2/DONG/Ørsted. 
- Contact person: Charlotte B. Hasager; [ORCID:0000-0002-2124-5651](http://orcid.org/0000-0002-2124-5651) 
- Data administrtion: Steen-Arne Sørensen DTU Wind Energy 

//Kurt S. Hansen, DTU Wind Energy; [ORCID:0000-0001-7109-3855](http://orcid.org/0000-0001-7109-3855)

## Horns Rev 1 offshore Wind farm
The wind farm consists of  80 x V80-2MW wind turbines located  near the offshore location name Horns Reef, West of Jutland Denmark.
The SCADA measurements represent a period from 1. January 2005 - 31. December 2011 
The wind farm include 80 x 2MW wind turbines and 3 masts. 

![wind farm layout](./HR1/layout.png)
**Figure 1:** HR1 wind farm layout including two wake masts
1. [List of wind turbine and mast positions](./HR1/sites.csv)


## HR1 wind turbines
- Vestas: 2.0MW, 
- Diameter =80m, 
- hub height =70m, 
- Control =Active pitch
- Data period: 

## HR1 masts
- M2 : Mast with wind speed, wind direction & temperatures, h=62m used for resource measurement but with poor quality 
- M6 & M7: wake masts with wind speed, wind direction & temperatures, h=70 m.

## Contents of database
The database consists of 10 - minute SCADA values from the wind  turbinesand 10-minute statistics from the 5 met mast. 
Signals from turbine includes 
- Active power, 
- pitch, 
- rotor speed, 
- yaw position
- nacelle wind speed


## Data quality
All the SCADA values have been quality controlled and the quality parameter (quality or qa) have been updated with reference to the definition in TB=modes.

## Access to the MySQL database
- Database server: 	130.226.56.150
- Databases 
1. HRM2 - resource data from M2, initiated before wf installation.
2. hornsrev - public accesible HR1 database
3. wake - initial HR1 database - working edition
3. hr_wt14 - includes load statistics from wind turbine #14 (low quality)
- Access tools	SQL eq. HeidiSQL or Matlab

## Database structure
![wind farm layout](./HR1/offshore_db.png)
**Figure 2:** Structure of the HR1 database

The database structure includes 12 different tables where the most important tables have been listed below:

- TB=amok, contains stability data based on air & water temperature and wind speed @30m from M7 measurements with the Amok software. 
+ TB=channels; defines each signal including heigh and mounting and table location of the variables [channel_id], where each signal is characterized with name, unit and type.	
+ TB=dirs. includes all directions from sonics and vanes; optional values is calibrated wind turbine yaw position.
	- signal=wpd #766 is the dreived inflow direction to the wind farm
- TB=modes, operational modes with reference to the quality parameter of the power signal.
- TB=operation, include scada values for the operational parameters: pitch, rotorspeed, yaw position ao.
- TB=powers, includes all 10-minute averaged power values where quality=1 indicates the power has been accepted. Please note records with quality=0 have not been validated eg. in 2010-2011.  
- TB=quality = modes.
- TB=runs, defines each 10-minute time stamp including year, month, day, hour and minute [rid].
- TB=sites;  includes an identification of park and masts [site_id].
+ TB=speeds, includes all speed values rom the masts and the wind tubine nacelle wind speed (wsn). 
	- signal=wsp #1900 is the derived inflow wind speed at hub height - to the wind farm.
 

## Publications
1. The impact of turbulence intensity and atmospheric stability on power deficits due to wind turbine wakes at Horns Rev wind farm by Hansen, K. S., Barthelmie, R. J., Jensen, L. E. & Sommer, A., 2012, In : Wind Energy. 15, 1, p. 183-196
2. Evaluation of the wind direction uncertainty and its impact on wake modeling at the Horns Rev offshore wind farm by Gaumond, M., Réthoré, P-E., Ott, S., Peña, A., Bechmann, A. & Hansen, K. S., 2014, In : Wind Energy. 17, 8, p. 1169–1178
3. Modelling and Measuring Flow and Wind Turbine Wakes in Large Wind Farms Offshore by Barthelmie, R. J., Hansen, K. S., Frandsen, S. T., Rathmann, O., Schepers, J. G., Schlez, W., Phillips, J., Rados, K., Zervos, A., Politis, E. S. & Chaviaropoulos, P. K., 2009, In : Wind Energy. 12, 5, p. 431-444
4. Wall effects in offshore wind farms by K. Mitraszewski, K.S. Hansen, N.G. Nygaard and P.E. Rethoré, Resesarchgate, January 2013
5. Offshore Wind Profiling Using Light Detection and Ranging Measurements by A Peña, CB Hasager, SE Gryning 
WIND ENERGY (2008) DOI: 10.1002/we.283.

