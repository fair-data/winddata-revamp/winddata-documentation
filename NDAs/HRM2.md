
# Description of the Wind Resources at Horns Rev 
## Data access
Access to the wind resource data from Horns Rev M2 mast is covered by a NDA with the owner: Energinet.dk 
- Contact person: Charlotte B. Hasager; [ORCID:0000-0002-2124-5651](http://orcid.org/0000-0002-2124-5651)  
- Data administrtion: Steen-Arne Sørensen DTU Wind Energy 

//Kurt S. Hansen, DTU Wind Energy; [ORCID:0000-0001-7109-3855](http://orcid.org/0000-0001-7109-3855)

## Horns Rev Mast M2
The meteorological mast (M2) at Horns Rev is located approximately 20 km west of Blåvands Huk. Its position is 7°47,217'E 55°31,208'N (WGS84) and its UTM coordinates are 423.412E 6.153.342N (WGS84 Zone 32).The offshore mast is located NW to the existing HR1 wind farm.
![Location](./HR_m2/HRM2_location.png)
**Figure 1:** Location of the Horns Rev mast M2.
- M2 : Mast with wind speed, wind direction & temperatures, h=62m used for resource measurement diring the period 1999 - 2003.  

![HR M2 layout](./HR_m2/HR_M2.png)
**Figure 2:** Layout of the Horns Rev mast M2.

## Contents of database
The database consists of 10 - minute statistics of 
- wind speed, 
- wind direction, 
- air temperatures and differences
- water temperature.
- Barometer pressure
- Relative humidity

## Data quality
All the values have been quality controlled and the quality parameter (quality or qa) have been updated with reference to the definition in TB=modes.

## Access to the MySQL database
- Database server: 	130.226.56.150
- Database name: 	hr_m2 - (public accesible database)
Access tools	SQL eq. HeidiSQL or Matlab

## Database structure
![wind farm layout](./HR_m2/siite.png)
**Figure 2:** Structure of the HR-M2 database

The database structure includes more than 7 tables where the most important tables have been listed below:

+ TB=channels; defines each signal including heigh and mounting and table location of the variables [channel_id], where each signal is characterized with name, unit and type.
	- [List of mast channels](./HR_m2/channels.csv)	
+ TB=dirs. includes all directions from sonics and vanes; 
- TB=modes, operational modes with reference to the quality parameter of the power signal.
- TB=quality = modes.
- TB=runs, defines each 10-minute time stamp including year, month, day, hour and minute [rid].
+ TB=speeds, includes all speed values from the masts; 
+ TB=temps, includes all temperature values from the masts. 
+ TB=adds, includes all other meteorological values from the masts.
 

## Publications
1. Wind Resources at Horns Rev, Eltra PSO-2000 Proj. nr. EG-05 3248 Programme for measuring wind, wave and current at Horns Rev. 
[(3MB)](./HR_m2/HR_ELTRA_PSO_2002_Tech-wise.pdf)



