
# Description of Rødsand II (RS2) offshore wind farm dataset. 
## RS2 NDA conditions
Access to the SCADA data from Nysted offshore wind farm are covered by a NDA with EON
- Contact: Gunner Chr. Larsen; DTU Wind Energy; [ORCID:0000-0002-6482-8208](http://orcid.org/0000-0002-6482-8208) 
- Data administration: Steen-Arne Sørensen DTU Wind Energy 

//Kurt S. Hansen, DTU Wind Energy; [ORCID:0000-0001-7109-3855](http://orcid.org/0000-0001-7109-3855)

## RS2 Wind farm

Rødsand II was installed in 2010, as an extension 207 MW extension of the existing wind farm towards west. The turbines are controlled with a combination of variable speed and pitch. The turbines are located on 5 arcs, with increased spacing as shown in Figure 1. The arcs are named I, J, K, L and M where the internal spacing increases from 5D towards 10D. The wind turbines are identical to the turbines in Lillgrund offshore wind farm.

![wind farm layout](./RS2/red2_mast.png)
**Figure 1:** Redsand 2 wind farm layout including wake masts

![wind farm layout](./RS2/Rødsand_II.jpg)
**Photo 1:** Redsand 2 wind farm seejn towards East with Nysted OWF in the East of RS2 by courtisy from E-ON

1. [List of wind turbine and mast position](./RS2/sites.csv)


## RS2 wind turbines
- Siemens SWT-2.3MW, 
- Diameter =92.6mm, 
- hub height =68.5m, 
- Control =Var. pitch and var speed
- Data period: 2004-2006

## RS2 masts
- The mast with wind speed, wind direction & temperatures recordings are located towards W, h=7m and have been equipped with 3D sonic anemometer in  15, 40 & 57 m height.

## Contents of database
The database consists of 10 - minute SCADA values from the wind  turbines and 10-minute statistics from the met mast. 
Signal from turbine includes 
- Active power, 
- pitch, 
- rotor speed, 
- yaw position
- nacelle wind speed

## Data quality
All the SCADA values have been quality controlled and the quality parameter (quality or qa) have been updated with reference to the definition in TB=modes.

## Access to the MySQL database
- Database server: 	130.226.56.150
- Dabatase name: 	redsand2
- Access tools	SQL eq. HeidiSQL or Matlab

## Database structure
![wind farm layout](./RS2/offshore_db.png)
**Figure 2:** Nysted wind farm layout including wake masts
### Basic tables:	
- TB=amok, contains stability data based on air & water temperature and wind speed @30m from M7 measurements with the Amok software. 
+ TB=channels; defines each signal including heigh and mounting and table location of the variables [channel_id], where each signal is characterized with name, unit and type.
	- signal=wsp #11 is the derived inflow wind speed at hub height - to the wind farm.
	- signal=wpd #12 is the derived inflow wind direction to the wind farm
+ TB=dirs. includes all directions from sonics and vanes; optional values is calibrated wind turbine yaw position.
- TB=modes, operational modes with reference to the quality parameter of the power signal.
- TB=operation, include scada values for the operational parameters: pitch, rotorspeed, yaw position ao.
- TB=powers, includes all 10-minute averaged power values where quality=1 indicates the power has been accepted.Please note records with quality=0 have not been validated eg. in 2010-2011.  
- TB=runs, defines each 10-minute time stamp including year, month, day, hour and minute [rid].
- TB=sites;  includes an identification of park and masts [site_id].
+ TB=speeds, includes all speed values rom the masts and the wind tubine nacelle wind speed (wsn). 
- TB=temps, includes all measured temperatures from the mast and the water.
- TB=wind-turbines (tid=turbine id); includes coordinates for each wind turbine
- TB=modes; reflects turbine operational quality (power) 
- TB=windiris includes nacelle lidar measurements for L01 (inflow &wake) & L02 (inflow & wake) used in IEA31/wakebench
- TB=wrf_speed, wrf_speed_2010, wrf_speed_2013 & wrf_stab includes sync data for simulated wrf values next to the RS2 WF - used in project Modfarm.

## Publications
1. Design tool for offshore wind farm cluster planning
Hasager, C. B., Giebel, G., Hansen, K. S., Madsen, P. H., Schepers, G., Cantero, E., Waldl, I. & Anaya-Lara, O., 2015
2. Simulation of wake effects between two wind farms
Kurt Schaldemose Hansen, Pierre-Elouan Réthoré, Jose Palma, B.G. Hevia, J. Prospathopoulos, Alfredo Pena Diaz, Søren Ott, G. Schepers, A. Palomares, Paul van der Laan, Patrick Volker


	
	
