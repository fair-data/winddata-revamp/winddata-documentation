
# Description of San Gregorio (SG) onshore wind farm dataset
## SG NDA 
Access to the SCADA data from SG onshore wind farm are covered by a NDA with the owner Sorgenia Green SRL, Italy.
- Contact person: Francesco Castellani; Univ. Perugia, Italy; [ORCID:0000-0002-4748-8256](http://orcid.org/0000-0002-4748-8256)  
- Data administrtion: Steen-Arne Sørensen, DTU Wind Energy 

\\Kurt S. Hansen, DTU Wind Energy; [ORCID:0000-0001-7109-3855](http://orcid.org/0000-0001-7109-3855)
## SG Wind farm

The San Gregorio Magno onshore wind farm, composed of 17 turbines located on public land, is located on four ridges at an altitude of between 700 and 1,100 metres above sea level with a Capacity of 39.1 MW
This site's weather conditions are extreme; although they guarantee excellent levels of production, the condition of both the turbines and the land need to be heavily monitored.
For this reason, the turbine selected for the site has one of the most advanced systems for real time monitoring of how the machine is operating and its stresses, as well as keeping track of the meteorological conditions (temperature, pressure, vibrations, lightning)


![wind farm layout](./SG/layout.png)
**Figure 1:** The SG wind farm layout including wake masts
![wind farm layout](./SG/San_Gregorio.png)
**Figure 2:** The SG wind farm located in hilly terrain
1. [List of wind turbine and mast positions](./SG/sites.csv)


## SG wind turbines
- Siemens SWT-2.3MW, 
- Diameter =92.3m, 
- hub height =80m, 
- Control =var pitch & var speed
- Data period: 2013

## SG masts
- SNG1 - SNG6: as single masts with wind speed, wind direction & temperatures recordings distributed between the turbines. SNG7 is sampled by SWP scada system.

## Contents of database
The database consists of 10 - minute SCADA values from the wind  turbines and 10-minute statistics from the 7 met mast. 
Signal from SG turbine includes 
- Active power, 
- pitch, 
- rotor speed, 
- yaw position
- nacelle wind speed

## Data quality
All the SCADA values have been quality controlled and the quality parameter (quality or qa) have been updated with reference to the definition in TB=modes.

## Access to the MySQL database:
- Database server: 	130.226.56.150
- Dabatase name: 	Gregorio
- Access tools	SQL eq. HeidiSQL or Matlab

## Database structure
![wind farm layout](./Nysted/offshore_db.png)
**Figure 2:** Structure of DB=gregorio. 

## Basic tables:	
+ TB=channels; defines each signal including heigh and mounting and table location of the variables [channel_id], where each signal is characterized with name, unit and type.
- TB=dirs. includes all directions from sonics and vanes; optional values is calibrated wind turbine yaw position.
- TB=modes, operational modes with reference to the quality parameter of the power signal.
- TB=operation, include scada values for the operational parameters: pitch, rotorspeed, yaw position ao.
- TB=powers, includes all 10-minute averaged power values where quality=1 indicates the power has been accepted.Please note records with quality=0 have not been validated eg. in 2010-2011.  
- TB=quality = modes.
- TB=runs, defines each 10-minute time stamp including year, month, day, hour and minute [rid].
- TB=sites;  includes an identification of park and masts [site_id].
+ TB=speeds, includes all speed values rom the masts and the wind tubine nacelle wind speed (wsn). 
-TB=temps, includes all measured temperatures from the mast and the water.
- TB=wind-turbines (tid=turbine id); includes coordinates for each wind turbine

## Publications
1. Castellani, F., Astolfi, D., Terzi, L., Hansen, K. S., & Rodrigo, J. S. (2014). Analysing wind farm efficiency on complex terrains. Journal of Physics: Conference Series (Online), 524(1), [012142].
	
