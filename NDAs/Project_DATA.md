

# Introduction <a name="introduction"></a>
This document list a number of internal project databases hosted by DTU Wind Energy. The accessibility depend on the project managers permission.

IMPORTANT Access to these database are restricted to employees at DTU Wind Energy. 

//Kurt S. Hansen, DTU Wind Energy; [ORCID:0000-0001-7109-3855](http://orcid.org/0000-0001-7109-3855)

- Data DOI               : 'https://doi.org/10.11583/DTU.16458561'

# List of project databases

## Project 2lidar
This database  includes the 1.generation wake scanning (2011) performed by Stuttgart on the 42m/500kW Nordtank wind turbine prevously located on Risø Campus, Roskilde.

- Database.: 2Lidar, IP=130.226.56.150
- Period.: 2011 - 2012
- Contact person.:   Gunner Chr. Larsen; DTU Wind Energy; [ORCID:0000-0002-6482-8208](http://orcid.org/0000-0002-6482-8208)
- Database responsible.: Steen Arne Sørensen, DTU Wind Energy

## Project lidar
This database  includes the 2.generation wake scanning (2011) performed by Risø National Labs on the 80m/2MW NM80 wind turbine located on Tjæreborg enge. Furthermore  this database includes wind measuremetns from the met mast and SCADA data from all 8 turbine in Tjareborg Enge.

- Database.: Lidar, IP=130.226.56.150
- Period.: 2009 
- Contact person.:   Gunner Chr. Larsen; DTU Wind Energy; [ORCID:0000-0002-6482-8208](http://orcid.org/0000-0002-6482-8208)
- Database responsible.: Steen Arne Sørensen, DTU Wind Energy

## Project DECOWIND
This is a project database for DECOWIND, which was initiated in 2019.
The database includes inflow, wake and noise measurements for a 142m/4.2 wind turbine in Drantum and represent 2 campaign period in spring 2019 & spring 2020. Note: all measurement data are stored in NetCDF files.

- Database: Decowind; IP=130.226.56.150
- Project period: 2019 - 2020
- Contact person: Wen Zhong Shen, DTU Wind Energy; [ORCID:0000-0001-6233-2367](http://orcid.org/0000-0001-6233-2367)   
- Database responsible: Steen Arne Sørensen, DTU Wind Energy

## Project Greenland (Fyrtårnet)
This is a project database for the wind measurements measured as part of the ARTEK project, which was initiated in 20xx.
The database includes 10-minute statistics of wind speed, wind direction and temperature from 8x10m NRG Systems mast and some measurements from other systems, all located in Greenland. 
Measurement locations: D:\analysis\gold 
1. [List of measurement locations](./Project_data/Sites_Greenland.csv)

- Database: greenland, IP=130.226.56.150
- Project period: 1994 - 2010 
- Contact person: Martin O.L Hansen, DTU Wind Energy; [ORCID:0000-0002-2028-7139](http://orcid.org/0000-0002-2028-7139)   
- Database responsible: Steen Arne Sørensen, DTU Wind Energy

## Project Middelgrunden
This project database includes SCADA data from the Middelgrund 20 wind turbines for a period of 2 years used for wake analysis and teaching.

- Database: middelgrunden, IP=130.226.56.150
- Project period: 2000 - 2001
- Contact person:  Rebecca Barthelmie, Cornell University, Ithaca, US; [ORCID:0000-0003-0403-6046](http://orcid.org/0000-0003-0403-6046)  
- Database responsible: Steen Arne Sørensen, DTU Wind Energy

## Project Farmopt
This project database has been used for a research project between China & DTU Wind Energy on flow in complex terain. The database contents includes scada data from a wind farm and wind speed mesurements from 2 mast located in the corner of the wind farm.    

- Database: shanxi, IP=130.226.56.150
- Project period: 2015
- Contact person:  Wen Zhong Shen, DTU Wind Energy; [ORCID:0000-0001-6233-2367](http://orcid.org/0000-0001-6233-2367)     
- Database responsible: Steen Arne Sørensen, DTU Wind Energy

# Teaching databases
## Project Nordtank
This is the former database used for education 
## Project CCA-V52
The project database has been created as part of the CCA-V52 and used for teaching in two DTU Courses: 
1. 46400 Wind turbine measurement technique (campus) 
2. 46W37 Measurement technique in Wind Energy (online).

The database includes mast and structural load measurements (time series & statistics). Furthremore derived values for wind shear, stratification and calibrated spinner values are included.

- Database: V52, IP=130.226.56.150
- Project period: 2017-2018
- Contact person:  Mike Courtney, DTU Wind energy; [ORCID:0000-0001-6286-5235](http://orcid.org/0000-0001-6286-5235)     
- Database responsible: Steen Arne Sørensen, DTU Wind Energy

## Tutorial
This a small database used for a SQL tutorial in accessing databases
Associated to this database are 3 tutorial videos.

- Database: Tutorial, IP=130.226.56.150
- Project period: 2017-
- Contact person:  Mike Courtney, DTU Wind energy; [ORCID:0000-0001-6286-5235](http://orcid.org/0000-0001-6286-5235)     
- Database responsible: Steen Arne Sørensen, DTU Wind Energy 

1. [Wind turbine measurement technique—an open laboratory for educational purposes](https://onlinelibrary.wiley.com/doi/abs/10.1002/we.248)

## C4600 & (Group01 - 11) - Campus course
This project database is hosting input for class execises and assignments. The 11 Group databases are used for group hand-ins of results.

- Database: C4600, IP=130.226.56.150
- Project period: 2017-
- Contact person:  Mike Courtney, DTU Wind energy; [ORCID:0000-0001-6286-5235](http://orcid.org/0000-0001-6286-5235)     
- Database responsible: Steen Arne Sørensen, DTU Wind Energy 

## C46W47 - Online course
This project database is hosting input for class execises and assignments. 

- Database: C4600, IP=130.226.56.150
- Project period: 2017-
- Contact person:  Mike Courtney, DTU Wind energy; [ORCID:0000-0001-6286-5235](http://orcid.org/0000-0001-6286-5235)     
- Database responsible: Steen Arne Sørensen, DTU Wind Energy