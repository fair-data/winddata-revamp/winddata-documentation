

# Description of the Lillgrund offshore wind farm dataset <a name="dummy"></a>
## Restriction to Lillgrund Wind Farm dataset
Access to the SCADA data from Lillgrund offshore wind farm is covered by a NDA with former owner Vattenfall AB.
- Contact person: Gunner Chr. Larsen; DTU Wind Energy; [ORCID:0000-0002-6482-8208](http://orcid.org/0000-0002-6482-8208) 
- Data administrtion: Steen-Arne Sørensen, DTU Wind Energy 

//Kurt S. Hansen, DTU Wind Energy; [ORCID:0000-0001-7109-3855](http://orcid.org/0000-0001-7109-3855)
## Lillgrund Wind farm
Lillgrund Wind Farm is located about 10 km off the coast of southern Sweden, just south of the Öresund Bridge, where average wind speeds are 8 to 10 metres per second (26 to 33 ft/s). With 48 wind turbines (Siemens SWT-2.3-93) and a capacity of 110 megawatts (MW).

![wind farm layout](./LG/LG_layout.png)
**Figure 1:** LG wind farm layout including wake masts

![wind farm layout](./LG/Lillgrund.jpg)
**Photo 1:** LG wind farm layout by courtesy Kurt S. Hansen
1. [List of wind turbine and mast positions](./LG/sites.csv)

## Lillgrund wind turbines
- Siemens: 2.3MW, 
- Diameter =92.3m, 
- hub height =65m, 
- Control: Active pitch & variable speed,
- Data period: 2008-2013

## Lillgrund mast
- Mast 1: Mast with wind speed, wind direction & temperatures m,recordings, h=65m (operational in the period 2003-2013); Please notice: the qyality of met data is reduced after 2010 due to lack system maintenance.
- Mast 2: Drogden fyr, used for classification of stability.

## Contents of database
The database consists of 10 - minute SCADA values from the wind  turbines and 10-minute statistics from the met mast. 
Signal from turbine includes 
- Active power, 
- pitch, 
- rotor speed, 
- yaw position
- nacelle wind speed
- Period 2008 - 2013

## Data quality
All the SCADA values have been quality controlled and the quality parameter (quality or qa) have been updated with reference to the definition in TB=modes.

## Access to the MySQL database
- Database server: 	130.226.56.150
1. Dabatase name: 	Lillgrund
2. Database name:    LG2 (extended version, with structural loading statistics representing several wind turbine)
- Access tools:	SQL eq. HeidiSQL or Matlab

## Database structure
![wind farm layout](./LG/offshore_db.png)

**Figure 2:** LG wind farm layout including wake masts
### Basic tables:	
- Runs:	Time stamps; id, runname, yr, mo,..  
- Channels: 	Definition list of available signals: numbers, names & location.
+ Speeds:	Wind speed; mean, max, min, stdev & ti values
    - channel=91, derived inflow (wind speed)    
+ Dirs:	Wind direction; mean, max, min, stdev.
    - channel=92, derived inflow (wind direction)
- Powers:	Power values; mean, max, min, stdev.
+ Operational params (mean & stdev):	
    - pitch, 
    - rotor speed 
    - yaw position; 
- Temps: temperatures; Mean & stdev
- Adds: others meteorological params
- Amok: Derived stratification, based on Drogden recordings

## Publications
1. [Assessment of the Lillgrund Windfarm](./LG/perfomance.pdf)	
1. [Meteorological Conditions at Lillgrund](./LG/Assessment_of_wind.pdf)	
	
