# Description of Navarra onshore wind farm dataset
## Navarra NDA 
Access to the SCADA data from Navarra onshore wind farm are covered by a NDA with the Cener, ES.
- Contact person: Javier Sanz Rodrigo, CENER, ES  
- Data administrtion: Steen-Arne Sørensen, DTU Wind Energy 

//Kurt S. Hansen, DTU Wind Energy; [ORCID:0000-0001-7109-3855](http://orcid.org/0000-0001-7109-3855)

## SG Wind farm

The Navarra wind farm, consists of 43 small turbines located on public land, is located on four ridges at an altitude of between 700 and 1,100 metres above sea level with a Capacity of 39.1 MW. The distance between two machines in the same row is 1.5 D.
The distance between the first three rows is about 11 D, whereas the fourth and fifth rows are further apart, at distances 15 and 22 D from the third row, respectively. [The WF location is confidential, but close Navarra, Spain]

![wind farm layout](./navarra/layout.png)
**Figure 1:** The Navarra wind farm layout including wake masts
1. [List of wind turbine and mast positions(relative to mast #1)](./navarra/sites.csv)
## Navarra wind turbines
- 43 x Ecotechnia, h=45 - 55m 

## Navarra masts
- M1 & M2 single masts with each with a height of 45m and NRG Systems loggers and instrumentation for measuring:
- wind speed, 
- wind direction 
- temperatures


## Contents of database
The database consists of 10 - minute SCADA values from the wind  turbines. 
Signal from each turbine includes 
- Active power, 
- nacelle wind speed
- yaw position

## Data quality
All the SCADA values have been quality controlled and the quality parameter (quality or qa) have been updated with reference to the definition in TB=modes.

## Access to the MySQL database:
- Database server: 	130.226.56.150
- Dabatase name: 	Navarra
- Access tools	SQL eq. HeidiSQL or Matlab

## Database structure
![wind farm layout](./navarra/db.png)

**Figure 2:** Structure of DB=navarra. 

## Basic tables:	
+ TB=channels; defines each signal including heigh and mounting and table location of the variables [channel_id], where each signal is characterized with name, unit and type.
- TB=dirs. includes all directions from sonics and vanes; optional values is calibrated wind turbine yaw position.
- TB=modes, operational modes with reference to the quality parameter of the power signal.
- TB=powers, includes all 10-minute averaged power values where quality=1 indicates the power has been accepted.Please note records with quality=0 have not been validated eg. in 2010-2011.  
- TB=runs, defines each 10-minute time stamp including year, month, day, hour and minute [rid].
- TB=sites;  includes an identification of park and masts [site_id].
+ TB=speeds, includes all speed values rom the masts and the wind tubine nacelle wind speed (wsn). 
-TB=temps, includes all measured temperatures from the mast and the water.

## Publications
1. Modeling wake effects in large wind farms in complex terrain: the problem, the methods and the issues by E. S. Politis  J. Prospathopoulos  D. Cabezon  K. S. Hansen  P. K. Chaviaropoulos  R. J. Barthelmie First published: 08 August 2011 
https://doi.org/10.1002/we.481
	
