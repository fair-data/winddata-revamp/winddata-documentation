
# Description of Robres onshore wind farm dataset
## Robres NDA 
Access to the SCADA data from Robres wind farm in complex terrain are covered by a NDA with the Cener, ES.
- Contact person: Daniel Cabazon, Cener, Spain  
- Data administrtion: Steen-Arne Sørensen, DTU Wind Energy 

//Kurt S. Hansen, DTU Wind Energy; [ORCID:0000-0001-7109-3855](http://orcid.org/0000-0001-7109-3855)

## SG Wind farm
The Robres wind farm, consists of 13 large Vestas V90/1.8MW turbines located on public land, located on a ridge toward W from Robres.

![wind farm layout](./robres/area.png)
**Figure 1:** Location of the Robres wind farm outside the citi Robres. 

![wind farm layout](./robres/layout.png)
**Figure 2:** The Robres wind farm layout including wake masts
1. [List of wind turbine and mast positions(relative to mast #1)](./robres/sites.csv)
## Navarra wind turbines
- 13 x Ecotechnia, h=80m

## Navarra masts
- M01 single masts with each with a height of 45m and NRG Systems loggers and instrumentation for measuring:
- wind speed, 
- wind direction 
- temperatures


## Contents of database
The database consists of 10 - minute SCADA values from the wind  turbines. 
Signal from each turbine includes 
- Active power, 
- nacelle wind speed
- yaw position

## Data quality
All the SCADA values have been quality controlled and the quality parameter (quality or qa) have been updated with reference to the definition in TB=modes.
- Note: Calibration of wind direction signals is lacking.

## Access to the MySQL database:
- Database server: 	130.226.56.150
- Dabatase name: 	Navarra
- Access tools	SQL eq. HeidiSQL or Matlab

## Database structure
![wind farm database structure](./robres/db.png)

**Figure 2:** Structure of DB=robres. 

## Basic tables:	
+ TB=channels; defines each signal including heigh and mounting and table location of the variables [channel_id], where each signal is characterized with name, unit and type.
- TB=dirs. includes all directions from sonics and vanes; optional values is calibrated wind turbine yaw position.
- TB=modes, operational modes with reference to the quality parameter of the power signal.
- TB=powers, includes all 10-minute averaged power values where quality=1 indicates the power has been accepted.Please note records with quality=0 have not been validated eg. in 2010-2011.  
- TB=runs, defines each 10-minute time stamp including year, month, day, hour and minute [rid].
- TB=sites;  includes an identification of park and masts [site_id].
+ TB=speeds, includes all speed values rom the masts and the wind tubine nacelle wind speed (wsn). 
-TB=temps, includes all measured temperatures from the mast and the water.

## Publications
NA
	
