
# Description of the Wind Resources at Læsø Syd 
## Data access
Access to the resource data from Læsø Syd M1 mast is covered by a NDA with the data owner: Energinet.dk 
- Contact person: Charlotte B. Hasager; [ORCID:0000-0002-2124-5651](http://orcid.org/0000-0002-2124-5651)  
- Data administrtion: Steen-Arne Sørensen DTU Wind Energy 

//Kurt S. Hansen, DTU Wind Energy; [ORCID:0000-0001-7109-3855](http://orcid.org/0000-0001-7109-3855)

## Horns Rev Mast M2
The meteorological mast at Laesoe Syd is located approximately 12 km south of Laesoe. Its position is 11°7,395'E 57°5,053'N (WGS84) and its UTM coordinates are 628.679E 6.328.763N (WGS84 Zone 32). Offshore mast located NW to the existing HR1 wind farm.

![location](./LS_m1/LSM1_location.png)
**Figure 1:** Location of the Laesoe mast.

- M1 : Mast with wind speed, wind direction & temperatures, h=62m used for resource measurement diring the period 1999 - 2003.  

![HR M2 layout](./LS_m1/LSM1_mast.png)
**Figure 1:** Layout of the Laesoe Syd mast M1.

## Contents of database
The database consists of 10 - minute statistics of 
- wind speed, 
- wind direction, 
- air temperatures and differences
- water temperature.
- Barometer pressure
- Relative humidity

## Data quality
All the values have been quality controlled and the quality parameter (quality or qa) have been updated with reference to the definition in TB=modes.

## Access to the MySQL database
- Database server: 	130.226.56.150
- Database name: 	hr_m2 - (public accesible database)
Access tools	SQL eq. HeidiSQL or Matlab

## Database structure
![wind farm layout](./LS_m1/siite.png)
**Figure 2:** Structure of the LS-M1 database

The database structure includes 12 different tables where the most important tables have been listed below

+ TB=channels; defines each signal including heigh and mounting and table location of the variables [channel_id], where each signal is characterized with name, unit and type.
	- [List of mast channels](./LS_m1/channels.csv)	
+ TB=dirs. includes all directions from sonics and vanes; 
- TB=modes, operational modes with reference to the quality parameter of the power signal.
- TB=quality = modes.
- TB=runs, defines each 10-minute time stamp including year, month, day, hour and minute [rid].
+ TB=speeds, includes all speed values from the masts; 
+ TB=temps, includes all temperature values from the masts. 
+ TB=adds, includes all other meteorological values from the masts.
 

## Publications
1. Wind Resources at Laesoe Syd, Eltra PSO-2000 Proj. nr. EG-05 3248
Programme for measuring wind, wave and current at Laesoe Syd.[(3MB)](./LS_m1/LS_ELTRA_PSO_2002_Techwise.pdf)
