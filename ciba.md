## Background Information
- CLASSIFICATION: flat(flat landscape), pastoral(open fields and meadows)
- COUNTRY : Spain
- ALTITUDE : 840 [m]
- POSITION : [41˚ 49' 31.77'' N 5˚ 42'50.84'' E](https://geohack.toolforge.org/geohack.php?pagename=ciba&params=41_49_31.77_N_5_42_50.84_W_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This dataset consists of raw time series and 10-minute statistics of wind speed and wind direction measurements from a 100m tall met mast NW to Valladolid, Spain. The period includes more than 400 hours of turbulence profile measurements during the late summer 1988.
## Maps

![A map of Spain](./ts_wind/ciba/map_01.gif)
**Map 1:** Location of nearby Villanubla airport
![A map of Spain](./ts_wind/ciba/map_02.gif)
**Map 2:** Location of nearby Villanubla airport and topography
![A map of Spain](./ts_wind/ciba/msmap.gif)
**Map 3:**Location of CIBA and Villanubla near Valladolid - in Spain

## Drawinds
![](./ts_wind/ciba/draw_01.gif) **Drawing 01:**Monte Torozos plateau level
![](./ts_wind/ciba/layout.gif) **Drawing 02:**Instrumentation at CIBA
![](./ts_wind/ciba/r3_drawing_lrg.jpg) **Drawing 03:**Gill Solent anemometer

## Photos
![](./ts_wind/ciba/photo_01.jpg) **Photo 01:** Site photo with mast and boom indications
![](./ts_wind/ciba/r3r3a_lrg.jpg) **Photo 02:** Gill Ultrasonic anemometer.



## Reports
1. [Description of the setup](./ts_wind/ciba/ciba.pdf)
## Mast (relative positions with reference to the reference POSITION) 
1. 100 [m] (0,0,0)


## Project description
In the period 10-28 September 1998, the Stable Atmospheric Boundary Layer Experiment in Spain (hencefortth) SABLES 98) took place at the Research Centre for the Lower Atmospere (CIBA) in the northern Spanish plateau. This Centre belongs to theUniversity of Valladolid and the Spanish Meteorological Institute (INM), and the purpose of the experiment was to study the characteristics of the Stable Atmospere Boundry Layer in mid-latitudes. The surrounding terrain is fairly flat and homogeneous and the measurement site is located in the centre of an 800 km**2 plateau (Monte Torozos), which is 840 m above sea level and surrounded by fairly level grass plains with a surface roughness parameter, Zo, of 1.1 cm (Sam Jóse et. al. 1985). The plateau is raised about 50 m above en extensive region of homogeneous level terrain. Only the meteorological measurements (1 & 3D wind speed, direction, temperatures and atmosperic pressure) from these measurements has been made available through the wind database.

## Measurement system
A 100 m tower with booms oriented such that optimum measurements would be taken for easterly winds. The tower has been equipped with instruments provided and calibrated by Risø National Laboratories which was operated by Ph. D., M. Sc. Hans E. Jørgen, Department Wind Energy and Atmosphere Physics, Riso National Laboratory, 4000 Roskilde, Denmark.

1. [List of mast signals (time series of wind speed)](./ts_wind/ciba/ts_mast_signals.csv)
2. [List of additional signals](./ts_wind/ciba/add_signals.csv) Note: The statistics for these signals are only included in the header of the packed ASCII file. 


## Nominal values

![](./ts_wind/ciba/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/ciba/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/ciba/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wind/ciba/w_dist.gif)
**Figure n4:** Nominal wind speed distribution.

![A map of Denmark](./ts_wind/ciba/ti_dist.gif)
**Figure n5:** Nominal turbulence distribution.

![A map of Denmark](./ts_wind/ciba/dir_dist.gif)
**Figure n6:** Nominal wind direction distribution. 

## ACKNOWLEDMENTS	:	Hans E. Jørgenssen,DTU Wind Energy
- INSTITUTION       :	Risoe National Laboratories
- ADDRESS 	        :   Post box 49, DK4000 Roskilde, Denmark
- TEL/FAX 	        :   +45 4677 5034 / +45 4677 5970
- CONTACTS 	        :   Hans E. Jørgensen
- LINKS 	        :   http://www.risoe.dk/vea
- COLLABS 	        :   Instituto Nacional de Meteorologia, Spain
- FUND AGENTS       :   CICYT, project CLI97-0343, CLI98-1479E and CLI95-1794
- PERIOD 	        :   1988
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14380826'

## PUBLICATIONS	    : 
1. [Cuxart, J., Yagüe, C., Morales, G. et al. Stable Atmospheric Boundary-Layer Experiment in Spain (SABLES 98): A Report. Boundary-Layer Meteorology 96, 337–370 (2000).](https://doi.org/10.1023/A:1002609509707)

## Public data
1. Run statistics; ciba.nc (NetCDF) 
2. Raw time series = ciba.zip (zip) each with a duration of 1800 sec and sampled with 5 and 20 Hz. 

