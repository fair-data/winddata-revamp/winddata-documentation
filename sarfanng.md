## Background Information
- CLASSIFICATION: hill(rolling hills), coastal(water and land)
- COUNTRY : Greenland
- ALTITUDE : 100 [m]
- POSITION : [66˚ 53' 42.92'' N 52˚ 51' 58.62'' E](https://geohack.toolforge.org/geohack.php?pagename=sarfanng&params=66_53_42.96_N_52_51_58.62_W_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)


## Short summary
This resource dataset consists of 10 minute wind speed and wind direction measurements from a small met mast next to the settlement Sarfanguit,Greenland. The period includes 1 year of measurements, which starts in 2003.

## Map
![A map of Denmark](./Resource/sarfanng/msmap.jpg)
**Figure 1:** A map of the central Greenland.

![A map of Denmark](./Resource/sarfanng/map_01.jpg)
**Figure 2:** A map of the fjord next to Sarfanguit.

![A map of Denmark](./Resource/sarfanng/photo_01.jpg)
**Figure 3:** Airal picture of the mast location, next to the helipad.

## Drawings
![](./Resource/sarfanng/layout.gif) **Drawing 01:** Mast layout.

## Graphs
![](./Resource/sarfanng/aug03.jpg) **Graph 01:** Distribution measured in Aug 2003

## Reports
1. [Short setup description](./Resource/sarfanng/sarfannguaq.pdf)

## Mast (relative positions with reference to the reference POSITION) 
1. 10 [m] (0,0,0) (mast 1.)

## Project description
 In July 2003 a meteorological mast was erected near a small village, Sarsannguit, Sisimiut in Greenland The erection and operation has been initiated as a students work in danish arctic center at DTU. The main purpose is to measure the wind resource in a small remote village and determine the benefits of a combined wind diesel power system. The location of the measurement system is south west of the village, near a heli-port. The monthly maintenace, operation and data retrival are performed by the local diesel power plant operator. The Met.mast has been erected by Henrik Sogård Iversen & Jacob Skovgård Kristensen during the summer camp in July 2003.

## Measurement system
The meteorological data in terms of wind speed and wind direction at 10 m height, are recorded as 10 minute statistics with a standard NRG-system datalogger. The measurement campaign started in July 2003 and continues until summer 2004 and the database will be updated once every month.

1. [List of mast signals](./Resource/sarfanng/ts_mast_signals.csv)

## Nominal values

![](./Resource/sarfanng/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./Resource/sarfanng/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./Resource/sarfanng/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./Resource/sarfanng/w_dist.gif)
**Figure n4:** Nominal wind speed distribution.

![A map of Denmark](./Resource/sarfanng/ti_dist.gif)
**Figure n5:** Nominal turbulence distribution.

![A map of Denmark](./Resource/sarfanng/dir_dist.gif)
**Figure n6:** Nominal wind direction distribution. 

## ACKNOWLEDMENTS		
- Acknowledgemt     : Jacob, Henrik & Hans Hindrichsen, Sisimiut
- E-mail: 
- INSTITUTION       : Technical University of Denmark
- ADDRESS 	        : Fluid Mechanics Section, MEK, B403-DTU, 2800 Lyngby
- TEL/FAX 	        : +45 45 25 43 18 / +45 45 93 06 63
- CONTACTS 	        : Kurt S. Hansen / Martin O.L. Hansen
- LINKS 	        : www.afm.dtu.dk / www.mek.dtu.dk
- COLLABS 	        : Danish arctic centre
- FUND AGENTS       : Internal
- PERIOD 	        : 2003-08-01 00:00:00 - 2003-12-31 00:00:00
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.xxxxx/DTU.14153231'

## Public data
1. Resource data  (NetCDF) 