## Background Information
- CLASSIFICATION: hill(rolling hills), coastal(water and land)
- COUNTRY : Norway
- ALTITUDE : 15 [m]
- POSITION : [63˚ 39' 58'' N 81˚ 20'33'' E](https://geohack.toolforge.org/geohack.php?pagename=ski&params=63_39_58_N_8_20_33_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This dataset consists of raw time series and run statistics of wind speed and wind direction measurements from three tall met masts (45-101m) near Skipheya, on Frøya, Norway. The period includes more than 3 years of measurements from 1992.

## Map
![A map of Denmark](./ts_wind/ski/msmap.gif)
**Map 1:** Western part of Mid-Norway.
![A map of Denmark](./ts_wind/ski/layout.gif)
**Map 2:** Skipheya site layout.

## Photos
![](./ts_wind/ski/photo_1.gif) **Photo 01:** The landscape to the west of the station as viewed from a 100m mast

![](./ts_wind/ski/photo_2.gif) **Photo 02:** The three masts and instrumentation house seen from west

![](./ts_wind/ski/skiph.gif) **Photo 03:** The three masts seen from east - Photo: Gry Ostvik

## Drawings
![](./ts_wind/ski/map_1.gif) **Drawing 01:** The three masts with directions and distances.

![](./ts_wind/ski/map_2.gif) **Drawing 02:** Sketch of Mast2 with booms and sensors
![](./ts_wind/ski/map_3.gif) **Drawing 03:** Sketch of Mast3 with booms and sensors

## Reports

## Mast (relative positions with reference to the reference POSITION) 
1. 100 [m] (62,44,4)
2. 100 [m] (-3,89,5)
3.  45 [m] (3,-115,7)


## Project description
The station was build as a part of the Norwegian Wind Energy Programme in 1980. The purpose was to study the wind structure in details. Particularly, a database for high wind speed condition was desired. Data were originally intended for wind energy production; the dimension af the masts corresponds to a large WECS. The station should also serve as a reference station for other measurement stations in the region. Together with a fourth mast 4 km further west (Sletringen) the station has provided data for calculation of dynamic wind loads on off-shore constructions.

## Measurement system
The measurment system consists of 3 meteorological masts (100, 100 and 45 m) placed in a triangle 80-180 metres apart. Ten-minute average of wind speed and direction have been recorded since 1982. From 1988, the logging frequency has been 0.85 Hz. 40-60 channels have been recorded continuously. In a 45 m mast at Sletringen, data has been recorded for some periods since 1988.

1. [List of mast signals (time series of wind speed)](./ts_wind/ski/ts_mast_signals.csv)

## Nominal values

![](./ts_wind/ski/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/ski/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/ski/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wind/ski/w_dist.gif)
**Figure n4:** Nominal wind speed distribution.

![A map of Denmark](./ts_wind/ski/ti_dist.gif)
**Figure n5:** Nominal turbulence distribution.

![A map of Denmark](./ts_wind/ski/dir_dist.gif)
**Figure n6:** Nominal wind direction distribution. 

## ACKNOWLEDMENTS	:	Jørgen Løvseth, NTNU 
- INSTITUTION       :	Norwegian Univ. of Sci. and Techn., Dept. of Physics
- ADDRESS 	        :   NTNU, Dept. of Physics, Lade, N-7034 Trondheim, Norway
- TEL/FAX 	        :   +47 7359 1856 / +47 7359 1852
- CONTACTS 	        :   Jørgen Løvseth, NTNU
- LINKS 	        :   http://www.phys.ntnu.no/lade/forskning/miljo/miljofys.htm#Vind / 
- PERIOD 	        :   1992 - 1995
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14380766'
 

## PUBLICATIONS	    : 
NA

## Public data
1. Run statistics; ski.nc (NetCDF) 
2. Raw time series = ski.zip (zip) each with a duration of 3600 sec and sampled with 0.85 Hz. 

