## Background Information
- CLASSIFICATION: flat(flat landscape), coastal(water and land)
- COUNTRY : Belgium
- ALTITUDE : 2.5 [m]
- POSITION : [51˚ 21' 28'' N 3˚ 13'23'' E](https://geohack.toolforge.org/geohack.php?pagename=zeeb&params=51_21_28_N_3_13_23_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This dataset includes time series of wind data from a 40m mast located at Zeebrugge Harbor, Belgium. The period includes more than 100 hours of turbulence measurements, which starts in 1998. Furthermore, 2 Hz time series representing 2 cup anemometers and 1 vane. 

## Map
![A map of Denmark](./ts_wind/zeeb/map_1.gif)
**Figure 1:** A map of Belgium
![A map of Denmark](./ts_wind/zeeb/map_3.gif)
**Figure 2:** Layout of the harbour in Zeebrugge.

## Drawings

![](./ts_wind/zeeb/layout.gif) **Drawing 1:** Mast layout.
## Photos
![](./ts_wind/zeeb/picture1.png) **Photo 1:** Turbowinds T400-34 turbine at Zeebrugge.
![](./ts_wind/zeeb/picture2.png) **Photo 2:** Turbowinds T600-48 turbine at Zeebrugge
![](./ts_wind/zeeb/zeebrugge.gif) **Photo 3:** Turbines in Zeebrugge harbour.

## Masts (relative positions with reference to the reference POSITION) 

1. 64 [m] (0,0,2.5)

## WIND TURBINES
1. Windmaster 200kW 200 [kW] (115,119,2.5)
2. Windmaster 200kW 200 [kW] (59,227,2.5)
3. Windmaster 200kW 200 [kW] (-24,317,2.5)
4. Windmaster 200kW 200 [kW] (-121,390,2.5)
5. Windmaster 200kW 200 [kW] (-234,438,2.5)
6. Windmaster 200kW 200 [kW] (-351,472,2.5)
7. Windmaster 200kW 200 [kW] (-472,489,2.5)
8. Windmaster 200kW 200 [kW] (-592,511,2.5)
9. Windmaster 200kW 200 [kW] (-709,546,2.5)
10. Windmaster 200kW 200 [kW] (-829,568,2.5)
11. Windmaster 200kW 200 [kW] (-951,576,2.5)
12. Turbowinds T400-34 400 [kW] (-1,605,2.5)
13. Windmaster 200kW 200 [kW] (-708,-662,2.5)
14. Windmaster 200kW 200 [kW] (-729,-564,2.5)
15. Windmaster 200kW 200 [kW] (-749,-466,2.5)
16. Windmaster 200kW 200 [kW] (-768,-368,2.5)
17. Windmaster 200kW 200 [kW] (-730,-270,2.5)
18. Windmaster 200kW 200 [kW] (-604,-225,2.5)
19. Windmaster 200kW 200 [kW] (-483,-159,2.5)
20. Windmaster 200kW 200 [kW] (-156,-33,2.5)
21. Windmaster 200kW 200 [kW] (-59,-12,2.5)
22. Windmaster 200kW 200 [kW] (39,8,2.5)
23. Turbowinds T600-48 600 [kW] (137,29,2.5)

## Project description
On the sea-port of Zeebrugge in Belgium, 22 turbines are installed. 20 turbines of 200 kW (1986), 1x 175 kW, 1x 400 kW (1997) and 1x 600 kW (1998) turbine. The University of Brussels does measurements on the 600 kW turbine to examine the dynamic behavior of the blades, nacelle, and mast.


## Measurement system
The University of Brussels installed a 64 m wind-measuring mast. On top and on the intermediate height of 40 m, wind-speed is measured. Data is transferred to a central computer. The windspeeds on 40 m and 64 m together with the winddirection are interpreted with LabView . Data recording frequency is 2 Hz and the duration is 3600 sec.


1. [List of mast signals](./ts_wind/zeeb/ts_mast_signals.csv)

## Nominal values

![](./ts_wind/zeeb/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/zeeb/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/zeeb/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wind/zeeb/w_dist.gif)
**Figure n4:** Nominal wind speed distribution.

![A map of Denmark](./ts_wind/zeeb/ti_dist.gif)
**Figure n5:** Nominal turbulence distribution.

![A map of Denmark](./ts_wind/zeeb/dir_dist.gif)
**Figure n6:** Nominal wind direction distribution. 
## ACKNOWLEDMENTS		
- INSTITUTION       :	University of Brussels, dept. Fluid Dynamics
- ADDRESS 	        :   Pleinlaan 2, 1050 Brussel, Belgium
- TEL/FAX 	        :   +32 2 629 23 99 / +32 2 629 28 80
- CONTACTS 	        :   Luc Dewilde
- LINKS 	        :   http://stro9.vub.ac.be/wind/
- COLLABS 	        :	Turbowinds N.V.
- PERIOD 	        :   1998-08-11 - 1999-01-13
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.18718898'


## PUBLICATIONS	    : 
NA

## Public data
1. Run statistics; zeeb_all.nc (NetCDF) 
2. Run statistics; zeeb_concurrent.nc (NetCDF)

Raw time series (ascii) each with a duration of 3600 sec, sampled with 2 Hz:
3. zeeb.zip (zip)


