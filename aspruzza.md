## Background Information
- CLASSIFICATION: mountain(sharp contours - separation expected), pastoral(open fields and meadows)
- COUNTRY : Italy
- ALTITUDE : 1178 [m]
- POSITION : [41˚ 35' 47'' N 14˚ 24'0'' E](https://geohack.toolforge.org/geohack.php?pagename=spruzza&params=41_35_47_N_14_24_0_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This dataset consists of raw time series and statistics of wind speed and wind direction measurements from one met mast in the italien wind turbine test station in the spruzza mountains. The period includes less than 1 year of turbulence measurements starts in 1992.

## Map

![A map of Denmark](./ts_wind/spruzza/msmap.gif)
**Map 1:** Geographical location of spruzza, Italy

![A map of Denmark](./ts_wind/spruzza/map_1.gif)
**Map 2:** Orography map and wind turbine location

## Drawings
![](./ts_wind/spruzza/map_2.gif) **Drawing 01:** General lay-out of wind turbines and wind masts

## Graphs
![](./ts_wind/spruzza/graph_1.gif) **Graph 01:** Wind speed distribution, h = 15m, period = 1989 - 1995.
![](./ts_wind/spruzza/graph_2.gif) **Graph 02:** Wind rose, h = 15m, period = 1989 - 1995
![](./ts_wind/spruzza/graph_3.gif) **Graph 03:** 10-min max wind speed distribution, h = 15m, period = 1989 - 1995


## Reports
1. [Young Wind Monitor, speed and direction (pdf; 50 kB)](./ts_wind/spruzza/ws_05101.pdf)
2. [Maximum Wind Generator specifications (pdf; 240 kB)](./ts_wind/spruzza/Maximum_wind.pdf)
3. [NRG Wind Vane specifcations (pdf; 240 kB)](./ts_wind/spruzza/NRG_Vane.pdf)

## Mast (relative positions with reference to the reference POSITION) 
1. 40 [m] (0,0,0) (reference)

## Wind turbines
1. Riva Calzoni M30 250 [kW] (126,494,-4)
2. Vestas WD34 400 [kW] (250,370,-12)
3. WEG MS-3 300 [kW] (272,218,-3)
4. WEST Medit-I 320 [kW] (290,74,6)
5. WEG MS-3 300 [kW] (-150,378,-8)
6. WEST Medit-I 320 [kW] (-60,264,-9)
7. Riva Calzoni M30 250 [kW] (-14,106,-1)
8. Vestas WD34 400 [kW] (64,-70,-3)


## Project description
The Acqua Spruzza wind turbine test site has been built by ENEL S.p.A. within the framework of a programme aimed at evaluating the technology of commercial medium-sized machines operating in complex terrain and very hostile climate, with special regard to availability, energy output, lifetime (through the monitoring of loads), operating and maintenance costs. The objective is to assess the viability and the economic attractiveness of wind farms in hostile terrain and to understand the risks associated with the exploitation of these kind of sites. To this end, a suitable research programme has been outlined, comprising regular performance and load monitoring of the wind turbines and wind monitoring as well, through acquisition of both statistical and campaign series of data. The campaign wind data are available for the present Project "Database on Wind Characteristics"

## Measurement system
A number of data acquisition systems are presently installed at the Acqua Spruzza test site, namely the Measurement Control and Monitoring system (MCM), the Scientific Measurement Systems (SMS) and the SQUIRREL data loggers. The MCM system allows the general monitoring of wind turbines and the wind measurements from conventional anemometers installed on two wind masts, namely M1 and M2, through the recording of 10-min main statistics. The SMSs are specifically dedicated to the performance and load monitoring of three wind turbines, through the acquisition of extensive campaign data (20 - 60 Hz). The SQUIRREL data loggers are dedicated to summary wind data acquisition, some of which from sensors specially designed for operation in cold climate, and to the collection of wind data time series at 1 Hz frequency from mast M1. The wind campaigns recorded by a SQUIRREL data logger are


1. [List of mast signals (time series of wind speed)](./ts_wind/spruzza/ts_mast_signals.csv)


## Nominal values

![](./ts_wind/spruzza/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/spruzza/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/spruzza/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wind/spruzza/w_dist.gif)
**Figure n4:** Nominal wind speed distribution.

![A map of Denmark](./ts_wind/spruzza/ti_dist.gif)
**Figure n5:** Nominal turbulence distribution.

![A map of Denmark](./ts_wind/spruzza/dir_dist.gif)
**Figure n6:** Nominal wind direction distribution. 

## ACKNOWLEDGEMENTS	:	Massimo Cavaliere	
- INSTITUTION       :	ENEL Research, Renewable Energy Unit
- ADDRESS 	        :   Via Volta 1, 20093 Cologno Monzese
- TEL/FAX 	        :   +39 2 72245242 / +39 2 72245253
- CONTACTS 	        :   Massimo Cavaliere
- LINKS 	        :   http://www.spruzza.org/
- FUND AGENTS       :   ENEL / EU DG XVII
- PERIOD 	        :   1989 - 1999
- Naming_authority  :   'DTU Data'
- DOI               :   'https://doi.org/10.11583/DTU.14308034'

## PUBLICATIONS	    : 
1. NA

## Public data
1. Run statistics; spruzza.nc (NetCDF) 
2. Raw time series; duration= 600-3600 sec; freq=1 Hz;
   file=spruzza.zip (zip)

