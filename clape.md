## Background Information
- CLASSIFICATION: hill(rolling hills), scrub(bushes and small trees)
- COUNTRY : UK
- ALTITUDE : 151 [m]
- POSITION : [43˚ 9' 0'' N 3˚ 4'0'' W](https://geohack.toolforge.org/geohack.php?pagename=clape&params=43_9_0_N_3_4_0_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This dataset time series and statistics wind speed and wind direction measurements from a mast on La Clape, Narbonne, France. The period includes more than 240 hours of measurements from 1992.

## Map
![A map of Denmark](./ts_wind/clape/msmap.gif)
**Figure 1:** A map of Denmark

## Photos
![](./ts_wind/clape/photo_1.gif) **Photo 01:** The Hill seeing from the plain up wind.

![](./ts_wind/clape/photo_2.gif) **Photo 02:** Landscape of the summit in the South East direction.

![](./ts_wind/clape/photo_3.gif) **Photo 03:** 3 x Propellers

![](./ts_wind/clape/photo_4.gif) **Photo 04:** 3d  sonic anemometer.


## Drawings
![](./ts_wind/clape/layout.gif) **Drawing 01:** site layout

## Reports
1. Climatological analysis of wind measurements on a hill near Narbonne, CSTB report EN-CLI 94.19
## Mast (relative positions with reference to the reference POSITION) 
MASTS
1. 40 [m] (0,0,0)

## Project description
Wind measurements are being recorded on a hill facing prevailing winds, near the Mediterrean sea at Narbonne. The hill is situated in the north face of La Clape mountains, facing Tramontana winds. The upwind plain is very flat over more then 5 km, its altitude is 4 m above sea level; the hill summit is a 150 m above sea level, which gives an altitude difference of 145m and an average upwind slope greater then 0.2. Along hill slopes the ground is covered with bushes, vineyards and small pines. The plain is typically an open country around the reference site with some buildings and hedges.
## Measurement system
NA

1. [List of mast signals](./ts_wind/clape/ts_mast_signals.csv)
2. [List of additional signals](./ts_wind/clape/add_signals.csv) Note: The statistics for these signals are only included in the header of the packed ASCII file.

## Nominal values

![](./ts_wind/clape/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/clape/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/clape/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wind/clape/w_dist.gif)
**Figure n4:** Nominal wind speed distribuion.

![A map of Denmark](./ts_wind/clape/ti_dist.gif)
**Figure n5:** Nominal turbulence distribuion.

![A map of Denmark](./ts_wind/clape/dir_dist.gif)
**Figure n6:** Nominal wind direction distribuion. 

## ACKNOWLEDMENTS		
- INSTITUTION       :	CSTB, France
- ADDRESS 	        :   11 rue Henri Picherit BP 82341 F 44323 Nantes, Cedex
- TEL/FAX 	        :   +33 2 40 372020 / +33 2 40 372060
- CONTACTS 	        :	Christian Sacre  
- FUND AGENTS       :   IEU, French Ministry of Equipment
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14494926'
  
## PUBLICATIONS	    : 
1. NA

## Public data (NetCDF format);
1) clape_all.nc (NetCDF)
2) clape_concurrent.nc (NetCDF)

Raw time series each with a duration of 600 sec, sampled with 2 and 8 Hz:
3) clape.zip


