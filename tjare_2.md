## Background Information
- CLASSIFICATION: flat(flat landscape), coastal(water and land)
- COUNTRY : Denmark
- ALTITUDE : 3 [m]
- POSITION : [55˚ 26' 56.24'' N 8˚ 35'.62'' E](https://geohack.toolforge.org/geohack.php?pagename=tjare_2&params=55_26_56.24_N_8_35_4.62_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)
 ## Short summary
This resource dataset consists of 10-minute wind speed and wind direction measurements from the met mast at Tjaeborg Enge, Jutland in Denmark next to a large NEG-Micon 1500 kW turbine. The period includes more than one year of measurements, which starts in 1998.

## Map
![A map of Denmark](./Resource/tjare_2/msmap.gif)
**Figure 1:** A map of Denmark

## Photos
![](./Resource/tjare_2/NM64.jpg) **Photo 01:** Picture of the NTK 1500 prototype wind turbine.
![](./Resource/tjare_2/panoramic.gif) **Photo 02:** Panoramic view from the top of NTK 1500 turbine.

## Drawings
![](./Resource/tjare_2/layout.gif) **Drawing 01:** Layout of 64 mast.

## Mast (relative positions with reference to the reference POSITION) 
MASTS
1. 64 [m] (0,0,0)

## Project description
The resource data was recorded on a 64 m mast located next the 1500 kW wind turbine as part of the turbine commisioning. 

## Measurement system
Standard Risoe logger

1. [List of mast signals](./Resource/tjare_2/mast_signals.csv)

## Nominal values

![](./Resource/tjare_2/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./Resource/tjare_2/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./Resource/tjare_2/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./Resource/tjare_2/w_dist.gif)
**Figure n4:** Nominal wind speed distribuion.


## ACKNOWLEDMENTS		
- INSTITUTION       :	NEG-Micon
- ADDRESS 	        :   Alsvej 21, DK-8900 Randers, Denmark
- TEL/FAX 	        :   +45 87105169 / +45 86105001
- FUND AGENTS       :   Internal
- PERIOD 	        :   1999-01-01 - 2001-12-17
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14307707'

## PUBLICATIONS	    : 
1. 

## Public data
1. Resource data: tjare_2.nc (NetCDF) 


