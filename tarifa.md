## Background Information
- CLASSIFICATION: mountain(sharp contours - separation expected), pastoral(open fields and meadows)
- COUNTRY : Spain
- ALTITUDE :154 [m]
- POSITION : [36˚ 2' 49'' N 5˚ 34'6.76'' E](https://geohack.toolforge.org/geohack.php?pagename=tarifa&params=36_2_49_N_5_34_6.76_W_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This dataset consists of raw time series and 10-minute statistics of wind speed and wind direction measurements from two 40m mast in the Tarifa area, Spain. The period includes more than 280 hours of turbulence measurements during spring 1997.
## Maps

![A map of Spain](./ts_wind/tarifa/msmap.gif)
**Map 1:** Location map for Andalucia Autonomy including Tarifa site.
![A map of Spain](./ts_wind/tarifa/map_2.gif)
**Map 2:** Site location for Tarifa zone.
![A map of Spain](./ts_wind/tarifa/map_3.gif)
**Map 3:** Digital elevation model for Tarifa zone.

## Photos
![](./ts_wind/tarifa/photo_1.gif) **Photo 01:** Surroundings of wind turbine.
![](./ts_wind/tarifa/photo_3.gif) **Photo 02:** Surroundings of the wind turbine towards W direcction (270 degrees from the North).
![](./ts_wind/tarifa/photo_4.gif) **Photo 03:** Surroundings of the wind turbine towards WNW direcction (300 degrees from the North).
![](./ts_wind/tarifa/photo_5.gif) **Photo 04:** Surroundings of the wind turbine towards WSW direcction (240 degrees from the North).
![](./ts_wind/tarifa/photo_62.gif) **Photo 05:** Surroundings of the wind turbine towards WSW direcction (240 degrees from the North).
![](./ts_wind/tarifa/photo_7.gif) **Photo 06:** Surroundings of the wind turbine towards ENE direcction (60 degrees from the North).
![](./ts_wind/tarifa/photo_8.gif) **Photo 07:** Surroundings of the wind turbine towards ESE direcction (120 degrees from the North).

## Graph
![](./ts_wind/tarifa/graph_2.gif) **Graph 01:**Netpower curve for Ecotecnia 44/600 wind turbine
![](./ts_wind/tarifa/graph_4.gif) **Graph 02:**Tophographical Slices at tarifa site (Radius = 500 m).

## Drawings
![](./ts_wind/tarifa/layout.gif) **Drawing 01:** Site layout
![](./ts_wind/tarifa/map_4.gif) **Drawing 02:** Wind Speed Field m/s at hub height (42 m). Surroundings of Ecotecnia 44/600 Wind Turbine. NW view.
![](./ts_wind/tarifa/map_5.gif) **Drawing 03:** Topography site with masts and wind turbine location.

## Reports
NA
## Mast (relative positions with reference to the reference POSITION) 
1. 40 [m] (33,73,0)
2. 40 [m] (-2,-102,0)

## WIND TURBINES
1. Ecotecnia 44/600 600 [kW] (0,0,0)
2. Ecotecnia 40/500 500 [kW] (-43,-170,-2)
3. Ecotecnia 24/200 200 [kW] (-128,-250,3)
4. Ecotecnia 20/150 150 [kW] (-182,-256,11)
5. Nordtank VS 300 300 [kW] (625,227,71)
6. Nordtank VS 300 300 [kW] (653,268,81)
7. Nordtank VS 300 300 [kW] (662,314,87)
8. Nordtank VS 300 300 [kW] (643,359,95)
9. Nordtank VS 300 300 [kW] (632,396,100)
10. Nordtank VS 300 300 [kW] (645,428,100)

## Project description
The primary goal was to investigate of design aspects and design options for wind turbines operating in complex terrain environments within COMTER.ID project. The wind data, which were recorded as part of this measurement programme are available for the "Database on Wind Characteristics".

## Measurement system
The measurements system consist of two meteorological masts, mast n§1 located towards NNW direction and 15 degrees from the north, mast n§2 located towards South direction and 190 degrees from the north The measurements system was based on Garrad Hasan acquisition system. The system operation was primarely focusing on recording structural loads including a number of metereological channels. Data recording frecuency was 40 Hz and duration of 600 seconds.

1. [List of mast signals](./ts_wind/tarifa/ts_mast_signals.csv)

2. [List of additional signals](./ts_wind/tarifa/add_signals.csv) Note: The statistics for these signals are only included in the header of the packed ASCII file.
## Nominal values

![](./ts_wind/tarifa/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/tarifa/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/tarifa/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wind/tarifa/w_dist.gif)
**Figure n4:** Nominal wind speed distribution.

![A map of Denmark](./ts_wind/tarifa/ti_dist.gif)
**Figure n5:** Nominal turbulence distribution.

![A map of Denmark](./ts_wind/tarifa/dir_dist.gif)
**Figure n6:** Nominal wind direction distribution. 

## ACKNOWLEDMENTS	:	
- INSTITUTION       :	Dep. Renevable Energy, CIEMAT Spain
- ADDRESS 	        :   Dep. Renevable Energy, CIEMAT, Av. Complutense 22, 28040 Madrid, Spain
- TEL/FAX 	        :   +34 1 346 63 60 / +34 1 346 60 37
- CONTACTS 	        :   Jorge Navarro / Ignacio Sanchez-Ocana Crespo
- LINKS 	        :   http://www.risoe.dk/vea
- COLLABS 	        :   Ecotecnia, S.A.
- FUND AGENTS       :   E.U. JOULE (COMTER.ID)
- PERIOD 	        :   1997
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14541297'


## PUBLICATIONS	    : 
NA

## Public data
1. Run statistics; tarifa.nc (NetCDF) 
2. Raw time series = tarifa.zip (zip) each with a duration of 600 sec and sampled with 4 Hz. 

