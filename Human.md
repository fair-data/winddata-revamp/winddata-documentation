## Background Information
- CLASSIFICATION: lat(flat landscape), coastal(water and land)
- COUNTRY : South Africa
- ALTITUDE : 110[m]
- - POSITION : [34˚ 5' 46.8708''S 24˚ 30'53.128''E](https://geohack.toolforge.org/geohack.php?pagename=Human&params=34_5_46.8708_S_24_30_53.128_E_)
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

Human		S	34	5	46.8708             E	24	30	53.128		110.00

## Short summary
This resource dataset consists of 10-minute statistics of wind speed and wind direction measurements from a single 62m met mast located in Humansdorp (WM08), South Africa. The period includes 3 years of measurements, which starts in 2010. 

## Map
![A map of SA](./Resource/WASA/SA_Figure1.png)
**Figure 1:** Copy of Figure 1, WASA report. 

## Photo
![Mast layout](./Resource/WASA/wm08.jpg)
**Photo 1:** Picture of the Human mast.

## Drawing 
![Contour map](./Resource/WASA/wm08hc.jpg)
**Draw 1:** Elevation map from SRTM3 data, covering 4×4 km2, with 5-m contours.

![Mast layout](./Resource/WASA/wasa-62m-mast.jpg)
**Draw 2:** Layout of mast

## Reports
1. [Wind Atlas for South Africa (WASA) – Station and Site Description Report] (https://orbit.dtu.dk/files/110949157/DTU_Wind_Energy_E_0071.pdf)

## Mast (relative positions with reference to the reference POSITION) 
1. 62 [m] (0,0,0)

## Project description
As part of the “Wind Atlas for South Africa” project, site inspection trips were carried out by the Council for Scientific and Industrial Research (CSIR) and Risø DTU in April and June of 2011. A total of 10 sites featuring instrumented 60-m masts were visited; the present report summarises the findings of the site inspection teams. The main results are descriptions and documentation of the meteorological masts, instruments and site conditions. For each site, the location and magnetic declination have been determined, as well as the sensor boom directions on the mast. Elevation maps have been constructed to show the surrounding terrain and photos taken to document the land cover. Finally, the observed wind roses and wind speed distribution as of 1 October 2013 are shown.

## Measurement system
10 sites each with an identical 62 m mast. Detailed site description is given in: "Wind Atlas for South Africa (WASA) – Station and Site Description Report". All measurements are available for download.

1. WM01 Alexander Bay [AlexBay]
2. WM02 Calvinia [Calvinia]
3. WM03 Vredendal [VDal] 
4. WM04 Vredenburg [Vburg] 
5. WM05 Napier [Napier]  
6. WM06 Sutherland [Suland] 
7. WM07 Beaufort West [BeaufW] 
8. WM08 Humansdorp [Human] - Current
9. WM09 Noupoort [Noupoort]
10. WM10 Butterworth [Butterw]

## Identification of signals

1. [List of mast signals](./Resource/WASA/Human_mast_signals.csv)

## Distributions
![wind rose](./Resource/WASA/WM08_rose.png)
**Figure 1:** Wind rose measured at Human, furher information is given in the site description. 


## ACKNOWLEDMENTS		
- INSTITUTION       :	DTU Wind Energy
- ADDRESS 	        :   Frederiksborg vej 333, 4000 Roskilde
- TEL/FAX 	        :   +45 4525 2525
- CONTACTS 	        :   Niels G. Mortensen / Mark Kelly
- Funding           : DoE/South Africa and co-funded by SAWEP/NDP-GEF/SA and The Royal Danish Embassy. 
- PERIOD 	        :   2010-06-23 - 2013-10-01 
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14401424'


## PUBLICATIONS	    : 
NA 

## Public data
1. WM08.zip (csv files)


