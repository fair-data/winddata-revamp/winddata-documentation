## Background Information
- CLASSIFICATION: hill(rolling hills), scrub(bushes and small trees)
- COUNTRY : US
- ALTITUDE : 1483 [m]
- POSITION : [42˚ 27' 43'' N 99˚ 52'21'' E](https://geohack.toolforge.org/geohack.php?pagename=calwind&params=42_27_43_N_99_52_21_W_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This dataset consists of raw data and 10-minute statistics of wind speed and wind direction measurements from two met mast at Ainsworth, Nebraska. The period includes 2 years of measurements, which starts in 1990.

## Map
![A map of Denmark](./ts_wind/calwind/msmap.gif)
**Map 1:** Location of Ainsworth, NE 

![A map of Denmark](./ts_wind/nrel/map_01.gif)
**Map 2:** Geographical location of all 11 TCS sites in USA


## Drawings
![](./ts_wind/nrel/draw_01.gif) **Drawing 01:** Turbulence Characterization System (TCS) configuration.
![](./ts_wind/nrel/draw_02.gif) **Drawing 02:** Phase I schematic layout of system components.
![](./ts_wind/nrel/draw_03.gif) **Drawing 03:** Phase II schematic layout of system components.

## Reports
1. [Young Wind Monitor, speed and direction (pdf; 50 kB)](./ts_wind/nrel/ws_05101.pdf)
2. [Maximum Wind Generator specifications (pdf; 240 kB)](./ts_wind/nrel/Maximum_wind.pdf)
3. [NRG Wind Vane specifcations (pdf; 240 kB)](./ts_wind/nrel/NRG_Vane.pdf)

## Mast (relative positions with reference to the reference POSITION) 
1. 40 [m] (0,0,0)
2. 30 [m] (0,10,0)

## Project description
This project was initiated in 1990 to develop measurements and analysis methods that would establish representative and approciate turbulence characteristics needed for the design and siting of costeffective wind turbines. NREL and Batelle had additional objectives that included correlating the nature of the turbulence in the inflow to upwind features, and transferring the technology developed in the project to industry for operational applications. The significant results from this project include the following: 1) Development of a data filtering process which allowed the application of the inexpensive industry standard prospecting anemomenter to be used for appropiate turbulence measurements. 2) Demonstration of the caution required in using the turbulence inensity parameter for wind turbulence applications. 3) Development of an application for the turbulence intensity parameter in site wind power investigations. 4) Discovery of engulfing gusts and fluctuating shear features much stronger at some sites than others. 5) Relation of turbulence characteristics to upwind terrain features. 6) Transfer of design of data acquisition system and software to private industry. 7) Application of data base to provide analysis of extreme turbulence data for IEC turbine design standards. 8) Creation of a CD-ROM turbulence database and users' guide for nine sites representing a variety of wind variability characteristics that can be applied to the development of variable-speed wind turbine control systems.

## Measurement system
This project was initiated in 1990 to develop measurements and analysis methods that would establish representative and approciate turbulence characteristics needed for the design and siting of costeffective wind turbines. NREL and Batelle had additional objectives that included correlating the nature of the turbulence in the inflow to upwind features, and transferring the technology developed in the project to industry for operational applications. The significant results from this project include the following: 1) Development of a data filtering process which allowed the application of the inexpensive industry standard prospecting anemomenter to be used for appropiate turbulence measurements. 2) Demonstration of the caution required in using the turbulence inensity parameter for wind turbulence applications. 3) Development of an application for the turbulence intensity parameter in site wind power investigations. 4) Discovery of engulfing gusts and fluctuating shear features much stronger at some sites than others. 5) Relation of turbulence characteristics to upwind terrain features. 6) Transfer of design of data acquisition system and software to private industry. 7) Application of data base to provide analysis of extreme turbulence data for IEC turbine design standards. 8) Creation of a CD-ROM turbulence database and users' guide for nine sites representing a variety of wind variability characteristics that can be applied to the development of variable-speed wind turbine control systems.

The following sites are included:
1. Hanford, WA
2. Tehachapi, CA - current
3. Monolith, CA
4. Mojave, CA 
5. SanGorgonio, CA
6. Manchester, VT
7. Copenhagen, NY
8. Ainsworth, NE 
9. Jericho, TX
10. Holland, MN
11. Rosiere, WI

## Identification of signals
1. [List of mast signals (time series of wind speed)](./ts_wind/calwind/ts_mast_signals.csv)

2. [List of additional signals](./ts_wind/calwind/add_signals.csv) Note: The statistics for these signals are only included in the header of the packed ASCII file.
## Nominal values

![](./ts_wind/calwind/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/calwind/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/calwind/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wind/calwind/w_dist.gif)
**Figure n4:** Nominal wind speed distribution.

![A map of Denmark](./ts_wind/calwind/ti_dist.gif)
**Figure n5:** Nominal turbulence distribution.

![A map of Denmark](./ts_wind/calwind/dir_dist.gif)
**Figure n6:** Nominal wind direction distribution. 

## ACKNOWLEDGEMENT	:	Larry Wendell 
- INSTITUTION       :	National Renewable Energy Laboratory
- ADDRESS 	        :   1617 Cole Blvd, Golden, CO
- TEL/FAX 	        :   303-384-6902
- CONTACTS 	        :   Sandy Butterfield & Maureen Hand; NREL
- LINKS 	        :   http://www.nrel.org/
- COLLABS 	        :   Batelle, Pacific Northwest Division
- FUND AGENTS       :   Department of Energy's (DOE) Pacific & Laboratory (PNL).
- PERIOD 	        :   1990 - 1993
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14153270'


## PUBLICATIONS	    : 
1. [Turbulence_characterization for wind energy development report (18 MB)](./ts_wind/nrel/turbulence_characterization_for_wind_energy_development.pdf)

## Public data
1. Run statistics; calwind.nc (NetCDF) 
2. Time series (raw); Calwind.zip (zip)

