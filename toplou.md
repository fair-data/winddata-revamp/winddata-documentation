## Background Information
- CLASSIFICATION: mountain(sharp contours - separation expected), pastoral(open fields and meadows)
- COUNTRY : Greece
- ALTITUDE : 145 [m]
- POSITION : [35˚ 13' 23'' N 26˚14'3'' E](https://geohack.toolforge.org/geohack.php?pagename=toplou&params=35_13_23_N_26_14_3_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This dataset consists of time series and 10-minute statistics of wind speed and wind direction measurements from a 40m mast at Toplou, Crete, Greece. The period includes more than 170 hours of measurements from 1997.

## Map
![A map of Denmark](./ts_wind/toplou/map_2.gif)
**Figure 1:** A map of Crete, Greece
![A map of Denmark](./ts_wind/toplou/msmap.gif)
**Figure 2:** A map of Crete towards East

![A map of Denmark](./ts_wind/toplou/map_3.gif)
**Figure 1:** Measurering site.

## Drawing
![A map of Denmark](./ts_wind/toplou/toplou_layout.gif)
**Drawing 1:** Site layout


## Photos
![](./ts_wind/toplou/photo_1.gif) **Photo 01:** View of landscape; direction S
![](./ts_wind/toplou/photo_2.gif) **Photo 02:**View of wt's; direction NE (note met. mast not in position yet)
![](./ts_wind/toplou/photo_3.gif) **Photo 03:**view of the wt, the measuring meteo mast & part of the surrounding site
![](./ts_wind/toplou/photo_4.gif) **Photo 04:**view of the wt and the meteorological mast during erection
![](./ts_wind/toplou/photo_5.gif) **Photo 05:**view of landscape; direction NW


## Reports
NA

## Mast (relative positions with reference to the reference POSITION) 
1. 40 [m] (-79.87,-29.07,1)

## WIND TURBINES
1. Tacke TW-500 500 [kW] (0,0,0)

## Project description
The main objective of the project is to develop a cost effective and easy to apply method for further assessment of fatigue loading effects through measurements. Its simplicity and robustness will enable easy application by industries. Obtained fatigue load "footprints" will be able to back up findings as derived from theoretical research work and will contribute to improved specifications on wind farm and mountainous terrain operation in international guidelines.

## Measurement system
Wind and wind turbine power and loads measurements on mountainous terrain at Toplou site, in Crete. Wind measurements are carried out using cups and vanes and a sonic anemometer for determining the 3D characteristics of the wind and these measurements are coupled to the response of a 500kW stall regulated wt (TW-500) through load and power measurements.

1. [List of mast signals](./ts_wind/toplou/ts_mast_signals.csv)

2. [List of additional signals](./ts_wind/toplou/add_signals.csv) Note: The statistics for these signals are only included in the header of the packed ASCII file.

## Nominal values

![](./ts_wind/toplou/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/toplou/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/toplou/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wind/toplou/w_dist.gif)
**Figure n4:** Nominal wind speed distribution.

![A map of Denmark](./ts_wind/toplou/ti_dist.gif)
**Figure n5:** Nominal turbulence distribution.

![A map of Denmark](./ts_wind/toplou/dir_dist.gif)
**Figure n6:** Nominal wind direction distribution. 

## ACKNOWLEDMENTS	
- INSTITUTION       :	C.R.E.S, Greece
- ADDRESS 	        :   C.R.E.S., 19th km, Marathons Ave, 19009, Pikermi, Attika, Greece
- TEL/FAX 	        :   -6039871 / -6039876
- CONTACTS 	        :   E.E. Morfiakakis
- COLLABS 	        :   DEWI (D), Tacke (D),  FFA (S)
- FUND AGENTS       :   EU (DGXVII), Greek secretariat for Research & Technology
- PERIOD 	        :   1996-07-01 - 1995-06-30
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14494152'


## PUBLICATIONS	    : 
NA

## Public data
1. Run statistics; toplou_all.nc (NetCDF)
2. Run statistics; toplou_concurrent.nc (NetCDF)

Raw time series (ascii) each with a duration of 600 sec, sampled with 1 Hz:
3. toplou.zip (zip)


