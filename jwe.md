## Background Information
- CLASSIFICATION: flat(flat landscape), rural(agriculture with some buildings)
- COUNTRY : Germanu
- ALTITUDE : 1 [m]
- POSITION : [53˚37' 4.69'' N 8˚ 3'.3152'' W](https://geohack.toolforge.org/geohack.php?pagename=jwe&params=53_37_4.6932_N_8_3_6.3152_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This dataset consists of time series and statistics of wind speed and wind direction measurements from a 130m mast located next to Jade wind park, Germany. The period includes more than 300 hours of measurements from 1996-1997.

## Map
![A map of Denmark](./ts_wind/jwe/map_1.gif)
**Figure 1:** Location of Jaw wind park in Northern part of Germany.  
![A map of Denmark](./ts_wind/jwe/msmap.gif)
**Figure 1:** The Jade wind farm is located NW to Wilhelmshafen.

## Photos
![](./ts_wind/jwe/photo_1.gif) **Photo 01:** h=55m, sector 319 - 8 deg

![](./ts_wind/jwe/photo_2.gif) **Photo 02:**h=55, sector 342 - 32 deg
![](./ts_wind/jwe/photo_3.gif) **Photo 03:**h=55, sector 25 - 63 deg
![](./ts_wind/jwe/photo_4.gif) **Photo 04:**h=55, sector = 56 - 103 deg
![](./ts_wind/jwe/photo_5.gif) **Photo 05:**h=55, sector = 88 - 136 deg
![](./ts_wind/jwe/photo_6.gif) **Photo 06:**h=55, sector 127 - 170 deg
![](./ts_wind/jwe/photo_7.gif) **Photo 07:**h=55, sector 160 - 210 deg
![](./ts_wind/jwe/photo_8.gif) **Photo 08:**h=55, sector 203 - 256 deg
![](./ts_wind/jwe/photo_9.gif) **Photo 09:**h=55, sector 233 - 284 deg
![](./ts_wind/jwe/photo_10.gif) **Photo 10:**h=55, sector 279 - 329 deg
## Drawings
![](./ts_wind/jwe/jwe_layout.gif) **Drawing 01:** Site layout 130 mast 
![](./ts_wind/jwe/map_5.gif) **Drawing 01:** Detailed mast layout
![](./ts_wind/jwe/map_10.gif) **Drawing 01:** Anemometer 
![](./ts_wind/jwe/map_11.gif) **Drawing 01:** Cup- vane Anemometer 

## Mast (relative positions with reference to the reference POSITION) 
MASTS
1. 130 [m] (0,0,0)



## WIND TURBINES
1. MBB Aeolus II 3 [kW] (375,-760,0)
2. MBB Monotperos 50 640 [kW] (279,155,0)
3. MBB Monotperos 50 640 [kW] (234,-130,0)
4. MBB Monotperos 50 640 [kW] (244,-423,0)
## Project description
In the framework of the EU funded program "WEGA II Large Wind Turbine Scientific Evaluation Project" (Jou2-CT93-0349) a subproject CAN - Comparison of Aeolus II (located in Germany) and Naesudden II (located in Sweden) - was carried out to investigate the behaviour of two sister wind turbines with 3 MW rated power, but with different control mechanism and tower design. High resolution wind data recorded within this project at the Aeolus II to evaluate the mechanical loads also measured at the turbine are available for the "Database on Wind Characteristics".

## Measurement system
A 130 m high mast served as bases for the meteorological measurements. The meteorological data was collected by two independent data loggers. An Ammonit data logger was used for the long term recording of 5 minute averages of all data relevant for the power curve evaluation at the Aeolus II. In Addition a modular processor-controlled data acquisition system (MOPS) served for investigations of the structural loads and dynamic behaviour of the Aeolus II. Only the data gained from this system are relevant for the Data base on Wind Characteristics. The sample rate was 20 Hz and time series with a duration of 600 seconds have been stored.

1. [List of mast signals](./ts_wind/jwe/ts_mast_signals.csv)

## Nominal values

![](./ts_wind/jwe/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/jwe/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/jwe/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wind/jwe/w_dist.gif)
**Figure n4:** Nominal wind speed distribuion.

![A map of Denmark](./ts_wind/jwe/ti_dist.gif)
**Figure n5:** Nominal turbulence distribuion.

![A map of Denmark](./ts_wind/jwe/dir_dist.gif)
**Figure n6:** Nominal wind direction distribuion. 

## ACKNOWLEDMENTS		
- INSTITUTION       :	Deutsches Windenergie-Institut GmbH
- ADDRESS 	        :   Ebertstr. 96, 26382 Wilhelmshaven, Germany
- TEL/FAX 	        :   ++4421 4808 0 / ++4421 480843
- CONTACTS 	        :	Christian Hinsch
- LINKS 	        :	http://www.dewi.de / 
- FUND AGENTS       :   EU, Joule-II, Jou2-CT93-0349
- PERIOD 	        :   1994-01-01 - 2007-01-11
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14459307'

## PUBLICATIONS	    : 
1. NA

## Public data 
Run statistics (NetCDF format):
1) jwe_all.nc (NetCDF)
2) jwe_concurrent.nc (NetCDF)

Raw time series (ascii) each with a duration of 600 sec, sampled with 20 Hz:
3) jwe.zip


