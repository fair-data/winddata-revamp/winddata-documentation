## Background Information
- CLASSIFICATION: hill(rolling hills), coastal(water and land)
- COUNTRY : Spain
- ALTITUDE :207 [m]
- POSITION : [36˚ 2' 49'' N 5˚ 34'6.76'' E](https://geohack.toolforge.org/geohack.php?pagename=tarifa&params=36_2_49_N_5_34_6.76_W_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)
	  

## Short summary
This dataset consists of raw time series (20 Hz) and 10-minute statistics of wind speed and wind direction measurements from a 35m mast in the Tarifa area, Spain. The period includes more than 600 hours of turbulence measurements during 1998-99.
## Maps
![A map of Spain](./ts_wind/tarifa_2/msmap.gif)
**Map 1:** Location in Southern Spain
![A map of Spain](./ts_wind/tarifa_2/tarifamap.gif)
**Map 2:** Location of site in Spain
## Photos
![](./ts_wind/tarifa_2/map01.gif) **Photo 01:** view over Tarifa Wind Farm as seen from the north of the site
![](./ts_wind/tarifa_2/map02.gif) **Photo 02:** view from mast towards the north (0°)
![](./ts_wind/tarifa_2/map03.gif) **Photo 03:** view from mast towards 45°
![](./ts_wind/tarifa_2/map04.gif) **Photo 04:** view from mast towards the east (90°)
![](./ts_wind/tarifa_2/map05.gif) **Photo 05:** view from mast towards 135°
![](./ts_wind/tarifa_2/map06.gif) **Photo 06:** view from mast towards the south (180°)
![](./ts_wind/tarifa_2/map07.gif) **Photo 07:** view from mast towards 225°
![](./ts_wind/tarifa_2/map08.gif) **Photo 08:**view from mast towards the west (270°)
![](./ts_wind/tarifa_2/map09.gif) **Photo 09:**view from mast towards 315°

## Drawings
![](./ts_wind/tarifa_2/layout.gif) **Drawing 01:** Site layout

## Reports
NA
## Mast (relative positions with reference to the reference POSITION) 
1. 35 [m] (0,0,0)

## WIND TURBINES
1. Nordtank NTK 500 kW 500 [kW] (80,-165,10)
2. Nordtank NTK 500 kW 500 [kW] (52,-100,18)
3. Nordtank NTK 500 kW 500 [kW] (14,-50,10)
4. Nordtank NTK 500 kW 500 [kW] (-12,34,1)
5. Nordtank NTK 500 kW 500 [kW] (-56,95,-15)
6. Nordtank NTK 500 kW 500 [kW] (-154,164,2)
7. Nordtank NTK 500 kW 500 [kW] (-197,231,11)
8. Nordtank NTK 500 kW 500 [kW] (-230,300,20)
9. Nordtank NTK 500 kW 500 [kW] (-237,370,24)
10. Nordtank NTK 500 kW 500 [kW] (-256,463,29)
11. Nordtank NTK 500 kW 500 [kW] (-288,530,38)
12. Nordtank NTK 500 kW 500 [kW] (-289,594,47)

## Project description
 It has been known for many years that under certain climates stall-regulated wind turbines will stall at a lower level than expected - this is known as double-stall. In this project the wind climate was investigated at Tarifa in south Spain near Gibraltar where double-stall has often been observed. The object was to investigate the parameters that are believed to cause double-stall. The three turbulence components u, v and w are measured with sonic anemometer and stored as time-series. Furthermore, weather parameters like temperature, pressure, humidity and precipitation are stored as 10 minute average values. The wind data recorded with sonic anemometer are available for the database. Acknowledgements: Torkild Christensen, Tech-wise A/S

## Measurement system
The measurement system consists of 2 sonic METEK USA-1 anemometers located on a mast in 20 and 30 m height. Weather parameters are collected 3 m above ground level. The prevailing wind directions are east and west. 12 wind turbines and the mast are located on a north south going rim. Consequently, the flow at the mast will not be disturbed by the turbines. Data recording frequency was 20 Hz with the sonics and all data are stored in 600 sec files. File name indicate beginning of 10 minutes sampling period: MMDDHHMM. Eg 01091610 means 9 January, 600 seconds sampled from 4.10 p.m.

1. [List of mast signals (time series of wind speed)](./ts_wind/tarifa_2/ts_mast_signals.csv)

2. [List of additional signals](./ts_wind/tarifa_2/add_signals.csv) Note: The statistics for these signals are only included in the header of the packed ASCII file.
## Nominal values

![](./ts_wind/tarifa_2/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/tarifa_2/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/tarifa_2/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wind/tarifa_2/w_dist.gif)
**Figure n4:** Nominal wind speed distribution.

![A map of Denmark](./ts_wind/tarifa_2/ti_dist.gif)
**Figure n5:** Nominal turbulence distribution.

![A map of Denmark](./ts_wind/tarifa_2/dir_dist.gif)
**Figure n6:** Nominal wind direction distribution. 

## ACKNOWLEDMENTS	:	
- INSTITUTION       :	ELSAMPROJEKT A/S
- ADDRESS 	        :   Kraftvaerksvej 53, 7000 Fredericia
- TEL/FAX 	        :   +45 7923 3333 / +45 7556 4477
- CONTACTS 	        :   Torkild Christensen
- LINKS 	        :   http://www.techwise.dk / 
- COLLABS 	        :   LM Glasfiber A/S
- FUND AGENTS       :   EFP - Danish Minestry of Energy. J. nr. 1363/98-0007
- PERIOD 	        :   1998-01-01 - 2000-08-31
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14541303'

## PUBLICATIONS	    : 
NA

## Public data
1. Run statistics; tarifa_2.nc (NetCDF) 
2. Raw time series = tarifa_2.zip (zip) each with a duration of 600 sec and sampled with 20 Hz. 

