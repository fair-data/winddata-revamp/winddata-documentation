## Background Information
- CLASSIFICATION: hill(rolling hills), scrub(bushes and small trees)
- COUNTRY : US
- ALTITUDE : 1665 [m]
- POSITION : [35˚ 14' 15.22'' N 118˚ 11'0.49'' W](https://geohack.toolforge.org/geohack.php?pagename=skyv39&params=35_14_15.22_N_118_11_0.49_W_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)
## Short summary
This dataset consists of raw time series (8 and 32 Hz)and 10-minute statistics of wind speed and wind direction measurements from a 42m mast in the SkyRiver area, CA, US. The period includes more than 135 hours of turbulence measurements recorded during 1993.

## Map
![A map of Denmark](./ts_wind/skyv39/map_4.gif)
**Figure 1:** Geographic location of wind turbine and met. mast.
![A map of Denmark](./ts_wind/skyv39/map_5.gif)
**Figure 2:** Location of Sky River windfarms compared to Tehachapi anf Mojave.

## Photos

![](./ts_wind/skyv39/photo_1.gif) **Photo 01:** The terrain as seen from the naclle of V39 in direction 58 degrees.

![](./ts_wind/skyv39/photo_2.gif) **Photo 02:** The terrain as seen from the naclle of V39 in direction 148 degrees.
![](./ts_wind/skyv39/photo_3.gif) **Photo 03:** The terrain as seen from the naclle of V39 in direction 238 degrees.
![](./ts_wind/skyv39/photo_4.gif) **Photo 04:** The terrain as seen from the naclle of V39 in direction 328 degrees.
![](./ts_wind/skyv39/photo_5.gif) **Photo 05:** General view of the northen part of the wind farm.
![](./ts_wind/skyv39/photo_6.gif) **Photo 06:** General view of the northen part of the wind farm.
![](./ts_wind/skyv39/r3_drawing_lrg.jpg) **Photo 07:** Gill Solent anemometer.


## Drawings
![](./ts_wind/skyv39/layout.gif) **Drawing 01:** The test site layout.

The test site layout.
![](./ts_wind/skyv39/map_1.gif) **Drawing 02:** The test site topography
![](./ts_wind/skyv39/map_2.gif) **Drawing 03:** The test site topography inc. positions of mast and wt.

## Reports
1. [Description of measurement setup](./ts_wind/skyv39/skyriver.pdf)
## Mast (relative positions with reference to the reference POSITION) 
MASTS
1. 43 [m] (0,0,0)
2. 41.5 [m] (21,34,5)

Wind turbines
1. VESTAS V39 500 [kW] (21,34,5)

## Project description
The Sky River Wind farm is situated in a very complex mountainous terrain about 15 km northeast of Tehachapi Pass. The Sky River wind farm consists of 342 Vestas V27 wind turbines and one Vestas V39 wind turbine. The measurements were carried out in the period from 25 April to 21 June 1993. 

## Measurement system
The objective of the meteorological measurements is to provide information on the incoming wind flow to the wind turbine during load measurements at 2 different locations. Wind speeds were measured at four levels to give indications of the mean wind speed at hub height and to evaluate the mean wind shear. Wind direction is measured to give a general mean wind direction and to evaluate the yaw characteristics of the wind turbine. Local wind structure close to the ground and above hub height was measured to indicate local wind vectors and to indicate the wind structure, specifically the turbulence intensities, coming into the wind turbine rotor. [Measurements on the wind turbine] The main objective of the turbine specific measurements is to determine the dynamic loading of the wind turbine under different operating conditions in the wind farm in mountainous terrain.

1. [List of mast signals](./ts_wind/skyv39/ts_mast_signals.csv)

2. [List of additional signals](./ts_wind/skyv39/add_signals.csv) Note: The statistics for these signals are only included in the header of the packed ASCII file.

## Nominal values

![](./ts_wind/skyv39/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/skyv39/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/skyv39/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wind/skyv39/w_dist.gif)
**Figure n4:** Nominal wind speed distribuion.

![A map of Denmark](./ts_wind/skyv39/ti_dist.gif)
**Figure n5:** Nominal turbulence distribuion.

![A map of Denmark](./ts_wind/skyv39/dir_dist.gif)
**Figure n6:** Nominal wind direction distribuion. 

## ACKNOWLEDMENTS		
- INSTITUTION       :	Risoe National Laboratories
- ADDRESS 	        :   Post Box 49, DK4000 Roskilde
- TEL/FAX 	        :   +45 4677 5017 / +45 4675 5619
- CONTACTS          :   Soren Markkilde Petersen / Gunner Chr. Larsen
- LINKS 	        :	htts://www.risoe.dk/vea/
- COLLABS 	        :	Vestas Wind Systems A/S
- FUND AGENTS       :   Danish Ministry of Energy
- PERIOD 	        :   1993-04-25 - 1993-06-21
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14790621'
## PUBLICATIONS	    : 
1. NA

## Public data
Run statistics stored as NetCDF format:
1. skyv39_ts_all.nc (NetCDF)
2. skyv39_ts_concurrent.nc (NetCDF)

Raw time series (ascii), each with a duration of 600 sec, sampled with 8 & 32 Hz:<br> 
3. skyv39.zip 


