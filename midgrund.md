## Background Information
- CLASSIFICATION: flat(flat landscape), offshore(open sea)
- COUNTRY : Denmark
- ALTITUDE : 0 [m]
- POSITION : [55˚ 42' 10.3'' N 12˚ 39'2.4'' E](https://geohack.toolforge.org/geohack.php?pagename=midgrund&params=55_42_10.3_N_12_39_25.64_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)


## Short summary
This dataset consists of raw time series and statistics of wind speed and wind direction measurements from a 30m mast located on Middelgunden outside Copenhagen, Denmark. The period includes more than 1500 hours of measurements, which starts in 1997.

## Map

![A map of Denmark](./ts_wind/midgrund/msmap.gif)
**Map 2:** Location of midgrund, outside Copenhagen Harbour.

## Photos
![](./ts_wind/midgrund/mast_01.gif) **Photo 01:** 48 m Mast, seen in direction S.
![](./ts_wind/midgrund/mast_02.gif) **Photo 02:** Mast in direction SW, with Lynetten wind turbines and Copenhagen skyline in the background.


## Drawings
![](./ts_wind/midgrund/layout.gif) **Drawing 01:** Site layout
![](./ts_wind/midgrund/draw_01.jpg) **Drawing 01:** Mast layout with correct sensor heights above sea level.

## Reports
1. [Description of measurements setup - pdf file](./ts_wind/midgrund/midgrund.pdf)

## Mast (relative positions with reference to the reference POSITION) 
1. 48 [m] (0,0,0)

# Wind turbines
NA yet

## Project description
The meteorological mast at Middelgrunden was established in 1997 in order to provide wind and turbulence characteristics for the wind farm which was constructed in the autumn 2000. Middelgrunden is an atypical Danish offshore wind energy site in two respects:
1. It is east of the island of Zealand in the Øresund and has a relatively short sea fetch in most directions (from 2.1 km at 210 to a maximum of approximately 20 km across the Øresund and in north-south directions).
2. It is east of the city of Copenhagen, which introduces large roughness elements. These are expected to significantly influence particularly turbulence characteristics at the site.
The Middelgrunden meteorological mast (55˚ 42.1' N 12˚ 39.45' E) was installed in October 1997 and ran until January 2000 when it was destroyed by a ship. At the time of writing (Autumn 2000) plans were underway to re-instrument the mast and to install mains electricity and a fibre optic link from the nearby offshore wind farm.
Right now the (20 x 2) MW windfarm are under construction (nov2000) and described at the owners web-site: www.middelgrunden.dk.
Measurement_System

## Measurement system
The mast is a triangular tapered structure in 7.5 m sections. Booms are oriented 94deg and 274deg with all cup anemometers installed on the westerly booms. Wind speeds are measured at 7.9, 27.7 and 48 m from the base of the mast at Middelgrunden but because of the foundations the actual height above mean sea level is approximately 2.5 m higher. Therefore the wind speed reference levels are 10, 30 and 50 m above mean sea level.

1. [List of mast signals](./ts_wind/midgrund/ts_mast_signals.csv)

2. [List of additional signals](./ts_wind/midgrund/add_signals.csv) Note: The statistics for these signals are only included in the header of the packed ASCII file.

## Nominal values

![](./ts_wind/midgrund/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/midgrund/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/midgrund/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wind/midgrund/w_dist.gif)
**Figure n4:** Nominal wind speed distribution.

![A map of Denmark](./ts_wind/midgrund/ti_dist.gif)
**Figure n5:** Nominal turbulence distribution.

![A map of Denmark](./ts_wind/midgrund/dir_dist.gif)
**Figure n6:** Nominal wind direction distribution. 

## ACKNOWLEDMENTS	:	 
- INSTITUTION       :	Risoe National Laboratory
- ADDRESS 	        :   Department of Wind Energy and Atmospheric Physics 
- CONTACTS 	        :   Gunner C. Larsen / Rebecca Barthelmie
- LINKS 	        :   http://www.risoe.dk/vea/
- COLLABS 	        :	SEAS Distribution A.m.b.A.
- FUND AGENTS      	:	Danish Ministry of Energy & SEAS 
- PERIOD 	        :   1997 - 1999 
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14387639


## PUBLICATIONS	    : 
1. Barthelmie, R.J., Courtney, M. and Nielsen, M., 1998: The wind resource at Middelgrunden.

## Public data
1. Run statistics; midgrund.nc (NetCDF) 
2. Raw time series = midgrund.zip (ascii) each with a duration of 1800 sec and sampled with 5 Hz.


