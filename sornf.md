## Background Information
- CLASSIFICATION: mountain(sharp contours - separation expected), coastal(water and land)
- COUNTRY : Denmark
- ALTITUDE : 749 [m]
- POSITION : [62˚ 5' 0'' N 6˚ 5'0'' E](https://geohack.toolforge.org/geohack.php?pagename=borlum&params=62_5_0_N_6_50_0_W_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This resource dataset consists of 10-minute statistics of wind speed and wind direction measurements from a met mast at Sornfelli, Faro Island, Denmark. The period includes 1 year of measurements, which starts in 1999.

## Map
![A map of Denmark](./Resource/sornf/map_01.gif)
**Figure 1:** A map of the North Atlantic.
![A map of Denmark](./Resource/sornf/map_02.gif)
**Figure 2:** A map of Faro Island, Sornfelli is located NW of Thorshavn.

## Photos
![](./Resource/sornf/photo_01.jpg) **Photo 01:** Heated storage and Gill 2-D sonic anemometer - in upper position.

![](./Resource/sornf/sornfelli.jpg) **Photo 02:** Sornfelli mountain, seen from NW in 1999.

![](./Resource/sornf/Setup_Cam1_big.jpg) **Photo 03:** Web cam, No.1 and heated storage in background.

![](./Resource/sornf/Setup_Cam2_big.jpg) **Photo 01:** Web cam, No.2.

## Mast (relative positions with reference to the reference POSITION) 
1. 3 [m] (0,0,0)

## Project description
To obtain information on the mountain climate in the arctic part of the Faroese landscape, and from this analyse the vertical dimension of climate. Traditionally, most meteorological stations are located near sea level, which makes studies of vertical climate change effects difficult. The Faroe Islands are located in a key region for understanding land-atmosphere-ocean interaction in the North Atlantic region, as they are the only land area completely surrounded by the North Atlantic Drift. Sornfelli is a 749 m high mountain at the Faroe Islands in the middle of the North Atlantic Ocean, at the main island Streymoy, north west of the main city Thorshavn at 62N.

## Measurement system
MetSupport have built a special heated instrument drum to solve the problem. Most of the time the instruments are inside the heated drum to prevent ice build up on the instruments. Every half an hour the instruments are lifted out of the top of the drum and exposed to the open air for 10 minutes to measure the actual weather. Then the instruments are lowered back into the "nice and warm" instrument drum for 20 minutes, before the start of the next measurement period. The instrument measuring wind speed and wind direction is a 2 axis ultrasonic anemometer of the type "Solent WindObserver", model 1172T from Gill Instruments LTD. Temperatures are measured using Pt100 sensors. The measured wind speed and wind direction are 10 minutes mean values from the minute 00 to 10 and again from the minute 30 to 40 every hour. The temperature sensor must be exposed to the actual weather for a period before it can measure the right temperature. Therefore the temperature is a one minute mean value from the minute 09 to 10 and again from the minute 39 to 40.

1. [List of mast signals](./Resource/sornf/ts_mast_signals.csv)

## Nominal values

![Nominal values](./Resource/sornf/nom_speed.gif)
**Figure n1:** Nominal wind speed


![A map of Denmark](./Resource/sornf/nom_dir.gif)
**Figure n2:** Nominal wind direction


## ACKNOWLEDMENTS	
- ACKNOWLEDMENTS	: Institute of Geography,University of Copenhagen & MetSupport ApS, Roskilde, Denmark
- INSTITUTION       :	Institute of Geography,University of Copenhagen, Øster Voldgade 10,DK-1350,  Copenhagen K
- ADDRESS 	        :   Post box 49, DK4000 Roskilde, Denmark
- TEL/FAX 	        :   +45 35322500 / +45  35322501
- CONTACTS 	        :   Hanne Hvidtfelt Christiansen / MetSupport
- LINKS 	        :   http://www.geogr.ku.dk/projects/link / http://www.metsupport.dk/data/sornfelli/
- COLLABS 	        :	MetSupport ApS
- FUND AGENTS       :	The Danish Research Council
- PERIOD 	        :   1999-11-19 00:00:00 - 2001-12-31 00:00:00
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14185754'


## PUBLICATIONS	    : 
1. 

## Public data
1. Resource data  (NetCDF) 

