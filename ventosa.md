## Background Information
- CLASSIFICATION: hill(rolling hills), scrub(bushes and small trees)
- COUNTRY : Mexico
- ALTITUDE : 50 [m]
- POSITION : [16˚ 34' 31'' N 94˚ 49'41'' E](https://geohack.toolforge.org/geohack.php?pagename=ventosa&params=16_34_31_N_94_49_41_W_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This resource dataset consists of 10-minute statistics of wind speed and wind direction measurements from a singel 32m met mast located in oat La Venta or Ventosa, Oaxaca Mexico. The period includes 6 years of measurements, which starts in 2000. 

## Map
![A map of Mexico](./Resource/ventosa/mexico.jpg)
**Figure 1:** A map of Mexico
![A map of Denmark](./Resource/ventosa/msmap.gif)
**Figure 1:** A map of La Ventosa, Oaxaca.

## Reports
[Presentation of one year measurements](./Resource/ventosa/One year.pdf)
1. [One year of measurements](./Resource/Ventosa/normal_operation.pdf)
2. [Description of La Ventosa wind data](./Resource/Ventosa/data-laVentosa.pdf)
3. [Bimodal wind speed distribition](./Resource/Ventosa/BiModal_LaVenta.pdf)


## Mast (relative positions with reference to the reference POSITION) 
1. 32 [m] (0,0,0)

## Project description
Measurements are performed at different locations in Mexico for estimating the wind energy resource.

## Measurement system
Standard measurement system consisting of a 30 m mast equipped with NRG cups and Campbell model CR10X logger.

1. [List of mast signals](./Resource/ventosa/Mast_signals.csv)

## Distributions
![A map of Denmark](./Resource/ventosa/ventosa_dist.gif)
**Figure 1:** Nominal wind speed distribuion.


## ACKNOWLEDMENTS		
- INSTITUTION       :	Instituto de Investigaciones Eltricas,Division de Energia Alternas
- ADDRESS 	        :   Av. REFORMA No. 113 COL. PALMIRA, 62490 TEMIXCO, MORELOS, MEXICO
- TEL/FAX 	        :   +52 73 18 38 11 x 7251 / +52 73 18 24 36
- CONTACTS 	        :   Marco Borja 
- PERIOD 	        :   2002-01-01 -
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14135609'

## PUBLICATIONS	    : 
1. 

## Public data
1. Resource data  (NetCDF) 


