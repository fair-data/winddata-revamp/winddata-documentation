## Background Information
- CLASSIFICATION: mountain(sharp contours - separation expected), pastoral(open fields and meadows)
- COUNTRY : UK
- ALTITUDE : 51 [m]
- POSITION : [55˚ 17' 15.24'' N 4˚ 11'57.18'' W](https://geohack.toolforge.org/geohack.php?pagename=windy&params=55_17_15.24_N_4_11_57.18_W_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)
## Short summary
This dataset consists of time series and statistics of wind speed and wind direction measurements from one 45m offshore mast at Windy, UK. The period includes more than 400 hours of measurements from 1998.

## Maps
![A map of Denmark](./ts_wind/windy/msmap.gif)
**Figure 1:** A map of UK
![A map of Denmark](./ts_wind/windy/map_1.gif)
**Figure 2:** Local wind farm map including wind turbine disribution.

![A map of Denmark](./ts_wind/windy/mast_82.gif)
**Figure 3:** Mast location, close to turbines P11 & P12.

## Photos
![](./ts_wind/windy/photo_1.gif) **Photo 01:** Picture of the met mast.

![](./ts_wind/windy/photo_2.gif) **Photo 01:** Picture of the met mast.
![](./ts_wind/windy/photo_3.gif) **Photo 01:** Picture of the met mast including surroundings.

## Drawings
![](./ts_wind/windy/layout.gif) **Drawing 01:** Site layout for 33 mast.

## Mast (relative positions with reference to the reference POSITION) 
MASTS
1. 33.5 [m] (0,0,0)

## Wind turbine

1. Nordtank NTK 600/37H 600 [kW] (1,-66,85)
2. Nordtank NTK 600/37H 600 [kW] (1,-256,85)
3. Nordtank NTK 600/37H 600 [kW] (1,-236,85)
4. Nordtank NTK 600/37H 600 [kW] (1,-85,90)
5. Nordtank NTK 600/37H 600 [kW] (1,94,55)
6. Nordtank NTK 600/37H 600 [kW] (958,-9,10)
7. Nordtank NTK 600/37H 600 [kW] (813,174,15)
8. Nordtank NTK 600/37H 600 [kW] (713,-2,5)
9. Nordtank NTK 600/37H 600 [kW] (673,294,5)
10. Nordtank NTK 600/37H 600 [kW] (513,93,5)
11. Nordtank NTK 600/37H 600 [kW] (140,99,-5)
12. Nordtank NTK 600/37H 600 [kW] (-50,129,-5)
13. Nordtank NTK 600/37H 600 [kW] (-128,287,-5)
14. Nordtank NTK 600/37H 600 [kW] (3,384,-5)
15. Nordtank NTK 600/37H 600 [kW] (-187,494,-5)
16. Nordtank NTK 600/37H 600 [kW] (-216,714,-5)
17. Nordtank NTK 600/37H 600 [kW] (-389,754,-25)
18. Nordtank NTK 600/37H 600 [kW] (-197,884,-25)
19. Nordtank NTK 600/37H 600 [kW] (464,1,-45)
20. Nordtank NTK 600/37H 600 [kW] (583,1,-45)
21. Nordtank NTK 600/37H 600 [kW] (625,1,-35)
22. Nordtank NTK 600/37H 600 [kW] (813,1,-25)
23. Nordtank NTK 600/37H 600 [kW] (793,1,0)
24. Nordtank NTK 600/37H 600 [kW] (983,1,10)
25. Nordtank NTK 600/37H 600 [kW] (883,1,10)
26. Nordtank NTK 600/37H 600 [kW] (1,1,15)
27. Nordtank NTK 600/37H 600 [kW] (973,994,15)
28. Nordtank NTK 600/37H 600 [kW] (1,944,5)
29. Nordtank NTK 600/37H 600 [kW] (1,774,15)
30. Nordtank NTK 600/37H 600 [kW] (1,734,15)
31. Nordtank NTK 600/37H 600 [kW] (1,554,5)
32. Nordtank NTK 600/37H 600 [kW] (1,564,15)
33. Nordtank NTK 600/37H 600 [kW] (1,394,20)
34. Nordtank NTK 600/37H 600 [kW] (1,434,25)
35. Nordtank NTK 600/37H 600 [kW] (1,257,45)
36. Nordtank NTK 600/37H 600 [kW] (1,266,35)
## Project description
The CEC has declared that a central objective of its R&D actions in the field of wind energy is to reduce the cost of wind generated electricity to 0.04 ECU/kWh or less. It is probable that this target will be first reached on sites which offer high energy density but which also bring considerable risks due to high extreme wind speeds, severe turbulence due to highly complex terrain, and conditions of icing. It is essential that these risks are properly studied and understood in order that the full potential of such hostile sites might be safely realised. Thorough investigations have been undertaken of the meteorology, the behaviour and loading of the wind turbines at Windy Standard in the U.K. also at Acqua Spruzza in Italy. The objectives of the project have been as follows: · To understand, and hence reduce, the risks associated with the use of wind farm sites in hostile conditions; · To provide a critical appraisal of present design procedures used for hostile environments, and to refine the design classification of wind turbines for such sites; · To disseminate the results of the project to wind turbine manufacturers, wind farm developers, Classification Societies and Standards bodies.
## Measurement system
Meteorological instruments were monitored at three heights: 7.5 m, 16.5 m and 33.5 m. Cup anemometers at 7.5 m, 16.5 m and 33.5 m were heated. There was also a sonic anemometer at 7.5 m. No icing of instruments was experienced during the period of data supplied to the Danish Technical University. The meteorological mast was upwind of the turbine at which loads and other operational parameters were measured. The meteorological mast consists of a single conical steel tower which therefore causes considerable wake effects downwind.

1. [List of mast signals](./ts_wind/windy/ts_mast_signals.csv)

2. [List of additional signals](./ts_wind/windy/add_signals.csv) Note: The statistics for these signals are only included in the header of the packed ASCII file.

## Nominal values

![](./ts_wind/windy/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/windy/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/windy/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wind/windy/w_dist.gif)
**Figure n4:** Nominal wind speed distribuion.

![A map of Denmark](./ts_wind/windy/ti_dist.gif)
**Figure n5:** Nominal turbulence distribuion.

![A map of Denmark](./ts_wind/windy/dir_dist.gif)
**Figure n6:** Nominal wind direction distribuion. 

## ACKNOWLEDMENTS		
- INSTITUTION       :	Garrad Hassan and Partners Ltd.
- ADDRESS 	        :   The Coach House, Folleigh Lane, Long Ashton, Bristol. BS41 9JB UK
- TEL/FAX 	        :   +44 (0)1275 394360 / +44 (0)1275 394361
- CONTACTS 	        :	Mark Jonhston
- COLLABS 	                :	National Wind Power Ltd., ENEL, Germanischer Lloyd AG , NEG-Micon
- FUND AGENTS       :   EU and UK Department of Trade and Industry
- PERIOD 	        :   1996-01-01 - 1998-12-31 
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14495073'

## PUBLICATIONS	    : 
1. NA
## Public data 
Run statistics (NetCDF format);
1) windy_all.nc (NetCDF)
2) windy_concurrent.nc (NetCDF)

Raw time series each with a duration of 600 sec, sampled with 10 Hz:
3) windy.zip


