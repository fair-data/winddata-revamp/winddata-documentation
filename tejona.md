## Background Information
- CLASSIFICATION: mountain(sharp contours - separation expected), scrub(bushes and small trees)
- COUNTRY : CostaRica
- ALTITUDE : 780 [m]
- POSITION : [10˚ 32' 35'' N 85˚ 0'8.9'' E](https://geohack.toolforge.org/geohack.php?pagename=tejona&params=10_32_35_N_85_0_8.9_W_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This dataset consists of time series and statistics of wind speed and wind direction measurements from a met mast on a ridge near Tejona, Costa Rica. The period includes more than 2500 hours of measurements, which starts in 2000.

## Map
![A map of Denmark](./ts_wind/tejona/costarica.gif)
**Figure 1:** Location of Costa Rica
![A map of Denmark](./ts_wind/tejona/map2.jpg)
**Figure 2:** Map of Costa Rica
![A map of Denmark](./ts_wind/tejona/msmap.gif)
**Figure 3:** Detailed image of the test site
![A map of Denmark](./ts_wind/tejona/map4.jpg)
**Figure 3:** Detailed image of the test site

## Photos
![](./ts_wind/tejona/photo1.jpg) **Photo 01:** Last inspection
![](./ts_wind/tejona/photo2.jpg) **Photo 02:** Inspection of guy wires
![](./ts_wind/tejona/photo3.jpg) **Photo 03:** Raise of mast
![](./ts_wind/tejona/photo4.jpg) **Photo 04:** Mast almost ready
![](./ts_wind/tejona/sonic_ane.jpg) **Photo 05:** Sonic anemometer


## Mast (relative positions with reference to the reference POSITION) 
MASTS
1. 30 [m] (0,0,0)

## Project description
In view of the final lay out of a 20 MW wind farm in Tejona ECN has carried out the micrositing for the wind power plant. The 20 MW is built up by 30 wind turbines of 660 kW. The process involved determination of the wind climate, the terrain orography and roughness, the wind turbine characteristics and the wind turbine positions. For this purpose the ECN Wind Energy Experiments group has installed a measuring mast for short-term measurements at the site.
ded in the international winddatabase.

## Measurement system
The measurement system consists of a 30.9m mast with four guy wires. There are two sonics at the top, a cup anemometer 1m below the top and a vane and cup anemometer at 18m height. The meteo measurements include rain, pressure and temperature.


1. [List of mast signals](./ts_wind/tejona/ts_mast_signals.csv)

2. [List of additional signals](./ts_wind/tejona/add_signals.csv) Note: The statistics for these signals are only included in the header of the packed ASCII file.
## Nominal values

![](./ts_wind/tejona/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/tejona/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/tejona/nom_dir.gif)
**Figure n3:** Nominal wind direction

## ACKNOWLEDGEMENTS		
- INSTITUTION       :	Energy research Center of the Netherlands (ECN)
- ADDRESS 	        :   PO Box 1, NL1755ZG Petten, The Netherlands
- TEL/FAX 	        :   +31 224 564115 / +31 224 568214
- CONTACTS 	        :   Frank Ormel / Peter Eecen
- LINKS 	        :   http://www.ecn.nl/
- COLLABS 	        :   ICE, Essent Duurzaam BV
- FUND AGENTS       :   Essent Duurzaam BV
- PERIOD 	        :   2000-11-01 - 2001-03-31
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14387300'


## PUBLICATIONS	    : 
NA 

## Public data
1. Run statistics; tejona_all.nc (NetCDF)
2. Raw time series = tejona.zip (zip) each with a duration of 600 sec, sampled with 4 and 25 Hz.



