## Background Information
- CLASSIFICATION: flat(flat landscape), rural(agriculture with some buildings)
- COUNTRY : Sweden
- ALTITUDE : 25 [m]
- POSITION : [33˚ 56' 52'' N 116˚ 36'29'' E](https://geohack.toolforge.org/geohack.php?pagename=marsta&params=59_57_0_N_17_35_0_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This dataset consists of raw data and 10-minute statistics of wind speed and wind direction measurements from a 10m mast near Marsta, Swenden. The period includes less than 3000  hours of measurements, which starts in 1994.

## Map
![A map of Denmark](./ts_wind/marsta/map_01.gif)
**Map 1:** Location of Marsta in Sweden
![A map of Denmark](./ts_wind/marsta/msmap.gif)
**Map 2:** Location of Marsta near Uppsala in Sweden
![A map of Denmark](./ts_wind/marsta/map_03.jpg)
**Map 3:** Location of Marsta

## Photos
![](./ts_wind/marsta/photo_02.jpg) **Photo 01:** View from met. mast, direction SW

![](./ts_wind/marsta/photo_01.jpg) **Photo 02:** Meteorological mast from previous experiment

![](./ts_wind/marsta/sonic_ane.jpg) **Photo 03:** Gill Solent anemometer


## Drawings
![](./ts_wind/marsta/layout.gif) **Drawing 01:** Site layout.
![](./ts_wind/marsta/r3_drawing_lrg.jpg) **Drawing 02:** Gill Solent anemometer.


## Reports
NA

## Mast (relative positions with reference to the reference POSITION) 
1. 10 [m] (0,0,0)


## Project description
Meteological measurements from a farmland site in the centre of Sweden.

## Measurement system
21 Hz data from 10m level on a 24 m tower for ½ year. Note: NOTE! Each raw file is usually about 55-56 min long, but i some cases the measurements are not continuous during the hour. Each time series has been divided into 5 different parts: 1) 0 - 10 min, 2) 10 - 20 min, 3) 20 - 30 min, 4) 30 - 40 min, 5) 40 - 50 min. which are included in the database.

1. [List of mast signals (time series of wind speed)](./ts_wind/marsta/ts_mast_signals.csv)

2. [List of additional signals](./ts_wind/marsta/add_signals.csv) Note: The statistics for these signals are only included in the header of the packed ASCII file.

## Nominal values

![](./ts_wind/marsta/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/marsta/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/marsta/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wind/marsta/w_dist.gif)
**Figure n4:** Nominal wind speed distribution.

![A map of Denmark](./ts_wind/marsta/ti_dist.gif)
**Figure n5:** Nominal turbulence distribution.

![A map of Denmark](./ts_wind/marsta/dir_dist.gif)
**Figure n6:** Nominal wind direction distribution. 

## ACKNOWLEDMENTS	:	 
- INSTITUTION       :	Dept. of Meteorology, Uppsala University
- ADDRESS 	        :   Uppsala University, Box 516 S-751 20 Uppsala, Sweden
- TEL/FAX 	        :   +46-18 542792 / +46-18 544 706
- CONTACTS 	        :   Mikael Magnusson / Ann-Sofi Smedman
- LINKS 	        :   http://www.met.uu.se/
- PERIOD 	        :   1994 - 1995
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14387477'


## PUBLICATIONS	    : 
NA

## Public data
1. Run statistics; marsta.nc (NetCDF) 
2. Raw time series = marsta.zip (ascii), duration of 600 sec and sampling frequency = 21 Hz.


