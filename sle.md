## Background Information
- CLASSIFICATION: flat(flat landscape), coastal(water and land)
- COUNTRY : Norway
- ALTITUDE : 4 [m]
- POSITION : [63˚ 39' 57.35'' N 8˚ 15'45.69'' E](https://geohack.toolforge.org/geohack.php?pagename=sle&params=63_39_57.35_N_8_15_45.69_E_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This dataset consists of raw time series and run statistics of wind speed and wind direction measurements from the Sletringen 45m masts (Titran) Frøya, Norway. The period includes than 4000 hours of measurements from 1995.

## Map
![A map of Denmark](./ts_wind/sle/msmap.gif)
**Map 1:** Western part of Mid-Norway.
![A map of Denmark](./ts_wind/sle/layout.gif)
**Map 2:** Sletringen site layout.

## Photos
![](./ts_wind/sle/Sletringen.gif) **Photo 01:** The mast and the Lighthouse on Sletringen - Photo: Geir Ole Søreng

## Reports

## Mast (relative positions with reference to the reference POSITION) 
3.  45 [m] (0,0,0)

## Project description
The station was build as a part of the Norwegian Wind Energy Programme in 1980. The purpose was to study the wind structure in details. Particularly, a database for high wind speed condition was desired. Data were originally intended for wind energy production; the dimension af the masts corresponds to a large WECS. The station should also serve as a reference station for other measurement stations in the region. Together with a fourth mast 4 km further west (Sletringen) the station has provided data for calculation of dynamic wind loads on off-shore constructions.

## Measurement system
The measurment system consists of a 45m meteorological masts (100, 100 and 45 m) placed in a triangle 80-180 metres apart. Ten-minute average of wind speed and direction have been recorded since 1982. From 1988, the logging frequency has been 0.85 Hz. 40-60 channels have been recorded continuously. In a 45 m mast at Sletringen, data has been recorded for some periods since 1988.

1. [List of mast signals (time series of wind speed)](./ts_wind/sle/ts_mast_signals.csv)

## Nominal values

![](./ts_wind/sle/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wind/sle/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wind/sle/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wind/sle/w_dist.gif)
**Figure n4:** Nominal wind speed distribution.

![A map of Denmark](./ts_wind/sle/ti_dist.gif)
**Figure n5:** Nominal turbulence distribution.

![A map of Denmark](./ts_wind/sle/dir_dist.gif)
**Figure n6:** Nominal wind direction distribution. 

## ACKNOWLEDMENTS	:	Jørgen Løvseth, NTNU 
- INSTITUTION       :	Norwegian Univ. of Sci. and Techn., Dept. of Physics
- ADDRESS 	        :   NTNU, Dept. of Physics, Lade, N-7034 Trondheim, Norway
- TEL/FAX 	        :   +47 7359 1856 / +47 7359 1852
- CONTACTS 	        :   Jørgen Løvseth, NTNU
- LINKS 	        :   http://www.phys.ntnu.no/lade/forskning/miljo/miljofys.htm#Vind / 
- PERIOD 	        :   1992 - 1995
- Naming_authority  : 'DTU Data'
- DOI               : 'https://doi.org/10.11583/DTU.14380775'
 

## PUBLICATIONS	    : 
NA

## Public data
1. Run statistics; sle.nc (NetCDF) 
2. Raw time series = sle.zip (zip) each with a duration of 3600 sec and sampled with 0.85 Hz. 

