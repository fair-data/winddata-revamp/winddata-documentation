## Background Information
- CLASSIFICATION: flat(flat landscape), coastal(water and land)
- COUNTRY : Denmark
- ALTITUDE : 5 [m]
- POSITION : [55˚ 41' 3.97'' N 12˚ 5'48.08'' E](https://geohack.toolforge.org/geohack.php?pagename=nordtank&params=55_41_3.97_N_12_5_48.08_E_)
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)

## Short summary
This dataset consists of raw time series from a mast, turbine power and load measurements (appr. 35 Hz) including 10-minute statistics of these measurements. The 36m mast and the 41/550kW turbine is located at DTU Risø campus, Denmark. The period includes more than 1900 hours of mast and turbine measurements recorded during the period Oct04 - Apr06.

## Map
![A map of Denmark](./ts_wt/nordtank/msmap.gif)
**Figure 1:** A map of Sealand, Denmark
![A map of Denmark](./ts_wt/nordtank/map_01.jpg)
**Figure 2:** Windturbine location at former test station, inside DTU Risoe Campus. 

## Drawings
![](./ts_wt/nordtank/layout.gif) **Drawing 01:** Layout of 36m mast & turbine. 
## Photos
![](./ts_wt/nordtank/photo_01.gif) **Photo 01:** Nordtank NTK500/41 Wind turbine seen towards west
![](./ts_wt/nordtank/photo_03.JPG) **Photo 02:** Wind turbines on former test station
![](./ts_wt/nordtank/photo_05.JPG) **Photo 03:** Met. mast seen from Nordtank wind turbine
![](./ts_wt/nordtank/blade_2.JPG) **Photo 04:** Vortex generators on blade
![](./ts_wt/nordtank/blade_3.JPG) **Photo 05:** Blade root with SG-installation
![](./ts_wt/nordtank/photo_11.JPG) **Photo 06:** Landscape, seen from WT, direction NE
![](./ts_wt/nordtank/photo_12.JPG) **Photo 07:** Landscape, seen from WT, direction E
![](./ts_wt/nordtank/photo_13.JPG) **Photo 08:** Landscape/water, seen from WT, direction W
![](./ts_wt/nordtank/photo_14.JPG) **Photo 09:** Landscape, seen from WT, direction SW
![](./ts_wt/nordtank/photo_15.JPG) **Photo 10:** Landscape, seen from WT, direction NW
![](./ts_wt/nordtank/photo_16.JPG) **Photo 11:** Landscape, seen from WT, direction S
![](./ts_wt/nordtank/photo_17.JPG) **Photo 12:** Landscape, seen from WT, direction SE
![](./ts_wt/nordtank/photo_18.jpg) **Photo 13:** Nacelle mounted METEK 3-D sonic anemometer

## Reports
1. [Description of the 500kW Nordtank wind turbine](./ts_wt/nordtank/WT_description.pdf)
2. [Presentation of the Nordtank measurement system](./ts_wt/nordtank/Nordtank_poster.pdf)
3. [Cup anemometer presentation](./ts_wt/nordtank/Riso_P2546A_cup_anemometer.pdf)

## MAST
1 36 [m] (-80,0,0)

## WIND TURBINES
1 Nordtank A/S NTK 500/41 500 [kW] (0,0,0)

## Project description
As part of the International Master of Science Program in Wind Energy at DTU, a complete interactive wind turbine measurement laboratory has been developed. A 500 kW stall regulated wind turbine has been instrumented with sensors for recording 1) turbine operational parameters, 2) meteorological conditions, 3) electrical quantities and 4) mechanical loads in terms of strain gauge signals. The data acquisition system has been designed and implemented by Risø together with students and teachers from DTU. It is based on LabVIEW© combined with a MySQL database for data management. The system enables online access for real-time recordings, which are used both for demonstration purposes, in individual [student] exercises and in scientific investigations. Long-term registration of wind turbine loads results in a unique database of non-commercial time series, which are available for practicing fatigue calculations and extreme loads estimation in basic wind turbine courses. Power quality analysis is carried out based on high speed sampled, three-phase voltage and current signals. The wide spectrum of sensors enables a detailed study of the correlation between meteorological, mechanical and electrical quantities. Measurements are acquired by a PC placed at the wind turbine site near Risø National Laboratory. The PC can be remotely controlled from DTU, which gives the students the opportunity to work on an operating wind turbine. Furthermore, measurements are published on WindData.com, which facilitates cooperation with other Universities. WTMLAB enables development of new data acquisition utilities, enables hands-on experiences with wind turbine operations, enables design of new test procedures and algorithms and for collaboration with other universities and research institutions through the Internet. The database is open and published on the Internet. Thus, co-operation with other Institutions and Universities is possible – and welcome.

## Measurement system
The wind turbine is equipped with a data acquisition system for recording meteorological properties (wind speed, wind direction, air temperature, atmospheric pressure and rain), wind turbine operational properties (electric active power, nacelle position, rotor speed, rotor position, status signals) and structural loads in terms of strain gauge measurements from the blade root, the main shaft, tower top and tower bottom. DATA ACQUISITION A PC-based data acquisition system has been designed to monitor and collect data from the wind turbine sensors. The output signals from all sensors are conditioned to the +/- 5 V range. The signals are either continuously varying (strain gauges, temperature…) or of digital types such as train of pulses (rotational speed, anemometer…) or on/off levels (status of brake, blade tips and generator). All signals - except outputs from voltage and current transformers - are connected to one of three RISØ P2558A data acquisition units (DAU). One DAU unit is located in the bottom of the wind turbine tower, another in the nacelle and the third is mounted on the hub – it is rotating and transmitting data over a RF-link. The serial channel from each DAU is connected to the PC over a multi-port serial plug-in board. The selected scan rate of 35 Hz is sufficient for meteorological properties and structural load measurements, but it is far too slow when studying the impact of the wind turbine on the power grid. Thus, another part of the acquisition system works at a higher sampling rate (12.8 kHz) on transducer signals originating from the 3 phase voltages and currents at the 690 V generator terminals – see Figure 1. The signals are connected to a signal-conditioning interface that performs sample and hold operation, scales the signals to the ± 5 V level and delivers the conditioned signals to a 12-bit multi-channel ADC plug-in board in the PC. A standard desktop PC is used as the central component of the data acquisition system. The PC is connected to the Internet and thereby to DTU from where it can be operated remotely. Dedicated measurement software has been developed under LabVIEW©. Data are collected in 10 minutes intervals and the complete time series plus statistics derived from this series are saved on disk ready for later treatment. The electric power signals are treated differently due to the fast sampling rate. The idea is to aggregate power quality parameters over 10 minutes’ intervals – synchronous with the DAU intervals so atmospheric, mechanical and electrical behaviour directly can be correlated. To diminish the amount of data, on-line data reduction is compulsory. Autumn 2006, a 3d-Sonic anemometer has been installed on the nacelle. The signals (sx,sy,sz &st) has been recorded with 10 Hz both with operating, idling and parked.

1. [List of mast signals](./ts_wt/nordtank/ts_mast_signals.csv) 

2. [List of turbine signals](./ts_wt/nordtank/ts_wt_signals.csv) 


3. [List of additional signals](./ts_wt/nordtank/add_signals.csv) Note: The statistics for these signals are only included in the header of the packed ASCII files.
## ACKNOWLEDMENTS	:	
- INSTITUTION 		:	Risoe National Laboratories
- ADDRESS 	        :	Post box 49, DK4000 Roskilde, Denmark
- TEL/FAX 	        :	+45 46775044
- CONTACTS 	        :	Kurt S. Hansen / Uwe Schmidt Paulsen
- LINKS 	        :	http://www.risoe.dk/vea-data/
- FUND AGENTS      	:	Internal
- PERIOD 	        :	2004-10-21	- 2006-04-20 
- Naming_authority  :   'DTU Data'
- DOI               :   'https://doi.org/10.11583/DTU.16782598' 
## PUBLICATIONS	 

1. [Online wind turbine measurement laboratory](https://orbit.dtu.dk/files/2485403/paper0452.pdf)
## Public data
1. Run statistics (NetCDF)
2. Raw time series (appr. 35Hz) Nordtank.zip (zipped ascii files)
3. Re-sampled wind speed and power (1 Hz) Nordtank_1Hz.zip (zipped csv-files)


