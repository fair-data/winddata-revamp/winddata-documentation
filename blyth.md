## Background Information
- CLASSIFICATION: flat(flat landscape), offshore(open sea)
- COUNTRY : UK
- ALTITUDE : 0 [m]
- POSITION : [55. 7'47.61'' N 1. 29' 51.47'' W](https://geohack.toolforge.org/geohack.php?pagename=blyth&params=55_7_47.61_N_1_29_20_W_)	
(Note: Geohack often includes national high resolution maps, otherwise try Google Earth)
## Short summary
This dataset includes both wind measurements and structural measurements from one of the very first offshore wind farms, located next to blyth, UK. The dataset consists of time series and 10-minutes statistics. The duration of the time series is 442 hours, while the statistics covers 20600 hours, recorded during the years 2001-2003.    
## Map
![A map of Denmark](./ts_wt/blyth/map_01.gif)
**Map 1:** A map of the North Sea with Blyth close the northeastern coastline of UK.

![A map of Denmark](./ts_wt/blyth/map_02.jpg)
**Map 2:** Zoom into the blyth position next to Blyth WF and the met mast, which is located on the coast.

![A map of Denmark](./ts_wt/blyth/map_03.jpg)
**Map 3:** The met mast is located on the coast.

## Photos
![](./ts_wt/blyth/photo_01.jpg) **Photo 01:** Blyth offshore landing platform
![](./ts_wt/blyth/photo_02.jpg) **Photo 02:** blyth offshore wind farm direction SE.
![](./ts_wt/blyth/photo_03.jpg) **Photo 03:** Blyth offshore wind farm consists of 2 turbines.

## Graphs
NA
## Drawings
![](./ts_wt/blyth/layout.gif) **Drawing 01:** Site layout.
![](./ts_wt/blyth/draw_02.jpg) **Drawing 02:** Structural measurements in tower below mean sea leval (MSL).
![](./ts_wt/blyth/draw_01.jpg) **Drawing 03:** Saab Wave radar installation.

## Reports
1. [Load measurements on a 2MW offshore wind turbine in the north sea.](./ts_wt/blyth/doc_01.pdf)
2. [WAVE LOADS ON SLENDER OFFSHORE STRUCTURES.](./ts_wt/blyth/doc_02.pdf)
3. [HYDRODYNAMIC LOADING ON SLENDER OFFSHORE WIND TURBINES.](./ts_wt/blyth/doc_03.pdf)
4. [Hydrodynamic Loading on Offshore Wind Turbines.](./ts_wt/blyth/doc_04.pdf)

## Mast (relative positions with reference to the reference POSITION) 
1. 40 [m] (0,0,0)

## Windturbines
1. Vestas V66 2000 [kW] (462,571,0) (Reference POSITION)
- COORDINATES 	  	:	X:462 Y:571 Z:0 [m], reference= site position.
- RATED_POWER 	  	:	2000 [kW] at 14 [m/s]
- HUB_HEIGHT 	  	:	61.8 [m]- ROTOR_DIAMETER	:	66 [m]
- DESCRIPTION 	  	:	Pitch regulated  

2. Vestas V66 2000 [kW] (393,882,0)
- COORDINATES 	  	:	X:393 Y:882 Z:0 [m], reference= site position.
- RATED_POWER 	  	:	2000 [kW] at 14 [m/s]
- HUB_HEIGHT 	  	:	61.8 [m]
- ROTOR_DIAMETER	:	66 [m]
- DESCRIPTION 	  	:	Pitch regulated  

## Project description
blyth offshore wind farm is situated off the Northumberland coast, on the north-east of England. It comprises two Vestas V66 2MW wind turbines, situated approximately 1km offshore. The turbines were installed between August and October 2000 in rock-socket foundations on a submerged rock known as the ‘North Spit’. Both turbines are sited in a mean water depth of approximately 9m. Of the two turbines installed at blyth it was decided to instrument the southern-most turbine. This turbine is positioned at the top of a steeply shelving region of the sea bed which was considered would increase the likelihood of breaking waves at this turbine. Breaking waves were indeed experienced at the site during installation of the turbine. The offshore wind turbines installed at blyth are the first turbines to experience the full force of North Sea wave conditions. They therefore provide an ideal opportunity to study wave loading at full scale in an aggressive environment. The measurement programme at blyth and supporting theoretical studies are being performed in a collaborative project sponsored by the European Commission (JOR3-CT98-0284), the UK Department of Trade and Industry and Novem, The Netherlands agency for energy and the environment. The project, named ‘Offshore Wind Turbines at Exposed Sites’ (OWTES), is being undertaken collaboratively by Delft University of Technology, Germanischer Lloyd WindEnergie, Vestas Wind Systems, AMEC Wind and Powergen Renewables Developments under the leadership of Garrad Hassan and Partners.

## Measurement system
 The measurement system installed at blyth comprises three main elements: (i) measurement of the turbine structural loading (not public available) , (ii) measurement of the sea-state, and (iii) measurement of wind conditions at an onshore meteorological mast close to the turbine. The instrumentation comprising each of these sub-systems is described briefly below. Sea state climate: The wave and current climate is recorded using instruments mounted both above and below water level. A Saab WaveRadar unit is mounted on the turbine walkway to measure the instantaneous water level at the turbine base, including time-history profiles of passing waves. Simultaneously, instruments mounted on the sea bed approximately 40m from the foundation record statistics describing the wave climate and the current profile. These instruments include a wave and tide recorder (Coastal Leasing Microspec) and an acoustic doppler current profiler (Nortek ADCP). Wind measurements: Wind conditions are measured using anemometers and wind vanes mounted on an onshore meteorological mast and on the turbine nacelle. Although the meteorological mast for the project would be ideally located offshore, close to the monitored turbine, the large cost of such an installation was beyond the budget of the OWTES project. The mast has therefore been positioned on the coast, approximately 1km from the southern turbine. The mast features anemometers at heights of 10m, 20m, 30m and 40m above ground level and instruments to measure atmospheric pressure, temperature and precipitation. Measurement programme: The turbine structural loads, the sea-state at the turbine base and the wind characteristics are recorded simultaneously using a Garrad Hassan T-MON measurement system. This well-proven, robust system is well suited to offshore applications. Analogue-to-digital converters are distributed around the turbine structure and send data via a fibre-optic link to an onshore control room. Here, the collection and recording of data is controlled by a central computer which has a modem connection to allow remote interrogation. The measurement period began in November 2001 and will continue to November 2002. During this period, every sensor on the turbine is being sampled at 20Hz and data collected in two formats: ‘summary’ datasets and ‘campaign’ datasets. Summary datasets consist of the basic statistics of each channel, measured every ten minutes, comprising the minimum, maximum, mean and standard deviation of each signal. Campaign datasets store time-history data from each measured channel and will be recorded in order to establish the dynamic behaviour, fatigue load spectra and extreme loads experienced by the turbine. The recording of campaign datasets is triggered automatically by the central computer when pre-defined trigger conditions are met. A database of campaign datasets is currently being compiled for a range of ‘normal’ conditions, covering variations in mean water level, mean wind speed and significant wave height. Campaign datasets are also being used to record extreme wave loading events. Summary datasets are being used for a number of purposes, one of which is to compile a database of wind and wave characteristics at the site.

1. [List of statistical signals](./ts_wt/blyth/re_mast_signals.csv)
2. [List of turbine & sea state signals](./ts_wt/blyth/ts_signals.csv)
3. [List of additional signals](./ts_wt/blyth/add_signals.csv) Note: The statistics for these signals are only included in the header of the packed ASCII files.

## Nominal values

![](./ts_wt/blyth/nom_speed.gif)
**Figure n1:** Nominal wind speed

![A map of Denmark](./ts_wt/blyth/nom_ti.gif)
**Figure n2:** Nominal turbulence

![A map of Denmark](./ts_wt/blyth/nom_dir.gif)
**Figure n3:** Nominal wind direction

## Distributions
![A map of Denmark](./ts_wt/blyth/w_dist.gif)
**Figure n4:** Nominal wind speed distribuion.

![A map of Denmark](./ts_wt/blyth/ti_dist.gif)
**Figure n5:** Nominal turbulence distribuion.

![A map of Denmark](./ts_wt/blyth/dir_dist.gif)
**Figure n6:** Nominal wind direction distribuion. 

## ACKNOWLEDMENTS	:	
- INSTITUTION       :	Garrad Hassan and Partners Ltd. and OWTES 
- ADDRESS 	        :   St Vincent's Works, Silverthorne Lane, Bristol, BS2
- TEL/FAX 	        :   +44 (0)117 972 9900 / +44 (0)117 972 9901
- CONTACTS 	        :   Tim Camp
- LINKS 	        :   http:\\www.garradhassan.com\
- COLLABS 	        :   DUT, German. Loyd, Vestas, AMEC, Powergen
- FUND AGENTS       :   CEC, DGXII JOR3-CT98-0284
- PERIOD 	        :   2001-01-01 - 2003-12-31

## PUBLICATIONS	    : 
1. [Design methods for Offshore Wind Turbine at Exposed Sites. Final report.](./ts_wt/blyth/doc_05.pdf)

## Public data
1. Resource data  (NetCDF) 
2. Run statistics (NetCDF) 
3. Time series (zip)

